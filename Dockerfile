FROM node:10
WORKDIR /usr/src/app
COPY package*.json ./

USER root

#for serial port
#RUN apk update
#RUN apk add --no-cache git make gcc g++ python linux-headers udev build-essential libudev-dev

RUN apt-get update
RUN apt-get -y install build-essential libudev-dev

RUN npm install
COPY . .
EXPOSE 4003

CMD ["npm", "start"]
