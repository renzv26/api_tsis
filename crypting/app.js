const dotenv = require("dotenv");
dotenv.config();

const crypto = require("crypto"),
  algorithm = process.env.ALGORITHM,
  password = process.env.ENCRYPTION_KEY,
  iv = process.env.IV;

const encrypt = text => {
  var cipher = crypto.createCipheriv(algorithm, password, iv);
  var encrypted = cipher.update(text, "utf8", "hex");
  encrypted += cipher.final("hex");
  var tag = cipher.getAuthTag();
  return encrypted;
};

const decrypt = encrypted => {
  var decipher = crypto.createDecipheriv(algorithm, password, iv);
  var dec = decipher.update(encrypted, "hex", "utf8");
  return dec;
};

module.exports = { encrypt, decrypt };
