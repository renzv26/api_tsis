const {
  rawMaterialAddNews,
  selectAllRawMaterials,
  selectOneRawMaterials,
  rawMaterialUpdates,
} = require("../../src/controller/raw-material/app");

const rawMaterial = ({ router, makeExpressCallback, verifyTokens }) => {
  // POST

  // add new
  router.post("/add", verifyTokens, makeExpressCallback(rawMaterialAddNews));

  // GET
  // select all
  router.get(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllRawMaterials)
  );
  // select one
  router.get(
    "/select/:id",
    verifyTokens,
    makeExpressCallback(selectOneRawMaterials)
  );

  // PUT
  // update selected
  router.put(
    "/update/:id",
    verifyTokens,
    makeExpressCallback(rawMaterialUpdates)
  );

  return router;
};

module.exports = rawMaterial;
