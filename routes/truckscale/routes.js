const {
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates,
  selectLocations,
  locationAdds,
  locationUpdates,
  printLocAdds,
  selectPrintLocations,
  printLocationUpdates,
} = require("../../src/controller/truckscale/app");

const truckScale = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new truck scale
  router.post("/add", verifyTokens, makeExpressCallback(truckScaleAddNews));

  router.post(
    "/locations/select",
    verifyTokens,
    makeExpressCallback(selectLocations)
  );

  router.post(
    "/locations/add",
    verifyTokens,
    makeExpressCallback(locationAdds)
  );

  router.post(
    "/print-location/add",
    verifyTokens,
    makeExpressCallback(printLocAdds)
  );

  router.post(
    "/print-location/select",
    verifyTokens,
    makeExpressCallback(selectPrintLocations)
  );

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // update ts
  router.put(
    "/update/:id",
    verifyTokens,
    makeExpressCallback(truckScaleUpdates)
  );

  // update location
  router.put(
    "/locations/update",
    verifyTokens,
    makeExpressCallback(locationUpdates)
  );

  router.put(
    "/print-location/update",
    verifyTokens,
    makeExpressCallback(printLocationUpdates)
  );
  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all
  router.get("/select", verifyTokens, makeExpressCallback(selectAllTss));

  // select one
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneTss));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = truckScale;
