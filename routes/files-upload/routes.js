const { filesUploads } = require("../../src/controller/file-uploads/app");

const upload = async ({
  router,
  makeExpressCallback,
  uploadFiless,
  verifyTokens,
  resizeImages,
}) => {
  //########################
  // POST REQUESTS
  //########################

  // call function defined at the bottom
  const upload = await uploading({ uploadFiless });

  // upload files  to server
  router.post(
    "/uploads",
    upload.any(),
    verifyTokens,
    resizeImages,
    makeExpressCallback(filesUploads)
  );

  //########################
  // END POST REQUESTS
  //########################

  return router;
};

// return async function
const uploading = async ({ uploadFiless }) => {
  const data = await uploadFiless({});
  return data;
};

module.exports = upload;
