const {
    seedSupplierController
} = require("../../src/controller/supplier/app")

const suppllier = ({ router, makeExpressCallback, verifyTokens }) => {
    router.post("/batch", makeExpressCallback(seedSupplierController))
};

module.exports = suppllier;