const {
  printLogsInserts,
  countPrints,
  updatePrintLogs,
  selectPrintLogs,
} = require("../../src/controller/print-logs/app");

const printLogs = ({ router, makeExpressCallback, verifyTokens }) => {
  
  router.post("/add", verifyTokens, makeExpressCallback(printLogsInserts));

  router.post("/count", verifyTokens, makeExpressCallback(countPrints));

  router.post("/update", verifyTokens, makeExpressCallback(updatePrintLogs));

  router.post("/select", verifyTokens, makeExpressCallback(selectPrintLogs));

  return router;
};

module.exports = printLogs;
