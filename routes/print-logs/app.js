const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");
const { verifyTokens } = require("../../src/token/app");

const printLogs = require("./routes");

const printLogss = printLogs({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  printLogss,
});

module.exports = services;

module.exports = {
  printLogss,
};

module.exports = router;
