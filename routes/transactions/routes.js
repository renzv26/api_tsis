const {
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates,
  selectAllUoms,
  transactionsToUpdates,
  requestAdds,
  selectRequests,
  cancelRequests,
  c_addNewTransactionSqa,

  unahcoAddUseCase
} = require("../../src/controller/transactions/app");

const transaction = ({
  router,
  makeExpressCallback,
  verifyTokens,
  fsqrValidates,
}) => {
  // POST

  // add
  router.post("/sqa", makeExpressCallback(c_addNewTransactionSqa))

  router.post("/add", verifyTokens, makeExpressCallback(transactionAddNews));

  router.post("/scan", verifyTokens, makeExpressCallback(c_scanTransaction));

  // GET

  router.get(
    "/selectAllWarehouses",
    verifyTokens,
    makeExpressCallback(c_selectAllWarehouses)
  );

  // select all and single transaction if has query code in URL
  router.post(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllTransactionss)
  );

  router.post("/inbound/add", verifyTokens, makeExpressCallback(c_addInbound));
  router.post(
    "/outbound/add",
    verifyTokens,
    makeExpressCallback(c_addOutbound)
  );
  router.post("/fetch-sap", makeExpressCallback(purchaseOrderFetchs));

  router.get(
    "/notif/for-transmittal",
    verifyTokens,
    makeExpressCallback(notifForTransmittals)
  );

  // validate for fsqr only
  router.get("/grpo/:code", fsqrValidates, makeExpressCallback(grpoFsqrs));

  // update status and attached PO; in FSQR app
  router.post(
    "/update/:code",
    fsqrValidates,
    makeExpressCallback(transactionUpdates)
  );

  router.get("/uom", verifyTokens, makeExpressCallback(selectAllUoms));

  router.put(
    "/update-transaction",
    verifyTokens,
    makeExpressCallback(transactionsToUpdates)
  );

  router.post("/request/add", verifyTokens, makeExpressCallback(requestAdds));
  router.post(
    "/request/select",
    verifyTokens,
    makeExpressCallback(selectRequests)
  );

  router.put(
    "/request/update/:id",
    verifyTokens,
    makeExpressCallback(cancelRequests)
  );

  router.post("/unahco/update",  makeExpressCallback(unahcoAddUseCase));

  return router;
};

module.exports = transaction;
