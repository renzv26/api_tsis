const {
  weightTypeAddNews,
  selectAllWeightTypes,
  selectOneWeightTypes,
  weightTypeUpdates
} = require("../../src/controller/weight-types/app");

const weight = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add
  router.post("/add", verifyTokens, makeExpressCallback(weightTypeAddNews));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // update weight type
  router.put(
    "/update/:id",
    verifyTokens,
    makeExpressCallback(weightTypeUpdates)
  );

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all
  router.get(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllWeightTypes)
  );

  // select one
  router.get(
    "/select/:id",
    verifyTokens,
    makeExpressCallback(selectOneWeightTypes)
  );
  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = weight;
