const {
    selectAllItemsController
} = require("../../src/controller/items/app")

const items = ({ router, makeExpressCallback, verifyTokens }) => {
    
    router.get("/select", makeExpressCallback(selectAllItemsController));

    return router;
  };
  
  module.exports = items;
  