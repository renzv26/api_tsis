const {
  supplierAddNews,
  selectAllSuppliers,
  selectOneSuppliers,
  suppliersUpdates,
} = require("../../src/controller/suppliers/app");

const supplier = ({ router, makeExpressCallback, verifyTokens }) => {
  // POST

  // add new
  router.post("/add", verifyTokens, makeExpressCallback(supplierAddNews));

  // GET

  // select all
  router.get("/select", verifyTokens, makeExpressCallback(selectAllSuppliers));

  // no token route for fsqr
  router.get("/select/fsqr", makeExpressCallback(selectAllSuppliers));

  // select one
  router.get(
    "/select/:id",
    verifyTokens,
    makeExpressCallback(selectOneSuppliers)
  );

  // PUT

  // update selected supplier
  router.put(
    "/update/:id",
    verifyTokens,
    makeExpressCallback(suppliersUpdates)
  );

  return router;
};

module.exports = supplier;
