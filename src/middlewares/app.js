// for image download
const multer = require("multer");
const uniqueString = require("unique-string");
const fs = require("fs");
const path = require("path");
const resizeImg = require("resize-img");

// #############################

const uploadFiles = require("./file-uploader");
const fsqrValidate = require("./fsqr-credentials");
const resizeImage = require("./image-resizer");
// #############################
const uploadFiless = uploadFiles({ multer, path, uniqueString, fs });
const fsqrValidates = fsqrValidate({});
const resizeImages = resizeImage({ uniqueString, resizeImg, fs });

const services = Object.freeze({
  uploadFiless,
  fsqrValidates,
  resizeImages,
});

module.exports = services;
module.exports = {
  uploadFiless,
  fsqrValidates,
  resizeImages,
};
