const resizeImage = ({ uniqueString, resizeImg, fs }) => {
  return async function post(req, res, next) {
    //******** using sharp */
    // if(req.files.length > 0){

    //     const filename = uniqueString() + "-" +req.files[0].originalname.toLowerCase()

    //     await sharp(req.files[0].buffer)
    //     .resize(207, 79)
    //     .toFormat("png")
    //     .toFile(`uploads/${filename}`)

    //     req.files[0].filename = filename

    //     next()

    // } else {
    //     next()
    // }

    //******** using resize-img */
    if (req.files.length > 0) {
      (async () => {
        const filename =
          (await uniqueString()) +
          "-" +
          req.files[0].originalname.toLowerCase();

        req.files[0].filename = filename;

        const image = await resizeImg(req.files[0].buffer, {
          width: 207,
          height: 79,
        });

        fs.writeFileSync(`uploads/${filename}`, image);
      })();

      next();
    } else {
      next();
    }
  };
};

module.exports = resizeImage;
