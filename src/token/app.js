const jwt = require("jsonwebtoken");
require("dotenv").config();
const userDb = require("../data-access/db-layer/users/app");

//#######################
const makeToken = require("./make-token");
const verifyToken = require("./verify-token")

//#######################
const makeTokens = makeToken({ jwt });
const verifyTokens = verifyToken({jwt, userDb})

const services = Object.freeze({
  makeTokens,
  verifyTokens
});

module.exports = services;
module.exports = {
  makeTokens,
  verifyTokens
};
