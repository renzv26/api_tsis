const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const methodOverride = require("method-override");

const { initDbs } = require("../src/data-access/config/app");
initDbs();


dotenv.config();
const app = express();

// Add this to the VERY top of the first file loaded in your app
// var apm = require("elastic-apm-node").start({
//   // Override the service name from package.json
//   // Allowed characters: a-z, A-Z, 0-9, -, _, and space
//   serviceName: "tsis-api-eut",

//   // Use if APM Server requires a secret token
//   secretToken: "",

//   // Set the custom APM Server URL (default: http://localhost:8200)
//   serverUrl: "https://elasticsearch.biotechfarms.net",

//   // Set the service environment
//   environment: "eut",
// });

const session = require("express-session");

// can override requests
app.use(methodOverride("X-HTTP-Method-Override"));

// accessible to any
app.use(cors());

// Body Parser middleware to handle raw JSON files
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/uploads", express.static("uploads"));

const PORT = process.env.PORT || 3000;

const server = app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}.`);
});

const io = require("socket.io")(server); //Bind socket.io to our express server.

module.exports = { io };

// server should be export before the routes
// needed for socket.io; for serial communication

app.use("/tsis-api/drivers", require("../routes/drivers/app"));

app.use("/tsis-api/truck-scale", require("../routes/truckscale/app"));

app.use("/tsis-api/modules", require("../routes/modules/app"));

app.use("/tsis-api/actions", require("../routes/actions/app"));

app.use("/tsis-api/access-rights", require("../routes/access_rights/app"));

app.use("/tsis-api/roles", require("../routes/roles/app"));

app.use("/tsis-api/users", require("../routes/users/app"));

app.use("/tsis-api/ports", require("../routes/com-port/app"));

app.use("/tsis-api/seeders", require("../routes/seeders/app"));

app.use("/tsis-api/files", require("../routes/files-upload/app"));

app.use("/tsis-api/suppliers", require("../routes/suppliers/app"));

app.use("/tsis-api/raw-material", require("../routes/raw-material/app"));

app.use("/tsis-api/truck-types", require("../routes/truck-types/app"));

app.use("/tsis-api/truck-infos", require("../routes/truck-infos/app"));

app.use(
  "/tsis-api/transaction-type",
  require("../routes/transaction-type/app")
);

app.use("/tsis-api/weight-type", require("../routes/weight-types/app"));

app.use("/tsis-api/transactions", require("../routes/transactions/app"));

app.use("/tsis-api/reports", require("../routes/reports/app"));

app.use("/tsis-api/pos", require("../routes/pos/app"));

app.use("/tsis-api/print-logs", require("../routes/print-logs/app"));

app.use("/tsis-api/items", require("../routes/items/app"));

app.use("/tsis-api/supplier",require("../routes/supplier/app"));

// enable web socket
const { webSockets } = require("./socket/app");
webSockets();

module.exports = app;
