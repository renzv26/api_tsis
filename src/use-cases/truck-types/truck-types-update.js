const updateTruckType = ({
  truckTypesDb,
  e_updateTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'truck and driver','edit truck type')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if(mode == 1){

      
      delete info.modules


      delete info.mode; 
      delete info.source;

      info.id = id

      info = await objectLowerCaser(info)

      val({info})
      
      const check = await truckTypes.truckTypesAddSelectByName({info})

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Truck model already exists.`);
      }



      const res = await truckTypes.truckTypesUpdate({ info });
      return res;

    }else {


    const entity = await e_updateTruckType(info);

    // check if id exist
    const exist = await truckTypesDb.selectOneTruckType({ id });

    if (exist.rowCount === 0) {
      throw new Error(`Truck type doesn't exist.`);
    }

    // check if type and model exist
    const trucktype = await truckTypesDb.selectTruckTypeAndModelUpdate({
      truck_type: entity.getTruckType(),
      truck_model: entity.getTruckModel(),
      id
    });

    if (trucktype.rowCount !== 0) {
      throw new Error("The truck type and model already exist.");
    }

    // query previous values
    const previous = await truckTypesDb.selectOneTruckType({ id });
    const prev_data = previous.rows[0];
    const prev_values = {
      id: prev_data.id,
      truck_type: prev_data.truck_type,
      truck_model: prev_data.truck_model,
      created_at: prev_data.created_at,
      updated_at: prev_data.updated_at
    };

    // update
    const update = await truckTypesDb.updateTruckType({
      truck_type: entity.getTruckType(),
      truck_model: entity.getTruckModel(),
      truck_size: entity.getTruckSize(),
      updated_at: entity.getUpdatedAt(),
      id
    });

    const count = update.rowCount;

    const data = {
      msg: `Updated successfully ${count} truck type.`
    };

    // new values
    const new_values = {
      truck_type: entity.getTruckType(),
      truck_model: entity.getTruckModel(),
      truck_size: entity.getTruckSize(),
      updated_at: entity.getUpdatedAt()
    };

    // logs object
    const logs = {
      action_type: "UPDATE TRUCK TYPE",
      table_affected: "ts_truck_types",
      new_values,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  }
  };
};

const val = ({ info }) => {
  const { U_TS_TRTYPE, U_TS_TRMODEL, U_TS_TRSIZE } = info;
  
  if (!U_TS_TRTYPE) {
    const d = {
      msg: "Please enter truck type."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_TRMODEL) {
    const d = {
      msg: "Please enter truck model."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_TRSIZE) {
    const d = {
      msg: "Please enter truck size."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateTruckType;
