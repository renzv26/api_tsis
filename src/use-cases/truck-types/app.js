const { insertActivityLogss } = require("../users/app"); // logs

const {
  e_addTruckType,
  e_updateTruckType
} = require("../../entities/truck-types/app");

const truckTypesDb = require("../../data-access/db-layer/truck-types/app");

const { truckTypes } = require("../../data-access/sl-layer/truck-types/app");
const { objectLowerCaser } = require("../../lowerCaser/app"); 

const { validateAccessRights } = require("../../validator/app"); //validator

const addTruckType = require("./truck-types-add");
const selectAllTruckTypes = require("./truck-types-select-all");
const selectOneTruckType = require("./truck-types-select-one");
const updateTruckType = require("./truck-types-update");

const uc_addTruckType = addTruckType({
  truckTypesDb,
  e_addTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights
});
const uc_selectAllTruckTypes = selectAllTruckTypes({ truckTypesDb, truckTypes, validateAccessRights });
const uc_selectOneTruckType = selectOneTruckType({ truckTypesDb, truckTypes, validateAccessRights });
const uc_updateTruckType = updateTruckType({
  truckTypesDb,
  e_updateTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights
});

const services = Object.freeze({
  uc_addTruckType,
  uc_selectAllTruckTypes,
  uc_selectOneTruckType,
  uc_updateTruckType
});

module.exports = services;
module.exports = {
  uc_addTruckType,
  uc_selectAllTruckTypes,
  uc_selectOneTruckType,
  uc_updateTruckType
};
