const suppliersSelectAll = ({ supppliers }) => {
  return async function selects(info) {
    const { mode } = info;

    if (mode == 1) {
      const res = await supppliers.selectAllSuppliers({});
      return res;
    } else {
      // offline mode
    }
  };
};

module.exports = suppliersSelectAll;
