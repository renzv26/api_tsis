const { makeSupplier } = require("../../entities/suppliers/app"); // entity
const suppliersDb = require("../../data-access/db-layer/suppliers/app"); //db
const { insertActivityLogss } = require("../users/app"); // logs
const { supppliers } = require("../../data-access/sl-layer/suppliers/app");

//#######################
const addNewSupplier = require("./supplier-add");
const suppliersSelectAll = require("./supplier-select-all");
const suppliersSelectOne = require("./supplier-select-one");
const updateSuppliers = require("./supplier-update");
//#######################
const addNewSuppliers = addNewSupplier({
  suppliersDb,
  makeSupplier,
  insertActivityLogss,
});
const suppliersSelectAlls = suppliersSelectAll({ supppliers });
const suppliersSelectOnes = suppliersSelectOne({ suppliersDb });
const updateSupplierss = updateSuppliers({
  suppliersDb,
  makeSupplier,
  insertActivityLogss,
});

const services = Object.freeze({
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss,
});

module.exports = services;
module.exports = {
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss,
};
