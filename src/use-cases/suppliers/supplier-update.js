const updateSuppliers = ({
  suppliersDb,
  makeSupplier,
  insertActivityLogss
}) => {
  return async function put({ id, ...info } = {}) {
    const result = makeSupplier(info);

    const supplierExist = await suppliersDb.selectByNameUpdate({
      name: result.getSupplierName(),
      id
    });

    if (supplierExist.rowCount !== 0) {
      throw new Error("The supplier's name already exist.");
    }

    // query previous values
    const previous = await suppliersDb.selectOneSupplier({ id });
    const prev_data = previous.rows[0];
    const prev_values = {
      id: prev_data.id,
      name: prev_data.name,
      address: prev_data.address,
      contact_number: prev_data.contact_number
    };

    // update to db
    const update = await suppliersDb.updateSupplier({
      id,
      name: result.getSupplierName(),
      address: result.getAddress(),
      contact_number: result.getContact()
    });

    const count = update.rowCount; // get the number of updated data

    const data = {
      msg: `Updated successfully ${count} supplier.`
    };

    // logs
    // new values
    const new_values = {
      name: result.getSupplierName(),
      address: result.getAddress(),
      contact_number: result.getContact()
    };

    const logs = {
      action_type: "UPDATE SUPPLIER",
      table_affected: "ts_suppliers",
      new_values,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = updateSuppliers;
