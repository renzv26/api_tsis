const suppliersSelectOne = ({ suppliersDb }) => {
  return async function selects({ id }) {
    const result = await suppliersDb.selectOneSupplier({ id });
    const suppliers = result.rows;

    return suppliers;
  };
};

module.exports = suppliersSelectOne;
