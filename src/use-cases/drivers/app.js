const makeDriver = require("../../entities/drivers/app"); // entity
const driversDb = require("../../data-access/db-layer/drivers/app"); //db
const { decrypt } = require("../../../crypting/app");
const { validateAccessRights } = require("../../validator/app"); //validator
const { insertActivityLogss } = require("../users/app"); // logs

const { drivers } = require("../../data-access/sl-layer/drivers/app");

const { objectLowerCaser } = require("../../lowerCaser/app"); 

//####################
const addNewDriver = require("./drivers-add");
const updateDriver = require("./drivers-update");
const driversSelectAll = require("./drivers-select-all");
const driversSelectOne = require("./drivers-select-one");

//####################
const addNewDrivers = addNewDriver({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  drivers,
  objectLowerCaser
});
const updateDrivers = updateDriver({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  objectLowerCaser,
  drivers
});
const driversSelectAlls = driversSelectAll({
  driversDb,
  decrypt,
  validateAccessRights,
  drivers
});
const driversSelectOnes = driversSelectOne({
  driversDb,
  decrypt,
  validateAccessRights,
  drivers
});

const services = Object.freeze({
  addNewDrivers,
  updateDrivers,
  driversSelectAlls,
  driversSelectOnes
});

module.exports = services;
module.exports = {
  addNewDrivers,
  updateDrivers,
  driversSelectAlls,
  driversSelectOnes
};
