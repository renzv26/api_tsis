const { insertActivityLogss } = require("../users/app"); // logs

const {
  e_addTruckInfo,
  e_updateTruckInfo
} = require("../../entities/truck-infos/app");

const truckInfosDb = require("../../data-access/db-layer/truck-infos/app");
const truckTypesDb = require("../../data-access/db-layer/truck-types/app");


const { truckInfos } = require("../../data-access/sl-layer/truck-infos/app");

const { validateAccessRights } = require("../../validator/app"); //validator

const { objectLowerCaser } = require("../../lowerCaser/app"); 

const addTruckInfo = require("./truck-infos-add");
const selectAllTruckInfos = require("./truck-infos-select-all");
const selectOneTruckInfo = require("./truck-infos-select-one");
const updateTruckInfo = require("./truck-infos-update");

const uc_addTruckInfo = addTruckInfo({
  truckInfosDb,
  truckTypesDb,
  e_addTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights
});
const uc_selectAllTruckInfos = selectAllTruckInfos({ truckInfosDb, truckInfos, validateAccessRights });
const uc_selectOneTruckInfo = selectOneTruckInfo({ truckInfosDb, truckInfos, validateAccessRights });
const uc_updateTruckInfo = updateTruckInfo({
  truckInfosDb,
  truckTypesDb,
  e_updateTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights
});

const services = Object.freeze({
  uc_addTruckInfo,
  uc_selectAllTruckInfos,
  uc_selectOneTruckInfo,
  uc_updateTruckInfo
});

module.exports = services;
module.exports = {
  uc_addTruckInfo,
  uc_selectAllTruckInfos,
  uc_selectOneTruckInfo,
  uc_updateTruckInfo
};
