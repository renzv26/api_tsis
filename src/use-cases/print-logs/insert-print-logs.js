const insertPrintLogs = ({
  validateAccessRights,
  printLogs,
  moment,
  escpos,
  transactions,
}) => {
  return async function posts(info) {

    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "report",
      "print reports"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      // online
      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz("Asia/Manila").format("HH:mm:ss");
      // console.log('=---=',t);
      const time = await fixTime(t);

      // ####### start printing
      const { acronym, item, printCounts } = info;

      let device = null;

      try {
        const dev = new escpos.USB(
          process.env.printParams1,
          process.env.printParams2
        );
        device = dev;
      } catch (e) {
        console.log("Error on initialize device: ", e);
      }

      if (device) {
        const options = { encoding: "GB18030" /* default */ };

        const printer = new escpos.Printer(device, options);

        let headerImage,
          printCount = "",
          location = "";

        location = acronym; // pass acronym of print location selected

        const biotechLogo = await new Promise((resolve) => {
          escpos.Image.load(`static/biotech.png`, function (image) {
            resolve(image);
          });
        });

        const unahcoLogo = await new Promise((resolve) => {
          escpos.Image.load(`static/unahco.png`, function (image) {
            resolve(image);
          });
        });

        const whiteBackground = await new Promise((resolve) => {
          escpos.Image.load(`static/white.png`, function (image) {
            resolve(image);
          });
        });

        const res = await transactions.getReportHeaderByTransId(item.ticket_no);
//  console.log(res[0], "Item Print")
// console.log(item, "PRINT AFETR TEST") 
        const signature = await new Promise((resolve) => {
          if (item.weight) {
            escpos.Image.load(
              `${item.weight[0].signature.substring(1)}`,
              function (image) {
                resolve(image);
              }
            );
          } else {
            escpos.Image.load(
              `${res[0].OB_SIGN.substring(1)}`,
              function (image) {
                resolve(image);
              }
            );
          }
        });

        // determine header image
        // if (item.transaction_type) {
        //   //print for report page
        //   if (item.transaction_type.toLowerCase() == "unahco") {
        //     headerImage = unahcoLogo;
        //   } else {
        //     headerImage = biotechLogo;
        //   }
        // } else {
        //   //print for request page
        //   if (res[0].U_TS_TRNSTYPE.toLowerCase() == "unahco") {
        //     headerImage = unahcoLogo;
        //   } else {
        //     headerImage = biotechLogo;
        //   }
        // }
        
        // determine header image
        if (item.transaction_type) {
          //print for report page
          if(item.company){
          if (item.company.toLowerCase() == "unahco") {
            headerImage = unahcoLogo;
          } else {
            headerImage = biotechLogo;
          }
        }else{
          //print for report page
          if (item.transaction_type.toLowerCase() == "unahco") {
            headerImage = unahcoLogo;
          } else {
            headerImage = biotechLogo;
          }
        }
        } else {
          //print for request page
          if (res[0].U_COMPANY.toLowerCase() == "unahco") {
            headerImage = unahcoLogo;
          } else {
            headerImage = biotechLogo;
          }
        }

        if (printCounts == 0) {
          printCount = "1";
        } else {
          printCount = "RP# " + printCounts;
        }

        // get address of truckscale;
        const trnsId = item.transaction_id;
        
        if (trnsId) {
          let address = await printLogs.detailedAddress(trnsId);

          if (address.length > 0) {
            address = address[0].U_DETAILED_LOCATION;

            // new line if greater than 29 characters;
            address = `${address.substring(0, 29)}\n${address.substring(29)}`;
          } else {
            address = "";
          }

          let ib_date = `${moment(item.weight[0].ib_date).format(
            "YYYY-MM-DD"
          )} ${intToTime(item.weight[0].ib_time)}`;
          let ob_date = `${moment(item.weight[0].ib_date).format(
            "YYYY-MM-DD"
          )} ${intToTime(item.weight[0].ob_time)}`;

          ib_date = moment(ib_date).format("MMM DD, YYYY hh:mm A");
          ob_date = moment(ob_date).format("MMM DD, YYYY hh:mm A");

          device.open(function () {
            printer
              .align("ct")
              .hoiImage(whiteBackground)
              .hoiImage(headerImage)
              .size(2, 2)
              .text("Weight Slip")
              .size(1, 1)
              .text("--------------------------------")
              .text(`${location}                        ${printCount}`)
              .text("")
              .text(`${address.toUpperCase()}`)
              .text("")
              .align("lt")
              .text(`Ticket Number: ${item.ticket_number}`)
              .text("")
              .text(`Customer: ${item.supplier.toUpperCase()}`)
              .text("")
              .text(`DR no.: ${item.dr_number ? item.dr_number.toUpperCase() : "N/A"}`)
              .text("")
              .text(`Supplier Code: ${item.tracking_code.toUpperCase()}`)
              .text("")
              .text(`Address: ${item.address.toUpperCase()}`)
              .text("")
              .text(`Plot Code: ${item.plot_code ? item.plot_code.toUpperCase() : "N/A"}`)
              .text("")
              .text(`Commodity: ${item.raw_material.toUpperCase()}`)
              .text("")
              .text(`Plate Number: ${item.plate_number.toUpperCase()}`)
              .text("")
              .text(`Driver: ${item.driver.toUpperCase()}`)
              .text("")
              .text(`Quantity: ${item.quantity}`)
              .text("")
              .text(
                ` Inbound Wt kg: ${parseFloat(item.weight[0].ib).toFixed(2)}`
              )
              .text("")
              .text(`  Date: ${ib_date}`)
              .text("")
              .text(
                ` Outbound Wt kg: ${parseFloat(item.weight[0].ob).toFixed(2)}`
              )
              .text("")
              .text(`  Date: ${ob_date}`)
              .text("")
              .text(` Net Weight: ${parseFloat(item.weight[0].net).toFixed(2)}`)
              .text("")
              .text("Remarks: ")
              .text("")
              .text(
                `${item.weight[0].ob_remarks ? item.weight[0].ob_remarks : ""}`
              )
              .align("ct")
              .hoiImage(signature)
              .text(`Weigher: ${item.weight[0].ob_weigher}`)
              .text("")
              .text("")
              .text("")
              .text("")
              .cut();
            // .close();
          });
        } else {
          const trnsId = res[0].DocEntry;
         
          let address = await printLogs.detailedAddress(trnsId);
          // console.log(address, "Adress");
          if (address.length > 0) {
            address = address[0].U_DETAILED_LOCATION;

            // new line if greater than 29 characters;
            address = `${address.substring(0, 29)}\n${address.substring(29)}`;
          } else {
            address = "";
          }
          let ib_date = `${moment(res[0].IB_UPDATE_DATE).format(
            "YYYY-MM-DD"
          )} ${intToTime(res[0].IB_UPDATE_TIME)}`;
          
          let ob_date = `${moment(res[0].OB_UPDATE_DATE).format(
            "YYYY-MM-DD"
          )} ${intToTime(res[0].OB_UPDATE_TIME)}`;

          ib_date = moment(ib_date).format("MMM DD, YYYY hh:mm A");
          ob_date = moment(ob_date).format("MMM DD, YYYY hh:mm A");
          
          const net = parseFloat(
            parseFloat(res[0].IB_WEIGHT ? res[0].IB_WEIGHT : 0) -
              parseFloat(res[0].OB_WEIGHT ? res[0].OB_WEIGHT : 0)
          ).toFixed(2);
          device.open(function () {
            printer
              .align("ct")
              .hoiImage(whiteBackground)
              .hoiImage(headerImage)
              .size(2, 2)
              .text("Weight Slip")
              .size(1, 1)
              .text("--------------------------------")
              .text(`${location}                        ${printCount}`)
              .text("")
              .text(`${address.toUpperCase()}`)
              .text("")
              .align("lt")
              .text(`Ticket Number: ${res[0].U_TS_TICKETNUMBER}`)
              .text("")
              .text(`Customer: ${res[0].U_TS_SUPPLIER.toUpperCase()}`)
              .text("")
              .text(`DR no.: ${res[0].U_TS_DRNUMBER ? res[0].U_TS_DRNUMBER : "N/A"}`)
              .text("")
              .text(`Supplier Code: ${res[0].U_TS_TRCK_CODE.toUpperCase()}`)
              .text("")
              .text(`Address: ${res[0].U_TS_SUPPLIER_ADD.toUpperCase()}`)
              .text("")
              .text(`Plot Code: ${res[0].U_TS_PLOT_CODE ? res[0].U_TS_PLOT_CODE.toUpperCase() : "N/A"}`)
              .text("")
              .text(`Commodity: ${res[0].U_TS_ITEMNAME}`)
              .text("")
              .text(`Plate Number: ${res[0].U_TS_PLATE_NUM.toUpperCase()}`)
              .text("")
              .text(`Driver: ${res[0].DRIVER_NAME.toUpperCase()}`)
              .text("")
              .text(`Quantity: ${res[0].U_TS_NUM_BAGS} ${res[0].U_UOM_TS}`)
              .text("")
              .text(
                ` Inbound Wt kg: ${parseFloat(res[0].IB_WEIGHT).toFixed(2)}`
              )
              .text("")
              .text(`  Date: ${ib_date}`)
              .text("")
              .text(
                ` Outbound Wt kg: ${parseFloat(res[0].OB_WEIGHT).toFixed(2)}`
              )
              .text("")
              .text(`  Date: ${ob_date}`)
              .text("")
              .text(` Net Weight: ${parseFloat(net).toFixed(2)}`)
              .text("")
              .text("Remarks: ")
              .text("")
              .text(
                `${res[0].OB_REMARKS ? res[0].OB_REMARKS : ""}`
              )
              .align("ct")
              .hoiImage(signature)
              .text(`Weigher: ${res[0].OB_WEIGHER ? res[0].OB_WEIGHER : ""}`)
              .text("")
              .text("")
              .text("")
              .text("")
              .cut();
            // .close();
          });
        }

        
          
        
      }

      // ####### end printing

      // get max code
      const max = await printLogs.printLogsMaxCode({});
      const maxCode = max[0].maxCode;

      const { transaction_id, ticket_number, employee_id, cookie } = info;

      if (!transaction_id) throw new Error(`Please enter transaction ID.`);
      if (!employee_id) throw new Error(`Please enter employee ID.`);
// console.log(info, "PRINT AFETR")

      // make the object
      const data = {
        Code: maxCode,
        Name: maxCode,
        U_TS_EMPLOYEE_ID: employee_id,
        U_TS_TRNSCTION_ID: transaction_id,
        U_TS_TICKETNUMBER: ticket_number,
        U_TS_CREATEDATE: date,
        U_TS_CREATETIME: time,
        U_TS_UPDATEDATE: date,
        U_TS_UPDATETIME: time,
        cookie,
      };

      const res = await printLogs.printLogsAdd({ info: data });
      if (res.status == 201) {
      }
      return res;
    } else {
      // offline
    }
  };
};

// const convertCase = (str) => {
//   if (str) {
//     const splitStr = str.toLowerCase().split(" ");
//     for (let i = 0; i < splitStr.length; i++) {
//       splitStr[i] =
//         splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
//     }
//     return splitStr.join(" ");
//   }
// };

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

// to print weight slip; raw printing
const printWs = async ({ info, moment, printLogs }, escpos) => {};

module.exports = insertPrintLogs;
