const printLogsSelect = ({ validateAccessRights, printLogs, moment }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "view print logs"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      // online
      const date = info.dateRange;
      const { date_from, date_to } = date;

      const res = await printLogs.selectPrintLogs(date_from, date_to);

      let data = [];

      for (let i = 0; i < res.length; i++) {
        const e = res[i];

        const cdate = moment(e.U_TS_CREATEDATE).format("YYYY-MM-DD");
        const ctime = await intToTime(e.U_TS_CREATETIME);

        const company = e.U_COMPANY ? convertCase(e.U_COMPANY) : null;
        let tType = "";
        if (!company) {
          tType = convertCase(e.U_TS_TRNSTYPE);
        } else {
      
          tType = `${company} ( ${convertCase(e.U_TS_TRNSTYPE)} )`;
        }
        data.push({
          date_printed: `${cdate} ${ctime}`,
          employee_id: e.U_TS_EMP_ID,
          first_name: e.firstName,
          last_name: e.lastName,
          print_log_id: e.Code,
          transaction_id: e.U_TS_TRNSCTION_ID,
          ticket_number: e.U_TS_TICKETNUMBER,
          transaction_type: tType,
        });
      }

      return data;
    } else {
      // offline
    }
  };
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

// format string
const convertCase = (str) => {
  if (str) {
    const splitStr = str.toLowerCase().split(" ");
    for (let i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }
};

module.exports = printLogsSelect;
