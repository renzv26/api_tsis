const printCount = ({ validateAccessRights, printLogs }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "report",
      "view reports"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      // online
      const { id } = info; // transaction id
      if (!id) throw new Error(`Please enter transaction ID.`);

      const res = await printLogs.printCounts(id);

      // get the print location of the transaction;
      let location = [];
      const printLoc = await printLogs.getPrintLocations(id);
      for (let i = 0; i < printLoc.length; i++) {
        const e = printLoc[i];
        location.push(e);
      }
      res[0].location = location; // append
      return res;
    } else {
      // offline
    }
  };
};

module.exports = printCount;

