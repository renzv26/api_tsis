const moment = require("moment-timezone");
const escpos = require("../../../hoi-escpos");
escpos.USB = require("escpos-usb");

const { transactions } = require("../../data-access/sl-layer/transactions/app")
const { printLogs } = require("../../data-access/sl-layer/print-logs/app");
const { validateAccessRights } = require("../../validator/app"); //validator

// ###########################
const insertPrintLogs = require("./insert-print-logs");
const printCount = require("./print-count");
const printLogsUpdate = require("./print-logs-update");
const printLogsSelect = require("./print-logs-select");
// ###########################
const insertPrintLogss = insertPrintLogs({
  validateAccessRights,
  printLogs,
  moment,
  escpos,
  transactions
});
const printCounts = printCount({ validateAccessRights, printLogs });
const printLogsUpdates = printLogsUpdate({ printLogs, moment });
const printLogsSelects = printLogsSelect({
  validateAccessRights,
  printLogs,
  moment,
});
// ###########################
const services = Object.freeze({
  insertPrintLogss,
  printCounts,
  printLogsUpdates,
  printLogsSelects,
});

module.exports = services;
module.exports = {
  insertPrintLogss,
  printCounts,
  printLogsUpdates,
  printLogsSelects,
};
