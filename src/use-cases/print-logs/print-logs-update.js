const printLogsUpdate = ({ printLogs, moment }) => {
  return async function put({ id, ...info } = {}) {
    const mode = info.mode;
    const modules = info.modules;

    delete info.mode;
    delete info.modules;
    delete info.source;

    if (mode == 1) {
      // online mode
      const { cookie } = info;

      const d = new Date().toDateString();
      const date = moment(d).format("YYYY-MM-DD");
      const t = new Date().toTimeString();
      const time = await fixTime(t);

      // make batch req
      let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

      for (let i = 0; i < info.settings.length; i++) {
        const e = info.settings[i];

        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_BFI_TS_TRNSTYPE('${e.id}')

{
  "U_TS_PRINT_COUNT": "${e.print_count}"
}
`;
      }

      batchStr += `
--b--
--a--        
`;

      //   check if status is change to approved; loop 5 times;
      const data = {
        batchStr,
        cookie,
      };

      const res = await printLogs.batchRequest(data);
      return res;
    } else {
      // offline mode
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = printLogsUpdate;
