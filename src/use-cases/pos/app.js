const posDb = require("../../data-access/db-layer/pos/app"); //db

const selectAllPos = require("./select-all-pos");

const uc_selectAllPos = selectAllPos({ posDb });

const services = Object.freeze({
  uc_selectAllPos
});

module.exports = services;
module.exports = {
  uc_selectAllPos
};
