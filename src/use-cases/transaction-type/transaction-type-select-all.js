const transactionTypeSelectAll = ({ transactionTypeDb, transactionTypes }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      if (info.isLogs) {
        const res = await transactionTypes.transactionTypesSelectAll({ info });
        let data = []; // to push
        for (let i = 0; i < res.length; i++) {
          const e = res[i];

          data.push({
            created_at: `${e.U_TS_CREATEDATE} ${e.U_TS_CREATETIME}`,
            // created_by: 1,
            id: e.Code,
            name: e.U_TS_TRNSTYPE,
            print_count: e.U_TS_PRINT_COUNT,
            updated_at: `${e.U_TS_UPDATEDATE} ${e.U_TS_UPDATETIME}`,
            // updated_by: 1,
          });
        }

        return data;
      }
      // default
      const res = await transactionTypes.transactionTypesSelectAll({ info });
      return res;
    } else {
      const result = await transactionTypeDb.selectAllTransactionType();
      const ttype = result.rows;

      return ttype;
    }
  };
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

module.exports = transactionTypeSelectAll;
