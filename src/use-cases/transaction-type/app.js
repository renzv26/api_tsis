const { makeTransactionType } = require("../../entities/transaction-type/app"); // entity
const transactionTypeDb = require("../../data-access/db-layer/transaction-type/app"); //db
const { insertActivityLogss } = require("../users/app"); // logs

const { transactionTypes } = require("../../data-access/sl-layer/transaction-type/app")

//#######################
const addNewTransactionType = require("./transaction-type-add");
const transactionTypeSelectAll = require("./transaction-type-select-all");
const transactionTypeSelectOne = require("./transaction-type-select-one");
const updateTransactionType = require("./transaction-type-update");
//#######################
const addNewTransactionTypes = addNewTransactionType({
  transactionTypeDb,
  makeTransactionType,
  insertActivityLogss
});
const transactionTypeSelectAlls = transactionTypeSelectAll({
  transactionTypeDb,
  transactionTypes
});
const transactionTypeSelectOnes = transactionTypeSelectOne({
  transactionTypeDb
});
const updateTransactionTypes = updateTransactionType({
  transactionTypeDb,
  makeTransactionType,
  insertActivityLogss
});

const services = Object.freeze({
  addNewTransactionTypes,
  transactionTypeSelectAlls,
  transactionTypeSelectOnes,
  updateTransactionTypes
});

module.exports = services;
module.exports = {
  addNewTransactionTypes,
  transactionTypeSelectAlls,
  transactionTypeSelectOnes,
  updateTransactionTypes
};
