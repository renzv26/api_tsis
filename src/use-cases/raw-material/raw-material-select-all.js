const rawMaterialSelectAll = ({ rawMats, users }) => {
  return async function selects(info) {
    const { mode } = info;

    if (mode == 1) {
      const data = [];

      const login = await users.userLogin();

      const sessionId = `B1SESSION=${login.SessionId}`
      info.cookie = sessionId;


      const res = await rawMats.selectAllItems({info});
      for (let i = 0; i < res.length; i++) {
        const e = res[i];

        // if item name is not null
        if (e.ItemName) data.push(e);
      }

      return data;
    } else {
      // offline mode
    }
  };
};

module.exports = rawMaterialSelectAll;
