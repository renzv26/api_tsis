const updateRawMaterial = ({
  rawMaterialDb,
  makeRawMaterialUpdate,
  insertActivityLogss,
  decrypt
}) => {
  return async function put({ id, ...info } = {}) {
    const result = makeRawMaterialUpdate(info);

    const rawMaterialExist = await rawMaterialDb.selectByNameUpdate({
      description: result.getDescription(),
      id
    });

    if (rawMaterialExist.rowCount !== 0) {
      throw new Error("The raw material already exist.");
    }

    // query previous values
    const previous = await rawMaterialDb.selectOneRawMaterials({ id });
    const prev_data = previous.rows[0];
    const prev_values = {
      id: prev_data.id,
      description: prev_data.description,
      created_by: {
        createdfn: prev_data.createdfn ? decrypt(prev_data.createdfn) : "",
        createdln: prev_data.createdln ? decrypt(prev_data.createdln) : "",
        created_at: prev_data.created_at
      },
      updated_by: {
        updatedfn: prev_data.updatedfn ? decrypt(prev_data.updatedfn) : "",
        updatedln: prev_data.updatedln ? decrypt(prev_data.updatedln) : "",
        updated_at: prev_data.updated_at
      }
    };

    // update to db
    const update = await rawMaterialDb.updateRawMaterial({
      description: result.getDescription(),
      updated_by: result.getUpdatedBy(),
      updated_at: result.getUpdatedAt(),
      id
    });

    const count = update.rowCount; // get the number of updated data

    const data = {
      msg: `Updated successfully ${count} raw material.`
    };

    // logs
    const user = await rawMaterialDb.returnCreatedBy({
      id: result.getUpdatedBy()
    });
    const create = user.rows[0];
    const modified_by = {
      id: create.id,
      employee_id: create.employee_id ? decrypt(create.employee_id) : "",
      firstname: create.firstname ? decrypt(create.firstname) : "",
      lastname: create.lastname ? decrypt(create.lastname) : ""
    };

    // logs
    // new values
    const new_values = {
      description: result.getDescription(),
      updated_at: result.getUpdatedAt(),
      modified_by
    };

    const logs = {
      action_type: "UPDATE RAW MATERIAL",
      table_affected: "ts_raw_materials",
      new_values,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = updateRawMaterial;
