const {
  makeRawMaterial,
  makeRawMaterialUpdate,
} = require("../../entities/raw-material/app"); // entity
const rawMaterialDb = require("../../data-access/db-layer/raw-material/app"); //db
const { decrypt } = require("../../../crypting/app"); //decrypt
const { insertActivityLogss } = require("../users/app"); // logs
const { rawMats } = require("../../data-access/sl-layer/raw-mats/app");
const { users } = require("../../data-access/sl-layer/users/app");

//#######################
const addNewRawMaterial = require("./raw-material-add");
const rawMaterialSelectAll = require("./raw-material-select-all");
const rawMaterialSelectOne = require("./raw-material-select-one");
const updateRawMaterial = require("./raw-material-update");
//#######################
const addNewRawMaterials = addNewRawMaterial({
  rawMaterialDb,
  makeRawMaterial,
  insertActivityLogss,
  decrypt,
});
const rawMaterialSelectAlls = rawMaterialSelectAll({ rawMats, users });
const rawMaterialSelectOnes = rawMaterialSelectOne({ rawMaterialDb, decrypt });
const updateRawMaterials = updateRawMaterial({
  rawMaterialDb,
  makeRawMaterialUpdate,
  insertActivityLogss,
  decrypt,
});

const services = Object.freeze({
  addNewRawMaterials,
  rawMaterialSelectAlls,
  rawMaterialSelectOnes,
  updateRawMaterials,
});

module.exports = services;
module.exports = {
  addNewRawMaterials,
  rawMaterialSelectAlls,
  rawMaterialSelectOnes,
  updateRawMaterials,
};
