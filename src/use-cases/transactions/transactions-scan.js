const scanTransaction = ({
  drivers,
  transactions,
  transactionTypes,
  weightTypes,
  truckInfos,
  validateAccessRights,
  moment,
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }
    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "add transaction"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }
// console.log('infotttttttttttt', info);
    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      validateRequestBody({ info });

      // date and time
      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      const time = await fixTime(t);

      // const getcurrentSeries = await transactions.getDocEntryForSeriesNumber();
      // const ticket_number = `${info.truckscale_id}-${moment(d).format("MM")}${moment(d).format("YYYY")}-${primaryKeyNextValDetails.rows[0].new_id}-${transactionEnvironment}`
      // const currentSeries = getcurrentSeries[0].DocEntry + 1;
      // console.log(getcurrentSeries[0].DocEntry + 1, "CurrentSeries Plus 1")
      // console.log(info, "INFOOOOO SCAN")
    // if (info.company === 'unahco') {
    //   let str = "" + currentSeries
    //   let pad = "C000000"
    //   let ans = pad.substring(0, pad.length - str.length) + str
    //   info.ticket_number = `${ans}`
    // } else {
    //   info.ticket_number = `${info.truckscale_number}-${moment(d).format("MM")}${moment(d).format("YYYY")}-${currentSeries}`
    // } 
    // else {
    //   let str = "" + currentSeries
    //   let pad = "O-0000"
    //   let ans = pad.substring(0, pad.length - str.length) + str
    //   const ticket_number = `${ans}-${transactionEnvironment}`
    // }

      // console.log("@COMPANY",info.company);
      //  console.log(info, "FIX Napier Supp");
      if (
        info.transactionType === "delivery" ||
        info.transactionType === "others"
      ) {
        //get transaction details
        const transactionDetails = await transactions.transactionsSelectOneByTrackingCode(
          { info }
        );
        // expire session;
        if (transactionDetails.status == 401) {
          throw new Error("Session expired. Please login again.");
        } else {
          //if transaction does not exist
          if (transactionDetails.data.value.length === 0) {
            //declare variables
            let driverId,
              truckInfoId,
              transactionTypeId,
              grossWeightId,
              tareWeightId;

            //get transaction type details
            const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName(
              {
                U_TS_TRNSTYPE: info.transactionType,
                cookie: info.cookie,
              }
            );         
            // console.log(transactionDetails,"TRANSACTION DETAILSS")
            const printCount = transactionTypeDetails[0].U_TS_PRINT_COUNT;
            //get transaction type id
            transactionTypeId = transactionTypeDetails[0].Code;
            //ticket number incremented from last valu DocEntry
            
            const currentSeries = await transactions.getDocEntryForSeriesNumber();
            const ticket_number = `${info.truckscale_number}-${moment(date).format("MM")}${moment(date).format("YYYY")}-${currentSeries[0].new_id}-N`
            
            //get gross weight details
            const grossWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "gross",
                cookie: info.cookie,
              }
            );

            //get tare weight details
            const tareWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "tare",
                cookie: info.cookie,
              }
            );

            //get gross weight Id
            grossWeightId = grossWeightDetails[0].Code;

            //get tare weight Id
            tareWeightId = tareWeightDetails[0].Code;

            //get truck info details
            const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
              { info }
            );

            //check if plate number exist
            if (truckInfoDetails.length === 0) {
              throw new Error(
                JSON.stringify({
                  code: 1,
                  message: "Plate number does not exist",
                })
              );
            } else {
              truckInfoId = truckInfoDetails[0].Code;
            }

            //split driver to get firstname and lastname
            const driverDetailsFromBody = info.driver.split(", ");

            if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
              throw new Error(`Invalid driver name format`);
            }

            const driverFirstName = driverDetailsFromBody[1].toLowerCase();
            const driverLastName = driverDetailsFromBody[0].toLowerCase();

            //check driver if exist or not
            const checkDriver = await drivers.driversAddSelectByName({
              info: {
                U_TS_FN: driverFirstName,
                U_TS_LN: driverLastName,
                cookie: info.cookie,
              },
            });

            
            if (checkDriver.length > 0) {
              driverId = checkDriver[0].Code;
            } else {
              //get Driver Max Code
              const driverMax = await drivers.driversGetMaxCode({});
              const driverMaxCode = driverMax[0].maxCode;

              //insert driver
              const insertDriver = await drivers.driversAdd({
                info: {
                  Code: driverMaxCode,
                  Name: driverMaxCode,
                  U_TS_FN: driverFirstName,
                  U_TS_LN: driverLastName,
                  U_TS_CREATEDATE: date,
                  U_TS_CREATETIME: time,
                  U_TS_UPDATEDATE: date,
                  U_TS_UPDATETIME: time,
                  U_CREATED_BY: info.created_by,
                  U_UPDATED_BY: info.created_by,
                  cookie: info.cookie,
                },
              });

              if (insertDriver.status == 201) {
                driverId = driverMaxCode;
              } else {
                throw new Error(`Insert driver failed`);
              }
            }

            //build final object; determine if napier or not
            let deliveryTransactionDetails = {};
            if (info.isNapier) {
               console.log("wew");
              // console.log(info);
              // if true; means napier;
              deliveryTransactionDetails = {
                U_TS_TRNS_TYPE: transactionTypeId,
                U_TS_STATUS: "for inbound",
                U_TS_NUM_BAGS: info.no_of_bags,
                U_TS_TRCK_CODE: info.trackingCode,
                U_TS_LOC_DLVRY: info.locationDelivery,
                U_TS_SUPPLIER:info.supplier_name,
                U_TS_SUPPLIER_ADD:info.supplier_address,
                U_TS_ITEMNAME:info.item,
                U_TS_IS_NAPIER: 1,
                U_COMPANY: info.company ? info.company : null,
                U_TS_SUPPLIER:info.supplier_name,
                U_TS_SUPPLIER_ADD:info.supplier_address,
                U_PRINT_COUNT: printCount,
                U_TS_TICKETNUMBER: ticket_number,
                U_TS_DRNUMBER: info.dr_number,
                U_TS_PLOT_CODE: info.plot_code,
                U_CREATED_BY: info.created_by,
                U_UPDATED_BY: info.created_by,
                U_UOM: "KILOGRAM",
                U_UOM_TS: info.uom,
                BFI_TS_IBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                    U_TS_UPDATEDATE: info.created_date,
                    U_TS_UPDATETIME: info.created_time,
                  },
                ],
                BFI_TS_OBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                    U_TS_UPDATEDATE: info.created_date,
                    U_TS_UPDATETIME: info.created_time,
                  },
                ],
                cookie: info.cookie,
              };
            } else {
              // console.log("HEH");
              deliveryTransactionDetails = {
                U_TS_TRNS_TYPE: transactionTypeId,
                U_TS_STATUS: "for inbound",
                U_TS_NUM_BAGS: info.no_of_bags,
                U_TS_TRCK_CODE: info.trackingCode,
                U_TS_LOC_DLVRY: info.locationDelivery,
                U_TS_SUPPLIER:info.supplier_name,
                U_TS_SUPPLIER_ADD:info.supplier_address,
                U_TS_ITEMNAME:info.item,
                U_TS_IS_NAPIER: 0,
                U_COMPANY: info.company ? info.company : null,
                U_PRINT_COUNT: printCount,
                U_TS_TICKETNUMBER: ticket_number,
                U_TS_DRNUMBER: info.dr_number,
                U_TS_PLOT_CODE: info.plot_code,
                U_CREATED_BY: info.created_by,
                U_UPDATED_BY: info.created_by,
                U_UOM: "KILOGRAM",
                U_UOM_TS: info.uom,
                BFI_TS_IBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_TS_TRKSCL_ID: info.truckscale_id,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                    U_TS_UPDATEDATE: info.created_date,
                    U_TS_UPDATETIME: info.created_time,
                  },
                ],
                BFI_TS_OBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_TRKSCL_ID: info.truckscale_id,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                    U_TS_UPDATEDATE: info.created_date,
                    U_TS_UPDATETIME: info.created_time,
                  },
                ],
                cookie: info.cookie,
              };
            }
            // console.log(deliveryTransactionDetails, "DELIVERYDETAILS")
            // console.log("WT",deliveryTransactionDetails);
            const insertTransaction = await transactions.transactionsAdd(
              deliveryTransactionDetails
            );

            if (insertTransaction.status == 201) {
              return { transactionId: insertTransaction.data.DocEntry };
            } else {
              throw new Error(`Insert transaction failed`);
            }
          } else {
            //if transaction does exist
            const i = transactionDetails.data.value.length
            if(transactionDetails.data.value[0].U_IS_DONE == 1){
              throw new Error(`Transaction already completed.`)
            }

            //if transaction is completed
            if (transactionDetails.data.value[0].U_TS_STATUS === "completed") {
              if (info.specialCaseNewTransactionFlagTrue === 1) {
                // //get previous data
                // const prevInbound =
                //   transactionDetails.data.value[0].BFI_TS_IBTRNCollection[
                //     transactionDetails.data.value[0].BFI_TS_IBTRNCollection
                //       .length - 1
                //   ];
                // const prevOutbound =
                //   transactionDetails.data.value[0].BFI_TS_OBTRNCollection[
                //     transactionDetails.data.value[0].BFI_TS_OBTRNCollection
                //       .length - 1
                //   ];

                // //build final object
                // const deliverySpecialCaseTransactionDetails = {
                //   id: transactionDetails.data.value[0].DocEntry,
                //   U_TS_STATUS: "second scale",
                //   U_UPDATED_BY: info.created_by,
                //   BFI_TS_IBTRNCollection: [
                //     {
                //       U_TS_WEIGHT_ID: prevInbound.U_TS_WEIGHT_ID,
                //       U_TS_CREATEDBY: prevInbound.U_TS_CREATEDBY,
                //       U_UPDATED_BY: prevInbound.U_TS_CREATEDBY,
                //       U_TS_DRIVERID: prevInbound.U_TS_DRIVERID,
                //       // U_TS_IB_WEIGHT: prevOutbound.U_TS_OB_WEIGHT,
                //       // U_TS_REMARKS: prevOutbound.U_TS_REMARKS,
                //       U_TS_TRKINFO: prevInbound.U_TS_TRKINFO,
                //       U_TS_CREATEDATE: prevInbound.U_TS_CREATEDATE,
                //       U_TS_CREATETIME: prevInbound.U_TS_CREATETIME,
                //       U_TS_UPDATEDATE: prevInbound.U_TS_CREATEDATE,
                //       U_TS_UPDATETIME: prevInbound.U_TS_CREATETIME,
                //     },
                //   ],
                //   BFI_TS_OBTRNCollection: [
                //     {
                //       U_TS_WEIGHT_ID: prevOutbound.U_TS_WEIGHT_ID,
                //       U_TS_CREATEDBY: prevOutbound.U_TS_CREATEDBY,
                //       U_UPDATED_BY: prevOutbound.U_TS_CREATEDBY,
                //       U_TS_DRIVERID: prevOutbound.U_TS_DRIVERID,
                //       U_TS_TRKINFO: prevOutbound.U_TS_TRKINFO,
                //       U_TS_CREATEDATE: prevOutbound.U_TS_CREATEDATE,
                //       U_TS_CREATETIME: prevOutbound.U_TS_CREATETIME,
                //       U_TS_UPDATEDATE: prevOutbound.U_TS_CREATEDATE,
                //       U_TS_UPDATETIME: prevOutbound.U_TS_CREATETIME,
                //     },
                //   ],
                //   cookie: info.cookie,
                // };

                // // console.log('D',deliverySpecialCaseTransactionDetails);
                // const updateTransaction = await transactions.transactionsUpdate(
                //   deliverySpecialCaseTransactionDetails
                // );

                // return {
                //   transactionId: transactionDetails.data.value[0].DocEntry,
                // };
                //declare variables
                  let driverId,
                  truckInfoId,
                  transactionTypeId,
                  grossWeightId,
                  tareWeightId;

                //get transaction type details
                const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName(
                  {
                    U_TS_TRNSTYPE: info.transactionType,
                    cookie: info.cookie,
                  }
                );
                // console.log(transactionDetails,"TRANSACTION DETAILSS")
                const printCount = transactionTypeDetails[0].U_TS_PRINT_COUNT;
                //get transaction type id
                transactionTypeId = transactionTypeDetails[0].Code;
                //ticket number incremented from last valu DocEntry
                const currentSeries = await transactions.getDocEntryForSeriesNumber();
                const ticket_number = `${info.truckscale_number}-${moment(date).format("MM")}${moment(date).format("YYYY")}-${currentSeries[0].new_id}-N`
          
                //get gross weight details
                const grossWeightDetails = await weightTypes.weightTypesSelectByName(
                  {
                    U_TS_WEIGHT: "gross",
                    cookie: info.cookie,
                  }
                );

                //get tare weight details
                const tareWeightDetails = await weightTypes.weightTypesSelectByName(
                  {
                    U_TS_WEIGHT: "tare",
                    cookie: info.cookie,
                  }
                );

                //get gross weight Id
                grossWeightId = grossWeightDetails[0].Code;

                //get tare weight Id
                tareWeightId = tareWeightDetails[0].Code;

                //get truck info details
                const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
                  { info }
                );

                //check if plate number exist
                if (truckInfoDetails.length === 0) {
                  const max = await truckInfos.truckInfosGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertTruckInfo = await truckInfos.truckInfosAddAuto({
              Code: maxCode,
              Name: maxCode,
              U_TS_TRTYPE_ID: 1,
              U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.createdby,
              U_UPDATED_BY: info.createdby,
              cookie: info.cookie
           })

           if (insertTruckInfo.status == 201) {
            truckInfoId = maxCode;
          } else {
            throw new Error(`Insert plate number failed`);
          }
          //  const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          //   { info }
          // );

          // truckInfoId = truckInfoDetails.data[0].Code;
          // throw new Error(
          //   JSON.stringify({
          //     code: 1,
          //     message: "Plate number does not exist",
          //   })
          // );
                } else {
                  truckInfoId = truckInfoDetails[0].Code;
                }

                //split driver to get firstname and lastname
                const driverDetailsFromBody = info.driver.split(", ");

                if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
                  throw new Error(`Invalid driver name format`);
                }

                const driverFirstName = driverDetailsFromBody[1].toLowerCase();
                const driverLastName = driverDetailsFromBody[0].toLowerCase();

                //check driver if exist or not
                const checkDriver = await drivers.driversAddSelectByName({
                  info: {
                    U_TS_FN: driverFirstName,
                    U_TS_LN: driverLastName,
                    cookie: info.cookie,
                  },
                });

                if (checkDriver.length > 0) {
                  driverId = checkDriver[0].Code;
                } else {
                  //get Driver Max Code
                  const driverMax = await drivers.driversGetMaxCode({});
                  const driverMaxCode = driverMax[0].maxCode;

                  //insert driver
                  const insertDriver = await drivers.driversAdd({
                    info: {
                      Code: driverMaxCode,
                      Name: driverMaxCode,
                      U_TS_FN: driverFirstName,
                      U_TS_LN: driverLastName,
                      U_TS_CREATEDATE: date,
                      U_TS_CREATETIME: time,
                      U_TS_UPDATEDATE: date,
                      U_TS_UPDATETIME: time,
                      U_CREATED_BY: info.created_by,
                      U_UPDATED_BY: info.created_by,
                      cookie: info.cookie,
                    },
                  });

                  if (insertDriver.status == 201) {
                    driverId = driverMaxCode;
                  } else {
                    throw new Error(`Insert driver failed`);
                  }
                }

                //build final object; determine if napier or not
                let deliveryTransactionDetails = {};
                if (info.isNapier) {
                  console.log("wew");
                  // console.log(info);
                  // if true; means napier;
                  deliveryTransactionDetails = {
                    U_TS_TRNS_TYPE: transactionTypeId,
                    U_TS_STATUS: "for inbound",
                    U_TS_NUM_BAGS: info.no_of_bags,
                    U_TS_TRCK_CODE: info.trackingCode,
                    U_TS_LOC_DLVRY: info.locationDelivery,
                    U_TS_SUPPLIER:info.supplier_name,
                    U_TS_SUPPLIER_ADD:info.supplier_address,
                    U_TS_ITEMNAME:info.item,
                    U_TS_IS_NAPIER: 1,
                    U_COMPANY: info.company ? info.company : null,
                    U_TS_SUPPLIER:info.supplier_name,
                    U_TS_SUPPLIER_ADD:info.supplier_address,
                    U_PRINT_COUNT: printCount,
                    U_TS_TICKETNUMBER: ticket_number,
                    U_TS_DRNUMBER: info.dr_number,
                    U_TS_PLOT_CODE: info.plot_code,
                    U_CREATED_BY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_UOM: "KILOGRAM",
                    U_UOM_TS: info.uom,
                    BFI_TS_IBTRNCollection: [
                      {
                        U_TS_WEIGHT_ID: grossWeightId,
                        U_TS_CREATEDBY: info.created_by,
                        U_UPDATED_BY: info.created_by,
                        U_TS_DRIVERID: driverId,
                        U_TS_TRKINFO: truckInfoId,
                        U_TS_CREATEDATE: info.created_date,
                        U_TS_CREATETIME: info.created_time,
                        U_TS_UPDATEDATE: info.created_date,
                        U_TS_UPDATETIME: info.created_time,
                      },
                    ],
                    BFI_TS_OBTRNCollection: [
                      {
                        U_TS_WEIGHT_ID: tareWeightId,
                        U_TS_CREATEDBY: info.created_by,
                        U_UPDATED_BY: info.created_by,
                        U_TS_DRIVERID: driverId,
                        U_TS_TRKINFO: truckInfoId,
                        U_TS_CREATEDATE: info.created_date,
                        U_TS_CREATETIME: info.created_time,
                        U_TS_UPDATEDATE: info.created_date,
                        U_TS_UPDATETIME: info.created_time,
                      },
                    ],
                    cookie: info.cookie,
                  };
                } else {
                  // console.log("HEH");
                  deliveryTransactionDetails = {
                    U_TS_TRNS_TYPE: transactionTypeId,
                    U_TS_STATUS: "for inbound",
                    U_TS_NUM_BAGS: info.no_of_bags,
                    U_TS_TRCK_CODE: info.trackingCode,
                    U_TS_LOC_DLVRY: info.locationDelivery,
                    U_TS_SUPPLIER:info.supplier_name,
                    U_TS_SUPPLIER_ADD:info.supplier_address,
                    U_TS_ITEMNAME:info.item,
                    U_APP_PO_ID: info.purchase_order_id,
                    U_TS_IS_NAPIER: 0,
                    U_COMPANY: info.company ? info.company : null,
                    U_PRINT_COUNT: printCount,
                    U_TS_TICKETNUMBER: ticket_number,
                    U_TS_DRNUMBER: info.dr_number,
                    U_TS_PLOT_CODE: info.plot_code,
                    U_CREATED_BY: info.created_by,
                    U_UPDATED_BY: info.created_by,
                    U_UOM: "KILOGRAM",
                    U_UOM_TS: info.uom,
                    BFI_TS_IBTRNCollection: [
                      {
                        U_TS_WEIGHT_ID: grossWeightId,
                        U_TS_CREATEDBY: info.created_by,
                        U_UPDATED_BY: info.created_by,
                        U_TS_TRKSCL_ID: info.truckscale_id,
                        U_TS_DRIVERID: driverId,
                        U_TS_TRKINFO: truckInfoId,
                        U_TS_CREATEDATE: info.created_date,
                        U_TS_CREATETIME: info.created_time,
                        U_TS_UPDATEDATE: info.created_date,
                        U_TS_UPDATETIME: info.created_time,
                      },
                    ],
                    BFI_TS_OBTRNCollection: [
                      {
                        U_TS_WEIGHT_ID: tareWeightId,
                        U_TS_CREATEDBY: info.created_by,
                        U_UPDATED_BY: info.created_by,
                        U_TS_DRIVERID: driverId,
                        U_TS_TRKINFO: truckInfoId,
                        U_TS_TRKSCL_ID: info.truckscale_id,
                        U_TS_CREATEDATE: info.created_date,
                        U_TS_CREATETIME: info.created_time,
                        U_TS_UPDATEDATE: info.created_date,
                        U_TS_UPDATETIME: info.created_time,
                      },
                    ],
                    cookie: info.cookie,
                  };
                }
                // console.log(deliveryTransactionDetails, "DELIVERYDETAILS")
                // console.log("WT",deliveryTransactionDetails);
                const insertTransaction = await transactions.transactionsAdd(
                  deliveryTransactionDetails
                );

                if (insertTransaction.status == 201) {
                  return { transactionId: insertTransaction.data.DocEntry };
                } else {
                  throw new Error(`Insert transaction failed`);
                }
              } else {

                // if (
                //   transactionDetails.data.value.length != 1
                // ) {
                //   throw new Error(
                //     JSON.stringify({
                //       code: 4,
                //       message: "Transaction is for double scale.",
                //     })
                //   );
                // }
  
                throw new Error(
                  JSON.stringify({
                    code: 2,
                    message: "Transaction already completed",
                    isNapier: transactionDetails.data.value[0].U_TS_IS_NAPIER,
                  })
                );
              }
            } else {
              let isNapier = 0;
              if (info.isNapier == true) {
                isNapier = 1;
              }

              if (
                transactionDetails.data.value[0].U_TS_IS_NAPIER !== isNapier
              ) {
                throw new Error(
                  JSON.stringify({
                    code: 3,
                    message: "Not allowed to transact here.",
                  })
                );
              }

              return {
                transactionId: transactionDetails.data.value[0].DocEntry,
              };
            }
          }
        }
      } else {
        // withdrawal
        // get transaction details
        const transactionDetails = await transactions.transactionsSelectOneByTrackingCode(
          { info }
        );
        // expire session;
        if (transactionDetails.status == 401) {
          throw new Error("Session expired. Please login again.");
        } else {
          //if transaction does not exist
          if (transactionDetails.data.value.length === 0) {
            //declare variables
            let driverId,
              truckInfoId,
              transactionTypeId,
              grossWeightId,
              tareWeightId;

            //get transaction type details
            const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName(
              {
                U_TS_TRNSTYPE: info.transactionType,
                cookie: info.cookie,
              }
            );

            //get transaction type id
            transactionTypeId = transactionTypeDetails[0].Code;
            const printCount = transactionTypeDetails[0].U_TS_PRINT_COUNT;

            //ticket number incremented from last valu DocEntry
            const currentSeries = await transactions.getDocEntryForSeriesNumber();
            console.log(currentSeries, "SERIES")
            const ticket_number = `${info.truckscale_number}-${moment(date).format("MM")}${moment(date).format("YYYY")}-${currentSeries[0].new_id}-N`

            //get gross weight details
            const grossWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "gross",
                cookie: info.cookie,
              }
            );

            //get tare weight details
            const tareWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "tare",
                cookie: info.cookie,
              }
            );

            //get gross weight Id
            grossWeightId = grossWeightDetails[0].Code;

            //get tare weight Id
            tareWeightId = tareWeightDetails[0].Code;

            //get truck info details
            const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
              { info }
            );

            //check if plate number exist
            if (truckInfoDetails.length === 0) {
              throw new Error(
                JSON.stringify({
                  code: 1,
                  message: "Plate number does not exist",
                })
              );
            } else {
              truckInfoId = truckInfoDetails[0].Code;
            }

            //split driver to get firstname and lastname
            const driverDetailsFromBody = info.driver.split(", ");

            if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
              throw new Error(`Invalid driver name format`);
            }

            const driverFirstName = driverDetailsFromBody[1].toLowerCase();
            const driverLastName = driverDetailsFromBody[0].toLowerCase();

            //check driver if exist or not
            const checkDriver = await drivers.driversAddSelectByName({
              info: {
                U_TS_FN: driverFirstName,
                U_TS_LN: driverLastName,
                cookie: info.cookie,
              },
            });

            if (checkDriver.length > 0) {
              driverId = checkDriver[0].Code;
            } else {
              //get Driver Max Code
              const driverMax = await drivers.driversGetMaxCode({});
              const driverMaxCode = driverMax[0].maxCode;

              //insert driver
              const insertDriver = await drivers.driversAdd({
                info: {
                  Code: driverMaxCode,
                  Name: driverMaxCode,
                  U_TS_FN: driverFirstName,
                  U_TS_LN: driverLastName,
                  U_TS_CREATEDATE: date,
                  U_TS_CREATETIME: time,
                  U_TS_UPDATEDATE: date,
                  U_TS_UPDATETIME: time,
                  U_CREATED_BY: info.created_by,
                  U_UPDATED_BY: info.created_by,
                  cookie: info.cookie,
                },
              });

              if (insertDriver.status == 201) {
                driverId = driverMaxCode;
              } else {
                throw new Error(`Insert driver failed`);
              }
            }

            // to post;
            let withdrawalObject = {};

            withdrawalObject = {
              U_TS_TRNS_TYPE: transactionTypeId,
              U_TS_STATUS: "for inbound",
              U_TS_NUM_BAGS: info.no_of_bags,
              U_TS_TRCK_CODE: info.trackingCode,
              U_TS_LOC_DLVRY: info.locationDelivery,
              U_TS_SUPPLIER:info.supplier_name,
              U_TS_SUPPLIER_ADD:info.supplier_address,
              U_TS_ITEMNAME:info.item,
              U_TS_IS_NAPIER: 0,
              //  U_COMPANY: "Biotech Farms Inc.",
              U_COMPANY: info.company ? info.company : null,
              U_APP_SO_ID: info.so,
              U_PRINT_COUNT: printCount,
              U_TS_TICKETNUMBER: ticket_number,
              U_CREATED_BY: info.created_by,
              U_UPDATED_BY: info.created_by,
              U_UOM: "KILOGRAM",
              U_UOM_TS: info.uom,
              BFI_TS_IBTRNCollection: [
                {
                  U_TS_WEIGHT_ID: grossWeightId,
                  U_TS_CREATEDBY: info.created_by,
                  U_UPDATED_BY: info.created_by,
                  U_TS_DRIVERID: driverId,
                  U_TS_TRKINFO: truckInfoId,
                  U_TS_CREATEDATE: info.created_date,
                  U_TS_CREATETIME: info.created_time,
                  U_TS_UPDATEDATE: info.created_date,
                  U_TS_UPDATETIME: info.created_time,
                },
              ],
              BFI_TS_OBTRNCollection: [
                {
                  U_TS_WEIGHT_ID: tareWeightId,
                  U_TS_CREATEDBY: info.created_by,
                  U_UPDATED_BY: info.created_by,
                  U_TS_DRIVERID: driverId,
                  U_TS_TRKINFO: truckInfoId,
                  U_TS_CREATEDATE: info.created_date,
                  U_TS_CREATETIME: info.created_time,
                  U_TS_UPDATEDATE: info.created_date,
                  U_TS_UPDATETIME: info.created_time,
                },
              ],
              cookie: info.cookie,
            };

            // console.log('W',withdrawalObject);
            const insertTransaction = await transactions.transactionsAdd(
              withdrawalObject
            );

            if (insertTransaction.status == 201) {
              return { transactionId: insertTransaction.data.DocEntry };
            } else {
              throw new Error(`Insert transaction failed`);
            }

            // end insert withdrawal transaction
          } else {
            // transaction does exist

            //  if transaction is completed
            if (transactionDetails.data.value[0].U_TS_STATUS === "completed") {
              throw new Error(
                JSON.stringify({
                  code: 2,
                  message: "Transaction already completed",
                })
              );
            } else {
              // transaction not yet completed
              return {
                transactionId: transactionDetails.data.value[0].DocEntry,
              };
            }
          }
        }
      }
    } else {
      // offline mode
    }
  };
};

const validateRequestBody = ({ info }) => {
  const {} = info;
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = scanTransaction;
