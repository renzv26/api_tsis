const requestSelect = ({ transactions, validateAccessRights, moment }) => {
  return async function selects(info) {
    // for access rights
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "request",
      "view requests"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      // select one for socket io; used only in use-case
      if (info.isSocket) {
        const res = await transactions.selectRequests({ info });
        let data = []; // array to fill

        for (let i = 0; i < res.length; i++) {
          const e = res[i];
          const cdate = moment(e.U_CREATEDATE).format("YYYY-MM-DD");
          const ctime = await intToTime(e.U_CREATETIME);
          const udate = moment(e.U_UPDATEDATE).format("YYYY-MM-DD");
          const utime = await intToTime(e.U_UPDATETIME);

          data.push({
            created_at: `${cdate} ${ctime}`,
            created_by: e.U_CREATED_BY,
            draft_data: e.U_DRAFT_DATA ? e.U_DRAFT_DATA.toString() : null,
            id: e.Code,
            is_edit: e.U_IS_EDIT ? true : false,
            is_reprint: e.U_IS_REPRINT ? true : false,
            print_count: e.U_PRINT_COUNT ? e.U_PRINT_COUNT : 0,
            request_reason: e.U_REQUEST_REASON ? e.U_REQUEST_REASON : "",
            requester_name: e.REQUESTER_NAME,
            status: e.U_STATUS,
            ticket_number: e.U_TS_TICKETNUMBER,
            transaction_id: e.U_TRANSCTION_ID,
            updated_at: `${udate} ${utime}`,
            updated_by: e.U_UPDATED_BY,
            current_print_count: e.CURRENT_COUNT ? e.CURRENT_COUNT : 0,
          });
        }

        return data;
      }

      // default select all
      const res = await transactions.selectRequests({ info });
      let data = []; // array to fill
      for (let i = 0; i < res.length; i++) {
        const e = res[i];
        const cdate = moment(e.U_CREATEDATE).format("YYYY-MM-DD");
        const ctime = await intToTime(e.U_CREATETIME);
        const udate = moment(e.U_UPDATEDATE).format("YYYY-MM-DD");
        const utime = await intToTime(e.U_UPDATETIME);

        data.push({
          created_at: `${cdate} ${ctime}`,
          created_by: e.U_CREATED_BY,
          draft_data: e.U_DRAFT_DATA ? e.U_DRAFT_DATA.toString() : null,
          id: e.Code,
          is_edit: e.U_IS_EDIT ? true : false,
          is_reprint: e.U_IS_REPRINT ? true : false,
          print_count: e.U_PRINT_COUNT ? e.U_PRINT_COUNT : 0,
          request_reason: e.U_REQUEST_REASON ? e.U_REQUEST_REASON : "",
          requester_name: e.REQUESTER_NAME ? e.REQUESTER_NAME : e.REQUESTER_NAME_RCI,
          status: e.U_STATUS,
          ticket_number: e.U_TS_TICKETNUMBER,
          transaction_id: e.U_TRANSCTION_ID,
          updated_at: `${udate} ${utime}`,
          updated_by: e.U_UPDATED_BY,
          current_print_count: e.CURRENT_COUNT ? e.CURRENT_COUNT : 0,
        });
      }

      return data;
    } else {
      // offline
    }
  };
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

module.exports = requestSelect;
