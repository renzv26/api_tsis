const addRequest = ({
  transactions,
  validateAccessRights,
  moment,
  io,
  nodemailer,
  dotenv,
}) => {
  return async function posts(info) {
    const mode = info.mode;
    dotenv.config();

    // for access rights
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "request",
      "add request"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    // end access rights validate
    if (mode == 1) {
      // online
      delete info.modules;
// console.log(info, "Request INFO")
      const emailAddressesTo = []; // email to send notifs
      let doer = "";

      // check if there are still validated or pending request of the transaction;
      const trnsId = info.transactionId;
      const check = await transactions.reqCheckValidatePending(trnsId);
      const count = check[0].COUNTS;

      // throw error; there are still pending and validated requests
      if (count > 0)
        throw new Error(
          `There are still remaining requests of this transaction.`
        );

      // for draft data
      const draft = {
        plateNumber: info.plateNumber,
        plateNumberId: info.plateNumberId,
        driver: info.driver,
        driverId: info.driverId,
        supplier: info.supplier,
        address: info.address,
        item: info.item,
        itemCode: info.itemCode,
        uom: info.uom,
        qty: info.qty,
        remarks: info.remarks,
      };

      // const d = new Date().toDateString();
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      // console.log('=---=',t);
      // console.log('----',date);

      // const t = new Date().toTimeString();
      const time = await fixTime(t);

      // max code
      const max = await transactions.requestMaxCode({});
      const maxCode = max[0].maxCode;

      let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

      // optionals
      const printCount = info.isReprint ? info.printCount : 0;
      const isEdit = info.isEdit ? 1 : 0;
      const isReprint = info.isReprint ? 1 : 0;
      if(isReprint == 0 && isEdit == 0)
        throw new Error(`Select request type.`)
      const draftData = info.isEdit
        ? `${JSON.stringify(JSON.stringify(JSON.stringify(draft)))}`
        : null;
      const reason = info.reason ? `"${info.reason}"` : null;
      if(!reason)
        throw new Error(`Enter reason.`)
      const trnsIds = info.transactionId ? `"${info.transactionId}"` : null;
      const ticket_number = info.ticket_number ? info.ticket_number : null;
      const userId = info.userId ? `"${info.userId}"` : null;

      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_BFI_TS_REQUESTS

{
  "Code": "${maxCode}",
  "Name": "${maxCode}",
  "U_REQUEST_REASON": ${reason},
  "U_TRANSCTION_ID": ${trnsIds},
  "U_PRINT_COUNT": ${printCount},
  "U_IS_EDIT": ${isEdit},
  "U_IS_REPRINT": ${isReprint},
  "U_DRAFT_DATA": ${draftData},
  "U_CREATED_BY": ${userId},
  "U_CREATEDATE": "${date}",
  "U_CREATETIME": "${time}",
  "U_UPDATED_BY": ${userId},
  "U_UPDATEDATE": "${date}",
  "U_UPDATETIME": "${time}",
  "U_STATUS": "pending",
  "U_TS_TICKETNUMBER": "${ticket_number}"
}
`;

      // get all users to be receiver of notifs; all controller and supervisor;
      const user = await transactions.getUserReceiverNotif(info.userId);
      // max code
      const maxs = await transactions.requestNotifMaxCode({});
      let maxCodes = maxs[0].maxCode;

      // get employee name;
      const empId = info.userId ? info.userId : null;
      const emp = await transactions.getEmployeeName(empId);
      let fn = "";
      let ln = "";
      if (emp.length > 0) {
        fn = emp[0].firstName;
        ln = emp[0].lastName;
        fn_r = emp[0].firstName_revive;
        ln_r = emp[0].lastName_revive;
      }

      // doer = `${fn} ${ln}`; // for email
      doer = fn && ln ? `${fn} ${ln}` : `${fn_r} ${ln_r}`; // for email

      let notif = []; // data to be emit
      for (let i = 0; i < user.length; i++) {
        const id = user[i].Code;
        // console.log(id);
        const email = user[i].U_EMAIL;
        emailAddressesTo.push(email); // push receipients of email notif
        notif.push({
          id: maxCodes,
          request_id: maxCode,
          receiver_id: id,
          request_status: "pending",
          date: `${date} ${timeFormat(t)}`,
          is_read: false,
          sender_id: empId,
          first_name: fn ? fn : fn_r,
          last_name: ln ? ln : ln_r,
          transaction_id: info.transactionId ? info.transactionId : null,
          ticket_number: info.ticket_number ? info.ticket_number : null,
          request_reason: info.reason,
          created_by: empId,
          print_count: printCount,
          is_Edit: isEdit,
          is_Reprint: isReprint,
          draft_data: draftData,
          current_print_count: 5,
        });

        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_BFI_TS_REQ_NOTIF

{
  "Code": "${maxCodes}",
  "Name": "${maxCodes}",
  "U_REQUEST_ID": "${maxCode}",
  "U_RECEIVER_ID": "${id}",
  "U_SENDER_ID": ${userId},
  "U_IS_READ": "0",
  "U_CREATEDBY": ${userId},
  "U_MODIFYBY": ${userId},
  "U_CREATEDATE": "${date}",
  "U_CREATETIME": "${time}",
  "U_UPDATEDATE": "${date}",
  "U_UPDATETIME": "${time}"
}
`;
        maxCodes++;
      }

      batchStr += `
--b--
--a--    
`;
      const data = {
        batchStr,
        cookie: info.cookie,
      };

      // make email composition
      const mailInfo = {
        from: `Biotech Farms Inc.`,
        to: emailAddressesTo,
        subject: "Request received from Truckscale Integrated System",
        text: `Good Day Ma'am/Sir,\n\nA Request from ${doer} was received.\n\nTicket Number: ${
          info.ticket_number ? info.ticket_number : ""
        } \nReason: ${
          info.reason ? info.reason : ""
        }\n\nPlease address this request as soon as possible.\n\nThank you.`,
      };
      // console.log(batchStr);
      await transactions.batchRequest(data);

      // for checking select req notif if exist;
      const checks = await transactions.checkIfExistReqNotif(maxCode);
      const counts = checks[0].COUNTS;

      if (counts > 0) {
        // exist; successfully inserted; emit data for notif; per receiver;
        for (let i = 0; i < notif.length; i++) {
          const e = notif[i];
          io.sockets.emit(`new req - ${e.receiver_id}`, {
            e,
          }); // emit data to per user;
        }

        // send email
        await emailNotif({ mailInfo }, nodemailer, dotenv); // send email


        const msg = `Request added successfully.`;
        return msg;
      } else {
        // error on inserting
        throw new Error(`Error on adding request, please try again.`);
      }
    } else {
      // offline
    }
  };
};

const fixTime = (t) => {
  try {
    // console.log('t',t);
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    // console.log("TIME",time);
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

const timeFormat = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}:${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

async function emailNotif({ mailInfo }, nodemailer, dotenv) {
  try {
    dotenv.config();

    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });

    const result = await new Promise((resolve) => {
      transporter.sendMail(mailInfo, (err, data) => {
        let msg;
        if (err) {
          msg = {
            status: 400,
            data: `Not sent\n\n${err}`,
          };

          resolve(msg);
        }
        msg = {
          status: 200,
          data: `Sent successfully\n\n ${data}`,
        };

        resolve(msg);
      });
    });
    return result;
  } catch (e) {
    console.log("Error: ", e);
  }
}

module.exports = addRequest;
