const uomSelectAll = ({ transactions }) => {
  return async function selects(info) {
    const mode = info.mode;
    if (mode == 1) {
      const res = await transactions.getUoM();
      return res;
    } else {
      // offline mode;
    }
  };
};

module.exports = uomSelectAll;
