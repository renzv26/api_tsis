const updateTransaction = ({ transactions, users }) => {
  return async function put({ ...info } = {}) {
    const mode = info.mode;

    // Only for FSQR use ! ! !

    if (mode == 1) {
      // online mode
      delete info.mode;
      delete info.user;
      delete info.pw;
      delete info.cookie;
      delete info.source;

      // get all data from body
      const { code } = info;

      // delete info.U_TS_STATUS;
      // delete info.U_APP_PO_ID;

      // login to SAP; Service layer account
      const res = await users.userLogin();

      const sessionId = `B1SESSION=${res.SessionId}`;
      info.cookie = sessionId;
      info.trackingCode = code;

      // get transaction data
      const fsqr = await transactions.transactionsSelectOneByTrackingCode({
        info,
      });
      const fsqrStatus = fsqr.status;
      if (fsqrStatus == 200) {
        if (fsqr.data.value.length > 0) {
          // transaction exist in tsis
          const { U_TS_STATUS, U_APP_PO_ID } = info;
          delete info.code;
          delete info.trackingCode;

          const data = fsqr.data.value[0];
          if (U_TS_STATUS) {
            const id = data.DocEntry;
            info.id = id; // append id to update

            // update status
            const res = await transactions.transactionsUpdate(info);
            if (res.status == 204) {
              const msg = "Updated transaction status successfully.";
              return msg;
            }
          }

          if (U_APP_PO_ID) {
            const id = data.DocEntry;
            info.id = id; // append id to update
            // update status
            const res = await transactions.transactionsUpdate(info);
            if (res.status == 204) {
              const msg = "Updated transaction PO successfully.";
              return msg;
            }
          }

          throw new Error("Please input required data.");
        } else {
          // transaction doesn't exist in tsis
          throw new Error(`Transaction doesn't exist.`);
        }
      } else {
        throw new Error("Error retrieving data.");
      }
    } else {
      // offline mode
    }
  };
};

module.exports = updateTransaction;
