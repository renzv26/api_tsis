const addNewTransaction = ({
  transactionDb,
  makeTransaction,
  validateAccessRights,
  driversDb,
  encrypt,
  drivers,
  weightDb,
  truckInfos,
  insertActivityLogss,
  forTransmittalNotifs,
  posDb,
  transactions,
  transactionTypes,
  weightTypes,
  moment,
  items
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "add transaction"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    //Online Mode
    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;
// console.log(info, "ADD Manual")
      // date and time
      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      const time = await fixTime(t);
// console.log('infoooo', info);
      //ticket number incremented from last valu DocEntry
      const currentSeries = await transactions.getDocEntryForSeriesNumber();
      const ticket_number = `${info.truckscale_number}-${moment(date).format("MM")}${moment(date).format("YYYY")}-${currentSeries[0].new_id}-N`
      // const getcurrentSeries = await transactions.getDocEntryForSeriesNumber();
      // const currentSeries = getcurrentSeries[0].DocEntry + 1;
      // let str = "" + currentSeries
      // let pad = "O-0000"
      // let ans = pad.substring(0, pad.length - str.length) + str
      // info.ticket_number = `${ans}`
      // Prereq
      const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName(
        {
          U_TS_TRNSTYPE: info.t_type,
          cookie: info.cookie,
        }
      );

      const grossWeightDetails = await weightTypes.weightTypesSelectByName({
        U_TS_WEIGHT: "gross",
        cookie: info.cookie,
      });

      const tareWeightDetails = await weightTypes.weightTypesSelectByName({
        U_TS_WEIGHT: "tare",
        cookie: info.cookie,
      });

      const transactionTypeId = transactionTypeDetails[0].Code;
      const printCount = transactionTypeDetails[0].U_TS_PRINT_COUNT;
      const grossWeightId = grossWeightDetails[0].Code;
      const tareWeightId = tareWeightDetails[0].Code;
      //Prereq

      if (info.t_type === "others") {
        //get item details
        if(!info.item || info.item == ""){
          throw new Error(`Please enter item`)
        }
        const itemDetails = await items.itemsSelectByItemName(
          { info }
        );

        if(itemDetails.length === 0){
          const max = await items.itemsGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertItem = await items.insertItem({
              Code: maxCode,
              Name: maxCode,
              U_TS_ITEM_NAME: info.item,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_TS_CREATED_BY: info.createdby,
              U_TS_UPDATED_BY: info.createdby,
              U_TS_IS_ACTIVE: "1",
              cookie: info.cookie
          })

          if(insertItem.status != 201){
            throw new Error(`Insert item failed`);
          }
        }
        if(!info.plate_number || info.plate_number == ""){
          throw new Error(`Please enter a valid plate number`)
        }
        const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          { info }
        );

        //check if plate number exist
        if (truckInfoDetails.length === 0) {
          const max = await truckInfos.truckInfosGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertTruckInfo = await truckInfos.truckInfosAddAuto({
              Code: maxCode,
              Name: maxCode,
              U_TS_TRTYPE_ID: 1,
              U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.createdby,
              U_UPDATED_BY: info.createdby,
              cookie: info.cookie
           })

           if (insertTruckInfo.status == 201) {
            truck_info_id = maxCode;
          } else {
            throw new Error(`Insert plate number failed`);
          }
          //  const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          //   { info }
          // );

          // truckInfoId = truckInfoDetails.data[0].Code;
          // throw new Error(
          //   JSON.stringify({
          //     code: 1,
          //     message: "Plate number does not exist",
          //   })
          // );
        } else {
          truck_info_id = truckInfoDetails[0].Code;
        }

        //split driver to get firstname and lastname
        const driverDetailsFromBody = info.driver.split(", ");

        if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
          throw new Error(`Invalid driver name format`);
        }

        const driverFirstName = driverDetailsFromBody[1].toLowerCase();
        const driverLastName = driverDetailsFromBody[0].toLowerCase();

        //check driver if exist or not
        const checkDriver = await drivers.driversAddSelectByName({
          info: {
            U_TS_FN: driverFirstName,
            U_TS_LN: driverLastName,
            cookie: info.cookie,
          },
        });

        if (checkDriver.length > 0) {
          driver_id = checkDriver[0].Code;
        } else {
          //get Driver Max Code
          const driverMax = await drivers.driversGetMaxCode({});
          const driverMaxCode = driverMax[0].maxCode;

          //insert driver
          const insertDriver = await drivers.driversAdd({
            info: {
              Code: driverMaxCode,
              Name: driverMaxCode,
              U_TS_FN: driverFirstName,
              U_TS_LN: driverLastName,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.created_by,
              U_UPDATED_BY: info.created_by,
              cookie: info.cookie,
            },
          });

          if (insertDriver.status == 201) {
            driver_id = driverMaxCode;
          } else {
            throw new Error(`Insert driver failed`);
          }
        }
        //build object for sap
        const transactionDetails = {
          U_TS_TRNS_TYPE: transactionTypeId,
          U_TS_STATUS: "for inbound",
          U_TS_NUM_BAGS: info.numberOfBags,
          U_TS_SUPPLIER: info.supplier,
          U_TS_SUPPLIER_ADD: info.supplierAddress,
          U_TS_ITEMCODE: info.itemCode,
          U_TS_ITEMNAME: info.item,
          U_UOM: info.uom,
          U_UOM_TS: info.uom,
          U_PRINT_COUNT: printCount,
          U_TS_TICKETNUMBER: ticket_number,
          U_COMPANY: info.company ? info.company : null,
          U_CREATED_BY: info.createdby,
          U_UPDATED_BY: info.createdby,
          BFI_TS_IBTRNCollection: [
            {
              U_TS_WEIGHT_ID: grossWeightId,
              U_TS_CREATEDBY: info.createdby,
              U_UPDATED_BY: info.createdby,
              U_TS_TRKSCL_ID: info.truckscale_id,
              U_TS_DRIVERID: driver_id,
              U_TS_TRKINFO: truck_info_id,
              U_TS_CREATEDATE: info.created_date,
              U_TS_CREATETIME: info.created_time,
              U_TS_UPDATEDATE: info.created_date,
              U_TS_UPDATETIME: info.created_time,
            },
          ],
          BFI_TS_OBTRNCollection: [
            {
              U_TS_WEIGHT_ID: tareWeightId,
              U_TS_CREATEDBY: info.createdby,
              U_UPDATED_BY: info.createdby,
              U_TS_TRKSCL_ID: info.truckscale_id,
              U_TS_DRIVERID: driver_id,
              U_TS_TRKINFO: truck_info_id,
              U_TS_CREATEDATE: info.created_date,
              U_TS_CREATETIME: info.created_time,
              U_TS_UPDATEDATE: info.created_date,
              U_TS_UPDATETIME: info.created_time,
            },
          ],
          cookie: info.cookie,
        };
        
// console.log('transactionDetails',transactionDetails);
        //insertion
        const insertTransaction = await transactions.transactionsAdd(transactionDetails);

        if (insertTransaction.status == 201) {
          return { transactionId: insertTransaction.data.DocEntry };
        } else {
          throw new Error(`Insert transaction failed. Please Try again.`);
        }
      } else if (info.t_type === "withdrawal") {
        // can't add manually withdrwal; only scanning;
        if(!info.item || info.item == ""){
          throw new Error(`Please enter item`)
        }
        const itemDetails = await items.itemsSelectByItemName(
          { info }
        );

        if(itemDetails.length === 0){
          const max = await items.itemsGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertItem = await items.insertItem({
              Code: maxCode,
              Name: maxCode,
              U_TS_ITEM_NAME: info.item,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_TS_CREATED_BY: info.createdby,
              U_TS_UPDATED_BY: info.createdby,
              U_TS_IS_ACTIVE: "1",
              cookie: info.cookie
          })

          if(insertItem.status != 201){
            throw new Error(`Insert item failed`);
          }
        }
        //get truck info details
        if(!info.plate_number || info.plate_number == ""){
          throw new Error(`Please enter a valid plate number`)
        }
        const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          { info }
        );

        //check if plate number exist
        if (truckInfoDetails.length === 0) {
          const max = await truckInfos.truckInfosGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertTruckInfo = await truckInfos.truckInfosAddAuto({
              Code: maxCode,
              Name: maxCode,
              U_TS_TRTYPE_ID: 1,
              U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.createdby,
              U_UPDATED_BY: info.createdby,
              cookie: info.cookie
           })

           if (insertTruckInfo.status == 201) {
            truckInfoId = maxCode;
          } else {
            throw new Error(`Insert plate number failed`);
          }
          //  const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          //   { info }
          // );

          // truckInfoId = truckInfoDetails.data[0].Code;
          // throw new Error(
          //   JSON.stringify({
          //     code: 1,
          //     message: "Plate number does not exist",
          //   })
          // );
        } else {
          truckInfoId = truckInfoDetails[0].Code;
        }

        //split driver to get firstname and lastname
        const driverDetailsFromBody = info.driver.split(", ");

        if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
          throw new Error(`Invalid driver name format`);
        }

        const driverFirstName = driverDetailsFromBody[1].toLowerCase();
        const driverLastName = driverDetailsFromBody[0].toLowerCase();

        //check driver if exist or not
        const checkDriver = await drivers.driversAddSelectByName({
          info: {
            U_TS_FN: driverFirstName,
            U_TS_LN: driverLastName,
            cookie: info.cookie,
          },
        });

        if (checkDriver.length > 0) {
          driverId = checkDriver[0].Code;
        } else {
          //get Driver Max Code
          const driverMax = await drivers.driversGetMaxCode({});
          const driverMaxCode = driverMax[0].maxCode;

          //insert driver
          const insertDriver = await drivers.driversAdd({
            info: {
              Code: driverMaxCode,
              Name: driverMaxCode,
              U_TS_FN: driverFirstName,
              U_TS_LN: driverLastName,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.created_by,
              U_UPDATED_BY: info.created_by,
              cookie: info.cookie,
            },
          });

          if (insertDriver.status == 201) {
            driverId = driverMaxCode;
          } else {
            throw new Error(`Insert driver failed`);
          }
        }
        //get transaction details
        // to post;
        let withdrawalObject = {};

        withdrawalObject = {
          U_TS_TRNS_TYPE: transactionTypeId,
          U_TS_STATUS: "for inbound",
          U_TS_NUM_BAGS: info.no_of_bags,
          U_TS_LOC_DLVRY: info.locationDelivery,
          U_TS_SUPPLIER:info.supplier_name,
          U_TS_SUPPLIER_ADD:info.supplier_address,
          U_TS_ITEMNAME:info.item,
          U_TS_IS_NAPIER: 0,
           U_COMPANY: "Biotech Farms Inc.",
          // U_COMPANY: info.company ? info.company : null,
          U_APP_SO_ID: info.so,
          U_PRINT_COUNT: printCount,
          U_TS_TICKETNUMBER: ticket_number,
          U_CREATED_BY: info.created_by,
          U_UPDATED_BY: info.created_by,
          U_UOM: "KILOGRAM",
          U_UOM_TS: info.uom,
          BFI_TS_IBTRNCollection: [
            {
              U_TS_WEIGHT_ID: grossWeightId,
              U_TS_CREATEDBY: info.created_by,
              U_UPDATED_BY: info.created_by,
              U_TS_DRIVERID: driverId,
              U_TS_TRKINFO: truckInfoId,
              U_TS_CREATEDATE: info.created_date,
              U_TS_CREATETIME: info.created_time,
              U_TS_UPDATEDATE: info.created_date,
              U_TS_UPDATETIME: info.created_time,
            },
          ],
          BFI_TS_OBTRNCollection: [
            {
              U_TS_WEIGHT_ID: tareWeightId,
              U_TS_CREATEDBY: info.created_by,
              U_UPDATED_BY: info.created_by,
              U_TS_DRIVERID: driverId,
              U_TS_TRKINFO: truckInfoId,
              U_TS_CREATEDATE: info.created_date,
              U_TS_CREATETIME: info.created_time,
              U_TS_UPDATEDATE: info.created_date,
              U_TS_UPDATETIME: info.created_time,
            },
          ],
          cookie: info.cookie,
        };

        //insertion
        const insertTransaction = await transactions.transactionsAdd(
          withdrawalObject
        );

        if (insertTransaction.status == 201) {
          return { transactionId: insertTransaction.data.DocEntry };
        } else {
          throw new Error(`Insert transaction failed. Please Try again.`);
        }
      } else if (info.t_type === "delivery") {
        //get item details
        if(!info.item || info.item == ""){
          throw new Error(`Please enter item`)
        }
        const itemDetails = await items.itemsSelectByItemName(
          { info }
        );

        if(itemDetails.length === 0){
          const max = await items.itemsGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertItem = await items.insertItem({
              Code: maxCode,
              Name: maxCode,
              U_TS_ITEM_NAME: info.item,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_TS_CREATED_BY: info.createdby,
              U_TS_UPDATED_BY: info.createdby,
              U_TS_IS_ACTIVE: "1",
              cookie: info.cookie
          })

          if(insertItem.status != 201){
            throw new Error(`Insert item failed`);
          }
        }
        //get truck info details
        if(!info.plate_number || info.plate_number == ""){
          throw new Error(`Please enter a valid plate number`)
        }
        const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          { info }
        );

        //check if plate number exist
        if (truckInfoDetails.length === 0) {
          const max = await truckInfos.truckInfosGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertTruckInfo = await truckInfos.truckInfosAddAuto({
              Code: maxCode,
              Name: maxCode,
              U_TS_TRTYPE_ID: 1,
              U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.createdby,
              U_UPDATED_BY: info.createdby,
              cookie: info.cookie
           })

           if (insertTruckInfo.status == 201) {
            truckInfoId = maxCode;
          } else {
            throw new Error(`Insert plate number failed`);
          }
          //  const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          //   { info }
          // );

          // truckInfoId = truckInfoDetails.data[0].Code;
          // throw new Error(
          //   JSON.stringify({
          //     code: 1,
          //     message: "Plate number does not exist",
          //   })
          // );
        } else {
          truckInfoId = truckInfoDetails[0].Code;
        }

        //split driver to get firstname and lastname
        const driverDetailsFromBody = info.driver.split(", ");

        if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
          throw new Error(`Invalid driver name format`);
        }

        const driverFirstName = driverDetailsFromBody[1].toLowerCase();
        const driverLastName = driverDetailsFromBody[0].toLowerCase();

        //check driver if exist or not
        const checkDriver = await drivers.driversAddSelectByName({
          info: {
            U_TS_FN: driverFirstName,
            U_TS_LN: driverLastName,
            cookie: info.cookie,
          },
        });

        if (checkDriver.length > 0) {
          driverId = checkDriver[0].Code;
        } else {
          //get Driver Max Code
          const driverMax = await drivers.driversGetMaxCode({});
          const driverMaxCode = driverMax[0].maxCode;

          //insert driver
          const insertDriver = await drivers.driversAdd({
            info: {
              Code: driverMaxCode,
              Name: driverMaxCode,
              U_TS_FN: driverFirstName,
              U_TS_LN: driverLastName,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.created_by,
              U_UPDATED_BY: info.created_by,
              cookie: info.cookie,
            },
          });

          if (insertDriver.status == 201) {
            driverId = driverMaxCode;
          } else {
            throw new Error(`Insert driver failed`);
          }
        }

        //build final object; determine if napier or not
        let deliveryTransactionDetails = {};
        if (info.isNapier) {
           console.log("wew");
          // console.log(info);
          // if true; means napier;
          deliveryTransactionDetails = {
            U_TS_TRNS_TYPE: transactionTypeId,
            U_TS_STATUS: "for inbound",
            U_TS_NUM_BAGS: info.no_of_bags,
            U_TS_TRCK_CODE: info.trackingCode,
            U_TS_LOC_DLVRY: info.locationDelivery,
            U_TS_SUPPLIER:info.supplier_name,
            U_TS_SUPPLIER_ADD:info.supplier_address,
            U_TS_ITEMNAME:info.item,
            U_TS_IS_NAPIER: 1,
            U_COMPANY: info.company ? info.company : null,
            U_TS_SUPPLIER:info.supplier_name,
            U_TS_SUPPLIER_ADD:info.supplier_address,
            U_PRINT_COUNT: printCount,
            U_TS_TICKETNUMBER: ticket_number,
            U_TS_DRNUMBER: info.dr_number,
            U_TS_PLOT_CODE: info.plot_code,
            U_CREATED_BY: info.createdby,
            U_UPDATED_BY: info.createdby,
            U_UOM: "KILOGRAM",
            U_UOM_TS: info.uom,
            BFI_TS_IBTRNCollection: [
              {
                U_TS_WEIGHT_ID: grossWeightId,
                U_TS_CREATEDBY: info.createdby,
                U_UPDATED_BY: info.createdby,
                U_TS_DRIVERID: driverId,
                U_TS_TRKINFO: truckInfoId,
                U_TS_CREATEDATE: info.created_date,
                U_TS_CREATETIME: info.created_time,
                U_TS_UPDATEDATE: info.created_date,
                U_TS_UPDATETIME: info.created_time,
              },
            ],
            BFI_TS_OBTRNCollection: [
              {
                U_TS_WEIGHT_ID: tareWeightId,
                U_TS_CREATEDBY: info.createdby,
                U_UPDATED_BY: info.createdby,
                U_TS_DRIVERID: driverId,
                U_TS_TRKINFO: truckInfoId,
                U_TS_CREATEDATE: info.created_date,
                U_TS_CREATETIME: info.created_time,
                U_TS_UPDATEDATE: info.created_date,
                U_TS_UPDATETIME: info.created_time,
              },
            ],
            cookie: info.cookie,
          };
        } else {
          // console.log("HEH");
          deliveryTransactionDetails = {
            U_TS_TRNS_TYPE: transactionTypeId,
            U_TS_STATUS: "for inbound",
            U_TS_NUM_BAGS: info.no_of_bags,
            U_TS_TRCK_CODE: info.trackingCode,
            U_TS_LOC_DLVRY: info.locationDelivery,
            U_TS_SUPPLIER:info.supplier_name,
            U_TS_SUPPLIER_ADD:info.supplier_address,
            U_TS_ITEMNAME:info.item,
            U_TS_IS_NAPIER: 0,
            U_COMPANY: info.company ? info.company : null,
            U_PRINT_COUNT: printCount,
            U_TS_TICKETNUMBER: ticket_number,
            U_TS_DRNUMBER: info.dr_number,
            U_TS_PLOT_CODE: info.plot_code,
            U_CREATED_BY: info.createdby,
            U_UPDATED_BY: info.createdby,
            U_UOM: "KILOGRAM",
            U_UOM_TS: info.uom,
            BFI_TS_IBTRNCollection: [
              {
                U_TS_WEIGHT_ID: grossWeightId,
                U_TS_CREATEDBY: info.createdby,
                U_UPDATED_BY: info.createdby,
                U_TS_TRKSCL_ID: info.truckscale_id,
                U_TS_DRIVERID: driverId,
                U_TS_TRKINFO: truckInfoId,
                U_TS_CREATEDATE: info.created_date,
                U_TS_CREATETIME: info.created_time,
                U_TS_UPDATEDATE: info.created_date,
                U_TS_UPDATETIME: info.created_time,
              },
            ],
            BFI_TS_OBTRNCollection: [
              {
                U_TS_WEIGHT_ID: tareWeightId,
                U_TS_CREATEDBY: info.createdby,
                U_UPDATED_BY: info.createdby,
                U_TS_DRIVERID: driverId,
                U_TS_TRKINFO: truckInfoId,
                U_TS_TRKSCL_ID: info.truckscale_id,
                U_TS_CREATEDATE: info.created_date,
                U_TS_CREATETIME: info.created_time,
                U_TS_UPDATEDATE: info.created_date,
                U_TS_UPDATETIME: info.created_time,
              },
            ],
            cookie: info.cookie,
          };
        }
        console.log(deliveryTransactionDetails, "DELIVERYDETAILS")
        // console.log("WT",deliveryTransactionDetails);
        const insertTransaction = await transactions.transactionsAdd(
          deliveryTransactionDetails
        );

        if (insertTransaction.status == 201) {
          return { transactionId: insertTransaction.data.DocEntry };
        } else {
          throw new Error(`Insert transaction failed. Please Try again.`);
        }
      }
    } else {
      const result = makeTransaction(info);
      let transactionTypeId; // hold transaction type id
      let supplierId; // hold supplier id
      let rawMaterialId; // hold raw material id
      let driverId; // hold driver id

      // get transaction type id
      const transactionType = await transactionDb.selectOneTransactionType({
        transaction_type: result.getTransactionType(),
      });
      // transaction type id
      transactionTypeId = transactionType.rows[0].id;

      // // get supplier id
      // const supplier = await transactionDb.selectOneSupplier({
      //   supplier: result.getSupplier()
      // });

      // // meaning if supplier exist
      // if (supplier.rowCount > 0) {
      //   // supplier id
      //   supplierId = supplier.rows[0].id;
      // } else {
      //   // insert supplier; then return id
      //   const insert = await transactionDb.insertSupplier({
      //     supplier: result.getSupplier()
      //   });
      //   // supplier id
      //   supplierId = insert.rows[0].id;
      // }

      // get raw material id
      // const rawMaterial = await transactionDb.selectOneRawMaterial({
      //   raw_material_name: result.getRawMaterial()
      // });

      // if (rawMaterial.rowCount > 0) {
      //   // raw material id
      //   rawMaterialId = rawMaterial.rows[0].id;
      // } else {
      //   // insert raw material; then return id
      //   const insert = await transactionDb.insertRawMaterial({
      //     raw_material_name: result.getRawMaterial(),
      //     created_by: result.getCreatedBy(),
      //     created_at: result.getCreatedAt()
      //   });
      //   // raw material id
      //   rawMaterialId = insert.rows[0].id;
      // }

      // transaction type
      const tType = result.getTransactionType();
      const tTypeLower = tType.toLowerCase();

      // transaction type is delivery
      if (tTypeLower === "delivery") {
        // ##########################
        // Auto insert driver
        // auto select / insert driver
        if (!info.driver) {
          throw new Error(`Please enter driver's information.`);
        }
        if (!info.driver.firstname) {
          throw new Error(`Please enter driver's first name.`);
        }
        if (!info.driver.lastname) {
          throw new Error(`Please enter driver's last name.`);
        }
        // get driver id
        const driverExist = await driversDb.findByName({
          firstname: encrypt(info.driver.firstname),
          lastname: encrypt(info.driver.lastname),
        });

        if (driverExist.rowCount > 0) {
          driverId = driverExist.rows[0].id; // driver's id
        } else {
          // insert raw material; then return id
          const insert = await driversDb.insertDriverInTransaction({
            firstname: encrypt(info.driver.firstname),
            lastname: encrypt(info.driver.lastname),
            created_at: new Date().toISOString(),
          });
          // raw material id
          driverId = insert.rows[0].id;
        }
        // ########################## end here

        // check if tracking code exist already; return transaction id
        const trackCode = result.getTrackingCode();
        const code = await transactionDb.checkTrackingCode({ code: trackCode });
        if (code.rowCount > 0) {
          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: code.rows[0].id,
          });

          // status of the transaction
          const transactionStatus = transactionInfo.rows[0].status.toLowerCase();

          // check status
          if (transactionStatus === "completed") {
            // to do
            const data = {
              msg: `re-scan QR; for outbound - completed`,
              transactionId: code.rows[0].id,
              hasOutbound: true,
            };

            return data;
          }

          if (transactionStatus === "for inbound") {
            // re-scan just return QR
            const data = {
              msg: `re-scan QR`,
              transactionId: code.rows[0].id,
              hasInbound: true,
            };

            return data;
          }

          if (transactionStatus === "for outbound") {
            if (!info.plate_number) {
              throw new Error(`Please enter plate number.`);
            }
            const plateNumberExist = await transactionDb.selectTruckTypeInfo({
              plate_number: info.plate_number,
            });
            if (plateNumberExist.rowCount > 0) {
              // truck infoid
              const truckInfoId = plateNumberExist.rows[0].id;

              const weightType = await weightDb.selectByName({
                weight_name: "Tare",
              }); //set weight type

              const weight_type_id = weightType.rows[0].id;

              // delete first before insert; to avoid duplicates
              await transactionDb.deleteNullOutboundTransactions({
                id: code.rows[0].id,
              });

              // insert to outbound
              const insert = await transactionDb.addOutboundTransaction({
                transaction_id: code.rows[0].id,
                weight_type_id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId,
              });

              // #################
              // logs
              // new values
              const new_values = {
                transaction_id: code.rows[0].id,
                weight_type_id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId,
              };

              const logs = {
                action_type: "CREATE DELIVERY TRANSACTION - OUTBOUND",
                table_affected: "ts_outbound_transactions",
                new_values,
                prev_values: null,
                created_at: new Date().toISOString(),
                updated_at: null,
                users_id: info.users_id,
              };

              await insertActivityLogss({ logs });
              // ################# logs

              const data = {
                msg: `re-scan QR; for outbound`,
                transactionId: code.rows[0].id,
                hasOutbound: true,
              };

              return data;
            } else {
              // if doesn't exist
              const data = {
                msg: `Failed to add transaction; for outbound.`,
                hasOutbound: false,
              };
              return data;
            }
          }
        } else {
          if (!info.plate_number) {
            throw new Error(`Please enter plate number.`);
          }

          // check plate number
          const plateNumberExist = await transactionDb.selectTruckTypeInfo({
            plate_number: info.plate_number,
          });

          // if exist plate number
          if (plateNumberExist.rowCount > 0) {
            // if code doesn't exist
            // insert to db

            let poData = await posDb.selectPoByNumber(
              result.getPurchaseOrderId()
            );

            if (poData.rowCount === 0) {
              throw new Error(`PO does not exist in SAP`);
            }

            let poId = poData.rows[0].id;

            const insert = await transactionDb.insertNewTransaction({
              purchase_order_id: poId,
              transaction_type_id: transactionTypeId,
              status: "for inbound",
              created_at: result.getCreatedAt(),
              created_by: result.getCreatedBy(),
              tracking_code: result.getTrackingCode(),
              no_of_bags: result.getNumberOfBags(),
            });

            // #################
            // logs
            // new values
            const new_values = {
              // supplier_id: supplierId,
              // raw_material_id: rawMaterialId,
              transaction_type_id: transactionTypeId,
              status: "for inbound",
              created_at: result.getCreatedAt(),
              created_by: result.getCreatedBy(),
              tracking_code: result.getTrackingCode(),
              no_of_bags: result.getNumberOfBags(),
            };

            const logs = {
              action_type: "CREATE DELIVERY TRANSACTION",
              table_affected: "ts_transactions",
              new_values,
              prev_values: null,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: result.getCreatedBy(),
            };

            await insertActivityLogss({ logs });
            // ################# logs

            // truck infoid
            const truckInfoId = plateNumberExist.rows[0].id;
            const weightType = await weightDb.selectByName({
              weight_name: "Gross",
            }); //set weight type

            // delete first before insert; to avoid duplicates
            await transactionDb.deleteNullInboundTransactions({
              id: insert.rows[0].id,
            });

            // insert to inbound
            const inbound = await transactionDb.addInboundTransaction({
              transaction_id: insert.rows[0].id,
              weight_type_id: weightType.rows[0].id,
              users_id: info.users_id,
              truckscale_id: info.truckscale_id,
              drivers_id: driverId,
              truck_info_id: truckInfoId,
            });

            const data = {
              msg: `for inbound`,
              transactionId: insert.rows[0].id,
              hasInbound: true,
            };

            // #################
            // logs
            // new values
            const new_valuess = {
              transaction_id: insert.rows[0].id,
              weight_type_id: weightType.rows[0].id,
              users_id: info.users_id,
              truckscale_id: info.truckscale_id,
              drivers_id: driverId,
              truck_info_id: truckInfoId,
            };

            const logss = {
              action_type: "CREATE DELIVERY TRANSACTION - INBOUND",
              table_affected: "ts_inbound_transactions",
              new_values: new_valuess,
              prev_values: null,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: info.users_id,
            };

            await insertActivityLogss({ logs: logss });
            // ################# logs

            return data;
          } else {
            // if doesn't exist
            const data = {
              msg: `Failed to add transaction.`,
              hasInbound: false,
            };
            return data;
          }
        }
      }
      // end delivery

      // ####################
      // transaction type is others
      if (tTypeLower === "others") {
        // check if there is drivers id in the req body
        if (!info.drivers_id) {
          throw new Error(`Please enter driver.`);
        }
        // check truck info is required
        if (!info.truck_info_id) {
          throw new Error("Please enter truck info.");
        }
        // insert to db
        const insert = await transactionDb.insertNewTransactionOthers({
          // supplier_id: supplierId,
          // raw_material_id: rawMaterialId,
          transaction_type_id: transactionTypeId,
          status: "others",
          created_at: result.getCreatedAt(),
          created_by: result.getCreatedBy(),
          drivers_id: info.drivers_id,
          truck_info_id: info.truck_info_id,
        });

        // #################
        // logs
        // new values
        const new_values = {
          // supplier_id: supplierId,
          // raw_material_id: rawMaterialId,
          transaction_type_id: transactionTypeId,
          status: "others",
          created_at: result.getCreatedAt(),
          created_by: result.getCreatedBy(),
          drivers_id: info.drivers_id,
          truck_info_id: info.truck_info_id,
        };

        const logs = {
          action_type: "CREATE OTHERS TRANSACTION",
          table_affected: "ts_transactions",
          new_values,
          prev_values: null,
          created_at: new Date().toISOString(),
          updated_at: null,
          users_id: result.getCreatedBy(),
        };

        await insertActivityLogss({ logs });
        // ################# logs

        const data = {
          msg: `Inserted successfully transaction.`,
        };

        return data;
      }
      // end others

      // ####################
      // transaction type is withdrawal
      // wait for API of SAP; make a route for worker
      if (tTypeLower === "withdrawal") {
        // if it doesn't exist; UI or QR code can insert
        // check PO if exist

        const poExist = await transactionDb.checkPurchaseOrderId({
          purchase_order_id: result.getPurchaseOrderId(),
        });

        if (poExist.rowCount > 0) {
          // get status of transaction

          const transactionId = poExist.rows[0].id;

          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: transactionId,
          });
          // status of transaction
          const transactionStatus = transactionInfo.rows[0].status.toLowerCase();

          if (transactionStatus === "for transmittal") {
            // update db; from info sheet
            const insert = await transactionDb.updateWithdrawalTransaction({
              // supplier_id: supplierId,
              // raw_material_id: rawMaterialId,
              transaction_type_id: transactionTypeId,
              status: "for inbound",
              updated_at: result.getUpdatedAt(),
              location_delivery: result.getLocationDelivery(),
              warehouse_location: result.getWarehouseLocations(),
              no_of_bags: result.getNumberOfBags(),
              transmittal_number: result.getTransmittalNumber(),
              created_by: result.getCreatedBy(),
              id: transactionId,
            });

            // emit notif
            await forTransmittalNotifs({});

            // #################
            // logs
            // new values
            const new_values = {
              // supplier_id: supplierId,
              // raw_material_id: rawMaterialId,
              transaction_type_id: transactionTypeId,
              status: "for inbound",
              updated_at: result.getUpdatedAt(),
              location_delivery: result.getLocationDelivery(),
              warehouse_location: result.getWarehouseLocations(),
              no_of_bags: result.getNumberOfBags(),
              transmittal_number: result.getTransmittalNumber(),
              created_by: result.getCreatedBy(),
              id: transactionId,
            };

            const prev_values = {
              id: transactionId,
              purchaseOrder_id: result.getPurchaseOrderId(),
            };

            const logs = {
              action_type: "UPDATE FOR TRANSMITTAL TRANSACTION",
              table_affected: "ts_transactions",
              new_values,
              prev_values,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: result.getCreatedBy(),
            };

            await insertActivityLogss({ logs });
            // ################# logs

            // return updated msg
            const count = insert.rowCount; // count how many updated data
            const data = {
              msg: `Updated successfully ${count} transaction.`,
            };
            return data;
          } else {
            // select data; from QR

            // to insert auto inbound transaction

            // ##########################

            // Auto insert driver
            // auto select / insert driver
            if (!info.driver) {
              throw new Error(`Please enter driver's information.`);
            }
            if (!info.driver.firstname) {
              throw new Error(`Please enter driver's first name.`);
            }
            if (!info.driver.lastname) {
              throw new Error(`Please enter driver's last name.`);
            }
            // get driver id
            const driverExist = await driversDb.findByName({
              firstname: encrypt(info.driver.firstname),
              lastname: encrypt(info.driver.lastname),
            });

            if (driverExist.rowCount > 0) {
              driverId = driverExist.rows[0].id; // driver's id
            } else {
              // insert raw material; then return id
              const insert = await driversDb.insertDriverInTransaction({
                firstname: encrypt(info.driver.firstname),
                lastname: encrypt(info.driver.lastname),
                created_at: new Date().toISOString(),
              });
              // raw material id
              driverId = insert.rows[0].id;
            }

            const plateNumberExist = await transactionDb.selectTruckTypeInfo({
              plate_number: info.plate_number,
            });

            // if exist plate number
            if (plateNumberExist.rowCount > 0) {
              // truck infoid
              const truckInfoId = plateNumberExist.rows[0].id;

              if (transactionStatus === "for inbound") {
                const weightType = await weightDb.selectByName({
                  weight_name: "Tare",
                }); //set weight type

                // delete first before insert; to avoid duplicates
                await transactionDb.deleteNullInboundTransactions({
                  id: transactionId,
                });

                // insert to inbound
                await transactionDb.addInboundTransaction({
                  transaction_id: transactionId,
                  weight_type_id: weightType.rows[0].id,
                  users_id: info.users_id,
                  truckscale_id: info.truckscale_id,
                  drivers_id: driverId,
                  truck_info_id: truckInfoId,
                });

                // #################
                // logs
                // new values
                const new_values = {
                  transaction_id: transactionId,
                  weight_type_id: weightType.rows[0].id,
                  users_id: info.users_id,
                  truckscale_id: info.truckscale_id,
                  drivers_id: driverId,
                  truck_info_id: truckInfoId,
                };

                const logs = {
                  action_type: "CREATE WITHDRAWAL TRANSACTION - INBOUND",
                  table_affected: "ts_transactions",
                  new_values,
                  prev_values: null,
                  created_at: new Date().toISOString(),
                  updated_at: null,
                  users_id: info.users_id,
                };

                await insertActivityLogss({ logs });
                // ################# logs
              }

              if (transactionStatus === "for outbound") {
                const weightType = await weightDb.selectByName({
                  weight_name: "Gross",
                }); //set weight type

                const weight_type_id = weightType.rows[0].id;

                // delete first before insert; to avoid duplicates
                await transactionDb.deleteNullOutboundTransactions({
                  id: transactionId,
                });

                // insert to outbound
                await transactionDb.addOutboundTransaction({
                  transaction_id: transactionId,
                  weight_type_id,
                  users_id: info.users_id,
                  truckscale_id: info.truckscale_id,
                  drivers_id: driverId,
                  truck_info_id: truckInfoId,
                });

                // #################
                // logs
                // new values
                const new_values = {
                  transaction_id: transactionId,
                  weight_type_id,
                  users_id: info.users_id,
                  truckscale_id: info.truckscale_id,
                  drivers_id: driverId,
                  truck_info_id: truckInfoId,
                };

                const logs = {
                  action_type: "CREATE WITHDRAWAL TRANSACTION - OUTBOUND",
                  table_affected: "ts_transactions",
                  new_values,
                  prev_values: null,
                  created_at: new Date().toISOString(),
                  updated_at: null,
                  users_id: info.users_id,
                };

                await insertActivityLogss({ logs });
                // ################# logs
              }
            }
            // end auto insert inbound transaction

            // return
            const data = {
              msg: `Transaction exist..`,
              transactionId,
            };

            return data;
          }
        } else {
          // return QR; to be inserted manually; modal

          return info;
        }
      }
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = addNewTransaction;
