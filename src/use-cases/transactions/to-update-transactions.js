const e = require("express");

const toUpdateTransaction = ({ transactions, truckInfos, drivers, moment, items }) => {
  return async function put({ ...info } = {}) {
    const mode = info.mode;
console.log('TEST');
    if (mode == 1) {
      // online
      console.log('inofof', info);
      delete info.mode;
      delete info.source;
      const { cookie } = info;
      
      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      // console.log('=---=',t);
      const time = await fixTime(t);

      if (info.isOthers) {
        // to edit others transaction
        if(!info.item || info.item == ""){
          throw new Error(`Please enter item`)
        }
        const itemDetails = await items.itemsSelectByItemName(
          { info }
        );

        if(itemDetails.length === 0){
          const max = await items.itemsGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertItem = await items.insertItem({
              Code: maxCode,
              Name: maxCode,
              U_TS_ITEM_NAME: info.item,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_TS_CREATED_BY: info.createdby,
              U_TS_UPDATED_BY: info.createdby,
              U_TS_IS_ACTIVE: "1",
              cookie: info.cookie
          })

          if(insertItem.status != 201){
            throw new Error(`Insert item failed`);
          }
        }
        let data = {
          cookie,
          id: info.id,
        };
        console.log('info.plate_number', info.plate_number);
        if(!info.plate_number || info.plate_number == ""){
          throw new Error(`Please enter a valid plate number`);
        }
        const res = await transactions.transactionsSelectOne({ info: data });
        if (res.status == 200) {
          const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
            { info }
          );
          if (truckInfoDetails.length === 0) {
            const max = await truckInfos.truckInfosGetMaxCode({})
            const maxCode = max[0].maxCode;
  
            const insertTruckInfo = await truckInfos.truckInfosAddAuto({
                Code: maxCode,
                Name: maxCode,
                U_TS_TRTYPE_ID: 1,
                U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
                U_TS_CREATEDATE: date,
                U_TS_CREATETIME: time,
                U_TS_UPDATEDATE: date,
                U_TS_UPDATETIME: time,
                U_CREATED_BY: info.created_by,
                U_UPDATED_BY: info.created_by,
                cookie
             })

             if (insertTruckInfo.status == 201) {
              truckInfoId = maxCode;
            } else {
              throw new Error(`Insert plate number failed`);
            }
            //  const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
            //   { info }
            // );
            // truckInfoId = truckInfoDetails.data[0].Code;
            // throw new Error(
            //   JSON.stringify({
            //     code: 1,
            //     message: "Plate number does not exist",
            //   })
            // );
          } else {
            truckInfoId = truckInfoDetails[0].Code;
          }

          //split driver to get firstname and lastname
        const driverDetailsFromBody = info.driver.split(", ");
        if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
          throw new Error(`Invalid driver name format`);
        }

        const driverFirstName = driverDetailsFromBody[1].toLowerCase();
        const driverLastName = driverDetailsFromBody[0].toLowerCase();

        //check driver if exist or not
        const checkDriver = await drivers.driversAddSelectByName({
          info: {
            U_TS_FN: driverFirstName,
            U_TS_LN: driverLastName,
            cookie: info.cookie,
          },
        });
        
        if (checkDriver.length > 0) {
          driverId = checkDriver[0].Code;
        } else {
          //get Driver Max Code
          const driverMax = await drivers.driversGetMaxCode({});
          const driverMaxCode = driverMax[0].maxCode;

          //insert driver
          const insertDriver = await drivers.driversAdd({
            info: {
              Code: driverMaxCode,
              Name: driverMaxCode,
              U_TS_FN: driverFirstName,
              U_TS_LN: driverLastName,
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.created_by,
              U_UPDATED_BY: info.created_by,
              cookie: info.cookie,
            },
          });

          if (insertDriver.status == 201) {
            driverId = driverMaxCode;
          } else {
            throw new Error(`Insert driver failed`);
          }
        }
          data = {}; // empty
          const ib_data = res.data.BFI_TS_IBTRNCollection;
          const ob_data = res.data.BFI_TS_OBTRNCollection;

          // ib and ob array to update
          const BFI_TS_IBTRNCollection = [];
          const BFI_TS_OBTRNCollection = [];


          // make ib array
          for (let i = 0; i < ib_data.length; i++) {
            const e = ib_data[i];
            BFI_TS_IBTRNCollection.push({
              LineId: e.LineId,
              U_TS_TRKINFO: truckInfoId,
              U_TS_DRIVERID: driverId,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
            });
          }

          // make ob array
          for (let i = 0; i < ob_data.length; i++) {
            const e = ob_data[i];
            BFI_TS_OBTRNCollection.push({
              LineId: e.LineId,
              U_TS_TRKINFO: truckInfoId,
              U_TS_DRIVERID: driverId,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_TS_REMARKS: info.remarks,
            });
          }

          // make the object
          data = {
            id: info.id,
            U_TS_SUPPLIER: info.supplier,
            U_TS_SUPPLIER_ADD: info.address,
            U_TS_ITEMCODE: info.itemCode,
            U_TS_ITEMNAME: info.item,
            U_UOM_TS: info.uom,
            U_TS_NUM_BAGS: info.qty,
            BFI_TS_IBTRNCollection,
            BFI_TS_OBTRNCollection,
            cookie,
          };

          const update = await transactions.transactionsUpdate(data);
          if (update.status !== 204)
            throw new Error(`Error on updating, please  try again.`);

          return update;
        } else {
          throw new Error(
            `Error retrieving transaction data, please try again.`
          );
        }
      }

      // change status to cancel
      if (info.isCancel) {
        const { id } = info; // id to update to cancel status

        const data = {
          cookie,
          id,
          U_TS_STATUS: "cancelled",
        };

        let res = null;

        // request 5 times; til success
        let counter = 5;
        while (counter !== 0) {
          res = await transactions.transactionsUpdate(data);

          if (res.status == 204) {
            counter = 0;
            break;
          }
          counter--;
        }

        if (res.status) {
          if (res.status !== 204)
            throw new Error(`Error on updating, please try again.`);

          const data = {
            msg: "Transaction was cancelled successfully.",
          };

          return data;
        } else {
          throw new Error(`Error on updating, please try again.`);
        }
      }
    } else {
      // offline
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = toUpdateTransaction;
