const fsqrGrpo = ({ transactions }) => {
  return async function selects(info) {
    const transaction = await transactions.transactionsSelectOneForGrpo({
      info,
    });
    const t = transaction;
    let array = [];
    for (let i = 0; i < t.length; i++) {
      if (i < 1) {
        let weights = [];
        weights.push({
          ibWeight: t[i].IB_WEIGHT ? parseFloat(t[i].IB_WEIGHT).toFixed(2) : 0,
          ibWeigher: t[i].IB_WEIGHER,
          obWeight: t[i].OB_WEIGHT ? parseFloat(t[i].OB_WEIGHT).toFixed(2) : 0,
          obWeigher: t[i].OB_WEIGHER,
          obSign: t[i].U_TS_OB_SIGN,
          ibTime: t[i].IB_UPDATE_TIME,
          obTime: t[i].OB_UPDATE_TIME,
          idWeigherOb: t[i].ID_WEIGHER_OB,
          idWeigherIb: t[i].ID_WEIGHER_IB,
          uom:t[i].U_UOM,
          netWeight: Math.abs(
            parseFloat((t[i].IB_WEIGHT - t[i].OB_WEIGHT).toFixed(2))
          ),
        });

        array.push({
          id: t[i].DocEntry,
          ticket_number: t[i].U_TS_TICKETNUMBER,
          trackingCode: t[i].U_TS_TRCK_CODE,
          plate_number: t[i].U_TS_PLATE_NUM,
          truckModel: t[i].U_TS_TRMODEL,
          driver_name: t[i].DRIVER_NAME,
          num_bags: t[i].U_TS_NUM_BAGS,
          items: t[i].ITEM_LIST,
          weights,
        });
      } else {
        array[0].weights.push({
          ibWeight: t[i].IB_WEIGHT ? parseFloat(t[i].IB_WEIGHT).toFixed(2) : 0,
          ibWeigher: t[i].IB_WEIGHER,
          obWeight: t[i].OB_WEIGHT ? parseFloat(t[i].OB_WEIGHT).toFixed(2) : 0,
          obWeigher: t[i].OB_WEIGHER,
          obSign: t[i].U_TS_OB_SIGN,
          ibTime: t[i].IB_UPDATE_TIME,
          obTime: t[i].OB_UPDATE_TIME,
          idWeigherOb: t[i].ID_WEIGHER_OB,
          idWeigherIb: t[i].ID_WEIGHER_IB,
          netWeight: Math.abs(
            parseFloat((t[i].IB_WEIGHT - t[i].OB_WEIGHT).toFixed(2))
          ),
        });
      }
    }


    return array;
  };
};

module.exports = fsqrGrpo;
