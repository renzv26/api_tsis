const addOutbound = ({
  transactionDb,
  weightDb,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights,
  users,
  accessRights
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "edit transaction"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.source;
      delete info.mode;

      //get transactions
      const transactionDetails = await transactions.transactionsSelectOne({
        info,
      });

      //throw error if transaction does not exist
      if (transactionDetails.status === 404) {
        throw new Error(`Transaction does not exist.`);
      }

      const transactionId = transactionDetails.data.DocEntry;

      //get transaction type id
      const transactionTypeId = transactionDetails.data.U_TS_TRNS_TYPE;

      //get transaction type details
      const transactionTypeDetails = await transactionTypes.transactionTypesSelectById(
        {
          Code: transactionTypeId,
          cookie: info.cookie,
        }
      );

      //get transaction type description
      transactionTypeDescription = transactionTypeDetails.data[0].U_TS_TRNSTYPE;

      // for double scaling net weight
      const trackingCode = transactionDetails.data.U_TS_TRCK_CODE;
      info.trackingCode = trackingCode;

      const check = await transactions.transactionsSelectOneByTrackingCode({ info });
      const transaction = check.data.value;

      if(transaction.length > 1){
        // get final net weight
        const firstNetWeight = transaction[1].BFI_TS_IBTRNCollection[0].U_TS_IB_WEIGHT - transaction[1].BFI_TS_OBTRNCollection[0].U_TS_OB_WEIGHT;
        const secondNetWeight = transaction[0].BFI_TS_IBTRNCollection[0].U_TS_IB_WEIGHT - info.ob_weight;
        info.net_weight = Math.abs(firstNetWeight - secondNetWeight);
      }
      //if transaction is others
      if (transactionTypeDescription === "others") {
        
        //to do validation
        if (!info.ob_weight) {
          throw new Error(`Please input outbound weight`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input date updated`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input time updated`);
        }
        if (!info.sign) {
          throw new Error(`Please input signature`);
        }

        //get line id
        const outboundId =
          transactionDetails.data.BFI_TS_OBTRNCollection[
            transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
          ].LineId;

        //build object for sap
        const outboundDetails = {
          id: transactionId,
          U_TS_STATUS: "completed",
          U_UPDATED_BY: info.userId,
          BFI_TS_OBTRNCollection: [
            {
              LineId: outboundId,
              U_TS_UPDATEDATE: info.updated_date,
              U_TS_UPDATETIME: info.updated_time,
              U_UPDATED_BY: info.userId,
              U_TS_OB_WEIGHT: info.ob_weight,
              U_TS_REMARKS: info.remarks,
              U_TS_OB_SIGN: info.sign,
            },
          ],
          cookie: info.cookie,
        };

        const res = await transactions.transactionsUpdate(outboundDetails);
        return res;
      } else if (
        // transactionTypeDescription === "withdrawal" ||
        transactionTypeDescription === "delivery"
      ) {
        //to do validation
        if (!info.ob_weight) {
          throw new Error(`Please input outbound weight`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input date updated`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input time updated`);
        }
        if (!info.truckscale_id) {
          throw new Error(`Please input truckscale ID`);
        }
        if (!info.sign) {
          throw new Error(`Please input signature`);
        }

        // check if there is PO attached or not;
        // dont allow outbound if there is no PO
        // remove this
        // const po = transactionDetails.data.U_APP_PO_ID;
        // if (!po) {
        //   const d = {
        //     code: 001,
        //     msg: "There must be a PO attached, try again next time."
        //   };
        //   throw new Error(JSON.stringify(d));
        // }

        //get line id
        const outboundId =
          transactionDetails.data.BFI_TS_OBTRNCollection[
            transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
          ].LineId;

        //build object for sap
        const outboundDetails = {
          id: transactionId,
          U_TS_STATUS: "completed",
          U_UPDATED_BY: info.userId,
          U_IS_DONE: info.is_done,
          BFI_TS_OBTRNCollection: [
            {
              LineId: outboundId,
              U_TS_UPDATEDATE: info.updated_date,
              U_TS_UPDATETIME: info.updated_time,
              U_UPDATED_BY: info.userId,
              U_TS_OB_WEIGHT: info.ob_weight,
              U_TS_TRKSCL_ID: info.truckscale_id,
              U_TS_OB_SIGN: info.sign,
              U_TS_REMARKS: info.remarks,

            },
          ],
          cookie: info.cookie,
        };

        if (info.is_done == 1) {

          //login biotech db
          const biotechLogin = await users.dbLogin({
            info: {
              db: process.env.sapDatabase,
              username: process.env.BIOTECH_USER,
              password: process.env.BIOTECH_PASSWORD
            }
          })
          //login revive db
          const reviveLogin = await users.dbLogin({
            info: {
              db: process.env.reviveDB,
              username: process.env.BIOTECH_USER,
              password: process.env.BIOTECH_PASSWORD
            }
          })

          //get cookie
          const biotechCookie = `B1SESSION=${biotechLogin.SessionId}`;
          const reviveCookie = `B1SESSION=${reviveLogin.SessionId}`


         
          // if(!info.fsqr_id){
          //   info.fsqr_id = 0
          // }

           //get draft details
           let draftDetailsBFI;
           let draftDetailsRCI;
          if(info.fsqr_id){
              draftDetailsBFI = await transactions.selectOneDraft({
            info: {
              id: info.fsqr_id,
              cookie: biotechCookie
            }
          })
              draftDetailsRCI = await transactions.selectOneDraft({
            info: {
              id: info.fsqr_id,
              cookie: reviveCookie
            }
          })
        }

          if(draftDetailsBFI){
            if (draftDetailsBFI.data.value.length > 0 && draftDetailsBFI.data.value[0].U_APP_IsDBTran == 0) { 
              //  let batch = ""
              const draftDetailsPost = draftDetailsBFI.data.value[0];
              const docEntry = draftDetailsPost.DocEntry;
              draftDetailsPost.DocumentLines[0].UoMEntry = 42
              draftDetailsPost.DocumentLines[0].Quantity = info.net_weight
              draftDetailsPost.cookie = biotechCookie
              
              // const bfiPO = {
              //   draftDetailsPost,
              //   cookie: biotechCookie,
              // }
              // CardCode: draftDetailsPost.CardCode,
              //     NumAtCard: draftDetailsPost.NumAtCard,
              //     DocDate: info.updated_date,
              //     DocDueDate: info.updated_date,
              //     U_APP_IsDBTran: 0,
              //     U_FSQRTransID: draftDetailsPost.U_FSQRTransID,
              //     U_APP_ProjCode: draftDetailsPost.U_APP_ProjCode,
              //     U_APP_Driver: draftDetailsPost.U_APP_Driver,
              //     U_APP_PlateNo : draftDetailsPost.U_APP_PlateNo,
              //     Comments: draftDetailsPost.Comments,
              //     DocumentLines: [
              //       {
              //         LineNum: 0,
              //         ItemCode: draftDetailsPost.DocumentLines[0].ItemCode,
              //         Quantity: `"${info.net_weight}"`,
              //         UoMEntry: 42,
              //         UnitPrice: draftDetailsPost.DocumentLines[0].UnitPrice,
              //         ProjectCode:draftDetailsPost.DocumentLines[0].ProjectCode,
              //       }
              //     ],

              // post PO with price
              const draftPostToPOBFI = await transactions.postPO({info: {...draftDetailsPost}})
              if(draftPostToPOBFI.status == 201){
                
                const deletePO = {
                  cookie: biotechCookie,
                  id: docEntry
                }
                // delete PO draft after taking details to PO
                const DeletePo = await transactions.deleteDraft({info: {...deletePO} })
  
                if (DeletePo.status != 204) {
                  throw new Error(`${DeletePo.msg}\nData: ${DeletePo.data.error.message.value}`)
                }
              } else {
                throw new Error(`${draftPostToPOBFI.msg}\nData: ${draftPostToPOBFI.data.error.message.value}`)
              }
              // batch = batch + `--a\nContent-Type:multipart/mixed;boundary=b\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\nPATCH /b1s/v1/Drafts(${docEntry})\n\n{\n\t"DocumentLines": [\n\t\t{\n\t\t\t"LineNum": 0,,\n\t\t\t"Quantity": ${info.net_weight},\n\t\t\t"UoMEntry": ${process.env.KILOGRAM_UOM_ID}\n\t\t}\n\t]\n}\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\nPOST /b1s/v1/DraftsService_SaveDraftToDocument\n\n{\n\t"Document": {\n\t\t"DocEntry": "${docEntry}",\n\t\t"BaseLine": ${BaseLine}\n\t}\n}\n\n--b--\n--a--`
  
  
              // const poBatch = await accessRights.batchRequest({ batchString: batch, cookie: biotechCookie })
  
              // const requestPriceApproval = await transactions.addPriceApprovalRequest({
              //   info: {
              //     draftKey: docEntry
              //   }
              // })
  
              // console.log("+=============================================================",docEntry)
  
              // if (requestPriceApproval.status != 201) {
              //   // console.log(requestPriceApproval)
              //   throw new Error(`Price approval request failed`)
              // }
              // console.log(poBatch)
              
            //   if (!poBatch || poBatch.response.includes('400')) {
            //     console.log(poBatch.response);
            //     throw new Error("Transaction failed. Contact the admin");
            // }
  
            // }
            // else{
            //   throw new Error("No Draft Po found")
            // }
          }
          }
          if(draftDetailsRCI){
            if (draftDetailsRCI.data.value.length > 0 && draftDetailsRCI.data.value[0].U_APP_IsDBTran == 0){
              const draftDetailsPost = draftDetailsRCI.data.value[0];
              const docEntry = draftDetailsPost.DocEntry;
              draftDetailsPost.DocumentLines[0].UoMEntry = 3
              draftDetailsPost.DocumentLines[0].Quantity = info.net_weight
              draftDetailsPost.cookie = reviveCookie
  
              // const rciPO = {
              //   cookie: reviveCookie,
              //   CardCode: draftDetailsPost.CardCode,
              //   NumAtCard: draftDetailsPost.NumAtCard,
              //   DocDate: info.updated_date,
              //   DocDueDate: info.updated_date,
              //   U_APP_IsDBTran: "0",
              //   U_FSQRTransID: draftDetailsPost.U_FSQRTransID,
              //   U_APP_ProjCode: draftDetailsPost.U_APP_ProjCode,
              //   U_APP_Driver: draftDetailsPost.U_APP_Driver,
              //   U_APP_PlateNo : draftDetailsPost.U_APP_PlateNo,
              //   Comments: draftDetailsPost.Comments,
              //   DocumentLines: [
              //     {
              //       ItemCode: draftDetailsPost.DocumentLines[0].ItemCode,
              //       Quantity: info.net_weight,
              //       UoMEntry: 3,
              //       UnitPrice: draftDetailsPost.DocumentLines[0].UnitPrice,
              //       ProjectCode:draftDetailsPost.DocumentLines[0].ProjectCode,
              //     }
              //   ]
              // }
  
              // post PO with price
              const draftPostToPORCI = await transactions.postPO({info: {...draftDetailsPost} })
  
              if(draftPostToPORCI.status == 201){
                
                const deletePO = {
                  cookie: reviveCookie,
                  id: docEntry
                }
                // delete PO draft after taking details to PO
                const DeletePo = await transactions.deleteDraft({ info: {...deletePO} })
  
                if (DeletePo.status != 204) {
                  throw new Error(`${DeletePo.msg}\nData: ${DeletePo.data.error.message.value}`)
                }
              } else {
                throw new Error(`${draftPostToPORCI.msg}\nData: ${draftPostToPORCI.data.error.message.value}`)
              }
            }
          } 
          

          const res = await transactions.transactionsUpdate(outboundDetails);
          return res;

        } else {

          const res = await transactions.transactionsUpdate(outboundDetails);
          return res;

        }




      } else if (transactionTypeDescription === "withdrawal") {
        //get line id
        const outboundId =
          transactionDetails.data.BFI_TS_OBTRNCollection[
            transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
          ].LineId;

        //build object for sap
        const outboundDetails = {
          id: transactionId,
          U_TS_STATUS: "completed",
          U_UPDATED_BY: info.userId,
          U_IS_DONE: info.is_done,
          U_UOM:info.uom,
          BFI_TS_OBTRNCollection: [
            {
              LineId: outboundId,
              U_TS_UPDATEDATE: info.updated_date,
              U_TS_UPDATETIME: info.updated_time,
              U_UPDATED_BY: info.userId,
              U_TS_OB_WEIGHT: info.ob_weight,
              U_TS_TRKSCL_ID: info.truckscale_id,
              U_TS_OB_SIGN: info.sign,
              U_TS_REMARKS: info.remarks,

            },
          ],
          cookie: info.cookie,
        };


        const res = await transactions.transactionsUpdate(outboundDetails);
        return res;

      }
    } else {
      // offline mode;

      // check if there is transaction id; doesn't pass thru entity
      if (!info.transaction_id) {
        throw new Error(`Please enter transaction ID.`);
      }
      

      const transactionId = info.transaction_id;

      //get transaction details
      const transactionInfo = await transactionDb.selectOneTransactionById({
        id: transactionId,
      });

      // check if transaction exist
      if (transactionInfo.rowCount === 0) {
        throw new Error(`Transaction does not exist.`);
      }

      // status of the transaction
      const status = transactionInfo.rows[0].status.toLowerCase();

      // transaction type
      const transactionType = transactionInfo.rows[0].transaction_type_name.toLowerCase();
      // if delivery set to tare
      if (transactionType === "delivery") {
        if (status === "completed") {
          // when completed; insert new outbound record together with the inbound id
          // because there is an inbound record already
          const entity = await e_addOutbound(info);

          // get the lates inbound id
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId,
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            // weight type
            const weightType = await weightDb.selectByName({
              weight_name: "Tare",
            });
            const weight_type_id = weightType.rows[0].id;

            const insert = await transactionDb.addOutboundTransaction({
              transaction_id: entity.getTransactionId(),
              weight_type_id,
              ob_weight: entity.getObWeight(),
              ob_timestamp: entity.getObTimestamp(),
              users_id: entity.getUsersId(),
              truckscale_id: entity.getTruckscaleId(),
              drivers_id: entity.getDriversId(),
              truck_info_id: entity.getTruckInfoId(),
              inbound_id,
              remarks: entity.getRemarks(),
            });
            const count = insert.rowCount;

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: entity.getTransactionId(),
              weight_type_id,
              ob_weight: entity.getObWeight(),
              ob_timestamp: entity.getObTimestamp(),
              users_id: entity.getUsersId(),
              truckscale_id: entity.getTruckscaleId(),
              drivers_id: entity.getDriversId(),
              truck_info_id: entity.getTruckInfoId(),
              inbound_id,
              remarks: entity.getRemarks(),
            };

            const logs = {
              action_type:
                "DELIVERY TRANSACTION SPECIAL CASE - INSERT OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values: null,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: entity.getUsersId(),
            };

            await insertActivityLogss({ logs });
            // ################# logs

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`,
            };

            return data;
          } else {
            throw new Error(`No inbound transaction found.`);
          }
        } else {
          if (!info.transaction_id) {
            throw new Error(`Please enter transaction ID.`);
          }
          if (!info.ob_weight) {
            throw new Error(`Please enter outbound weight.`);
          }
          // check if transaction id exist
          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: info.transaction_id,
          });

          if (transactionInfo.rowCount === 0) {
            throw new Error(`Transaction does not exist.`);
          }

          // get inbound id
          // get the lates inbound id
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId,
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            const insert = await transactionDb.updateOutboundTransactionDelivery(
              {
                transaction_id: info.transaction_id,
                ob_weight: info.ob_weight,
                ob_timestamp: new Date().toISOString(),
                inbound_id,
              }
            );

            const count = insert.rowCount;

            const transaction_id = info.transaction_id; // transaction id
            // update status
            const status = `completed`;
            await transactionDb.updateTransactionStatus({
              id: transaction_id,
              status,
            });

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`,
            };

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: info.transaction_id,
              ob_weight: info.ob_weight,
              ob_timestamp: new Date().toISOString(),
              inbound_id,
            };

            const prev_values = {
              transaction_id: info.transaction_id,
              ob_weight: null,
              ob_timestamp: null,
              inbound_id: null,
            };

            const logs = {
              action_type: "DELIVERY TRANSACTION - UPDATE OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values,
              created_at: null,
              updated_at: new Date().toISOString(),
              users_id: info.users_id,
            };

            await insertActivityLogss({ logs });
            // ################# logs

            return data;
          } else {
            throw new Error(`No inbound transaction found.`);
          }
        }
      }

      // if transaction type is withdrawal set to gross
      if (transactionType === "withdrawal") {
        if (status === "completed") {
          throw new Error(`Withdrawal transaction is completed.`);
        } else {
          if (!info.transaction_id) {
            throw new Error(`Please enter transaction ID.`);
          }
          if (!info.ob_weight) {
            throw new Error(`Please enter outbound weight.`);
          }
          // check if transaction id exist
          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: info.transaction_id,
          });

          if (transactionInfo.rowCount === 0) {
            throw new Error(`Transaction does not exist.`);
          }

          // update
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId,
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            const insert = await transactionDb.updateOutboundTransactionDelivery(
              {
                transaction_id: info.transaction_id,
                ob_weight: info.ob_weight,
                ob_timestamp: new Date().toISOString(),
                inbound_id,
              }
            );

            const count = insert.rowCount;

            const transaction_id = info.transaction_id; // transaction id
            // update status
            const status = `completed`;
            await transactionDb.updateTransactionStatus({
              id: transaction_id,
              status,
            });

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`,
            };

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: info.transaction_id,
              ob_weight: info.ob_weight,
              ob_timestamp: new Date().toISOString(),
              inbound_id,
            };

            const prev_values = {
              transaction_id: info.transaction_id,
              ob_weight: null,
              ob_timestamp: null,
              inbound_id: null,
            };

            const logs = {
              action_type: "WITHDRAWAL TRANSACTION - UPDATE OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values,
              created_at: null,
              updated_at: new Date().toISOString(),
              users_id: info.users_id,
            };

            await insertActivityLogss({ logs });
            // ################# logs

            return data;
          }
        }
      }

      // if transaction type is others
      if (transactionType === "others") {
        const weightType = await weightDb.selectByName({ weight_name: "Tare" });

        // check if there is ib weight
        if (!info.ob_weight) {
          throw new Error(`Please enter inbound weight.`);
        }
        // check if  there is truckscale id
        if (!info.truckscale_id) {
          throw new Error(`Please enter truckscale.`);
        }
        const ob_weight = info.ob_weight;
        const truckscale_id = info.truckscale_id;
        const transaction_id = info.transaction_id;
        const ob_timestamp = new Date().toISOString();
        const weight_type_id = weightType.rows[0].id;

        const inbound = await transactionDb.returnLatestInboundFromTransactionId(
          {
            id: transaction_id,
          }
        );
        const inbound_id = inbound.rows[0].id; // update this to outbound record

        // update to db
        const insert = await transactionDb.updateOutboundTransaction({
          transaction_id,
          weight_type_id,
          ob_weight,
          ob_timestamp,
          inbound_id,
          truckscale_id,
        });

        const count = insert.rowCount;

        // update status
        const status = `completed`;
        await transactionDb.updateTransactionStatus({
          id: transaction_id,
          status,
        });

        const data = {
          msg: `Inserted successfully ${count} outbound transaction.`,
        };

        // #################
        // logs
        // new values
        const new_values = {
          transaction_id,
          weight_type_id,
          ob_weight,
          ob_timestamp,
          inbound_id,
          truckscale_id,
        };

        const prev_values = {
          transaction_id,
          weight_type_id: null,
          ob_weight: null,
          ob_timestamp: null,
          inbound_id: null,
          truckscale_id: null,
        };

        const logs = {
          action_type: "OTHERS TRANSACTION - UPDATE OUTBOUND",
          table_affected: "ts_transactions",
          new_values,
          prev_values,
          created_at: null,
          updated_at: new Date().toISOString(),
          users_id: info.users_id,
        };

        await insertActivityLogss({ logs });
        // ################# logs

        return data;
      }
    }
  };
};

module.exports = addOutbound;
