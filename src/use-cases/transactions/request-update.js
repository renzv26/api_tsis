const requestCancel = ({
  transactions,
  validateAccessRights,
  moment,
  requestSelects,
  io,
  nodemailer,
  truckInfos,
  drivers,
  dotenv,
}) => {
  return async function put({ id, ...info } = {}) {
    // for access rights
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const mode = info.mode;
    const modules = info.modules;
    dotenv.config();

    delete info.mode;
    delete info.modules;
    delete info.source;

    if (mode == 1) {
      // online mode
      const { cookie } = info;

      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      // console.log('=---=',t);
      const time = await fixTime(t);

      // to disapprove
      if (info.isDisapprove) {
        const allowed = await validateAccessRights(
          modules,
          "request",
          "disapprove requests"
        );

        if (!allowed) {
          throw new Error(`Access denied`);
        }

        const data = {
          cookie,
          id,
          U_STATUS: "disapproved",
          U_UPDATED_BY: info.updated_by,
          U_UPDATEDATE: date,
          U_UPDATETIME: time,
        };

        const find = await transactions.selectNotifOne(data)
        const val = await transactions.selectNotifOneInNotif(find[0])
        const ticket_number = find[0].U_TS_TICKETNUMBER

        for (let i = 0; i < val.length; i++) {
          const e = val[i];
          const patch = {
            cookie:info.cookie,
            id: e.Code,
            U_IS_READ: 0,
            U_MODIFYBY: info.updated_by,
            U_UPDATEDATE: date,
            U_UPDATETIME: time
          }

          await  transactions.requestNotifUpdate(patch)
        }

        const res = await transactions.requestUpdate(data);

        // for email
        let emailAddressesTo = [];
        let doer = "";

        // get employee name;
        const empId = info.updated_by ? info.updated_by : null;
        const emp = await transactions.getEmployeeName(empId);
        let fn = "";
        let ln = "";
        if (emp.length > 0) {
          fn = emp[0].firstName;
          ln = emp[0].lastName;
          fn_r = emp[0].firstName_revive;
          ln_r = emp[0].lastName_revive;
        }

        // doer = `${fn} ${ln}`; // for email
        doer = fn && ln ? `${fn} ${ln}` : `${fn_r} ${ln_r}`; // for email

        // get all users to be receiver of notifs; all controller and supervisor;
        const user = await transactions.getUserReceiverNotif();
        for (let i = 0; i < user.length; i++) {
          const email = user[i].U_EMAIL;

          emailAddressesTo.push(email); // push receipients of email notif
        }

        const mailInfo = {
          from: `Biotech Farms Inc. `,
          to: emailAddressesTo,
          subject: "Request disapproved from Truckscale Integrated System",
          text: `Good Day Ma'am/Sir,\n\nRequest for Ticket Number ${ticket_number} have been disapproved by ${doer}.\n\nThank you.`,
        };

        // end for email
        if (res.status == 204) {
          // return updated request using socket io; for reload;
          const dummy = {
            isSocket: true,
            modules,
            mode: 1,
            id,
          };
          let req = await requestSelects(dummy);
          req = req[0]; // get object only; not array;
          io.sockets.emit("request", {
            req,
          }); // emit data to all connected clients

          // send email
          await emailNotif({ mailInfo }, nodemailer, dotenv); // send email

          const msg = "Updated successfully.";
          return msg;
        } else {
          throw new Error(`Error on updating, please try again.`);
        }
      }

      // to cancel
      if (info.isCancel) {
        const allowed = await validateAccessRights(
          modules,
          "request",
          "cancel requests"
        );

        if (!allowed) {
          throw new Error(`Access denied`);
        }

        const data = {
          cookie,
          id,
          U_STATUS: "cancelled",
          U_UPDATED_BY: info.updated_by,
          U_UPDATEDATE: date,
          U_UPDATETIME: time,
        };

        const find = await transactions.selectNotifOne(data)
        const val = await transactions.selectNotifOneInNotif(find[0])
        
        for (let i = 0; i < val.length; i++) {
          const e = val[i];
          const patch = {
            cookie:info.cookie,
            id: e.Code,
            U_IS_READ: 0,
            U_MODIFYBY: info.updated_by,
            U_UPDATEDATE: date,
            U_UPDATETIME: time
          }

          await  transactions.requestNotifUpdate(patch)
        }

        const res = await transactions.requestUpdate(data);

        if (res.status == 204) {
          // return updated request using socket io; for reload;
          const dummy = {
            isSocket: true,
            modules,
            mode: 1,
            id,
          };
          let req = await requestSelects(dummy);
          req = req[0]; // get object only; not array;
          io.sockets.emit("request", {
            req,
          }); // emit data to all connected clients

          const msg = "Updated successfully.";
          return msg;
        } else {
          throw new Error(`Error on updating, please try again.`);
        }
      }

      // to approve; and update transaction
      if (info.isApprove) {
        const allowed = await validateAccessRights(
          modules,
          "request",
          "approve requests"
        );

        if (!allowed) {
          throw new Error(`Access denied`);
        }     
        let draft;  
          if(info.draft_data){
            draft = JSON.parse(info.draft_data);
              info.plate_number = draft.plateNumber
            if(!info.plate_number || info.plate_number == ""){
              throw new Error(`Please enter a valid plate number`)
            }
        const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
          { info }
        );
        if (truckInfoDetails.length === 0) {
          const max = await truckInfos.truckInfosGetMaxCode({})
          const maxCode = max[0].maxCode;

          const insertTruckInfo = await truckInfos.truckInfosAddAuto({
              Code: maxCode,
              Name: maxCode,
              U_TS_TRTYPE_ID: 1,
              U_TS_PLATE_NUM: info.plate_number.toLowerCase(),
              U_TS_CREATEDATE: date,
              U_TS_CREATETIME: time,
              U_TS_UPDATEDATE: date,
              U_TS_UPDATETIME: time,
              U_CREATED_BY: info.updated_by,
              U_UPDATED_BY: info.updated_by,
              cookie
           })

           if (insertTruckInfo.status == 201) {
            truckInfoId = maxCode;
          } else {
            throw new Error(`Insert driver failed`);
          }
          // throw new Error(
          //   JSON.stringify({
          //     code: 1,
          //     message: "Plate number does not exist",
          //   })
          // );
        } else {
          truckInfoId = truckInfoDetails[0].Code;
        }

        //split driver to get firstname and lastname
      const driverDetailsFromBody = draft.driver.split(", ");
      if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
        throw new Error(`Invalid driver name format`);
      }

      const driverFirstName = driverDetailsFromBody[1].toLowerCase();
      const driverLastName = driverDetailsFromBody[0].toLowerCase();

      //check driver if exist or not
      const checkDriver = await drivers.driversAddSelectByName({
        info: {
          U_TS_FN: driverFirstName,
          U_TS_LN: driverLastName,
          cookie: info.cookie,
        },
      });

      if (checkDriver.length > 0) {
        driverId = checkDriver[0].Code;
      } else {
        //get Driver Max Code
        const driverMax = await drivers.driversGetMaxCode({});
        const driverMaxCode = driverMax[0].maxCode;

        //insert driver
        const insertDriver = await drivers.driversAdd({
          info: {
            Code: driverMaxCode,
            Name: driverMaxCode,
            U_TS_FN: driverFirstName,
            U_TS_LN: driverLastName,
            U_TS_CREATEDATE: date,
            U_TS_CREATETIME: time,
            U_TS_UPDATEDATE: date,
            U_TS_UPDATETIME: time,
            U_CREATED_BY: info.updated_by,
            U_UPDATED_BY: info.updated_by,
            cookie: info.cookie,
          },
        });

        if (insertDriver.status == 201) {
          driverId = driverMaxCode;
        } else {
          throw new Error(`Insert driver failed`);
        }
      }
    }
        // make batch req
        let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

        const printCount = info.print_count ? parseInt(info.print_count) : 0;
        const currentPrintCount = info.current_print_count
          ? parseInt(info.current_print_count)
          : 0;
        const finalPrintCount = printCount + currentPrintCount;

        // update transaction; always LineId 1; others no special case;
        if (draft) {
          batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/BFITSUDO001(${info.ticket_no}) 

{
  "U_TS_SUPPLIER": "${draft.supplier.toUpperCase()}",
  "U_TS_SUPPLIER_ADD": "${draft.address.toUpperCase()}",
  "U_TS_ITEMCODE": "${draft.itemCode}",
  "U_TS_ITEMNAME": "${draft.item}",
  "U_UOM": "${draft.uom}",
  "U_UOM_TS": "${draft.uom}",
  "U_PRINT_COUNT": ${finalPrintCount},
  "U_TS_NUM_BAGS": ${draft.qty ? draft.qty : 0},
  "U_UPDATED_BY": "${info.updated_by}",
  "BFI_TS_IBTRNCollection": [
    {
      "LineId": 1,
      "U_TS_DRIVERID": "${driverId}",
      "U_TS_TRKINFO": "${truckInfoId}",
      "U_UPDATED_BY": "${info.updated_by}",
      "U_TS_UPDATEDATE": "${date}",
      "U_TS_UPDATETIME": "${time}"
    }
  ],
  "BFI_TS_OBTRNCollection": [
    {
      "LineId": 1,
      "U_TS_DRIVERID": "${driverId}",
      "U_TS_TRKINFO": "${truckInfoId}",
      "U_TS_REMARKS": "${draft.remarks}",
      "U_UPDATED_BY": "${info.updated_by}",
      "U_TS_UPDATEDATE": "${date}",
      "U_TS_UPDATETIME": "${time}"
    }
  ]
}
`;
        } else {
          batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/BFITSUDO001(${info.ticket_no}) 

{
  "U_PRINT_COUNT": ${finalPrintCount},
  "U_UPDATED_BY": "${info.updated_by}"
}
`;
        }

        // update status of request to approved
        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_BFI_TS_REQUESTS('${id}')

{
    U_STATUS: "approved",
    U_UPDATED_BY: "${info.updated_by}",
    U_UPDATEDATE: "${date}",
    U_UPDATETIME: "${time}",
}
`;
        batchStr += `
--b--
--a--        
`;

        let count = 0;
        let counter = 5;

        // for email
        let emailAddressesTo = [];
        let doer = "";

        // get employee name;
        const empId = info.updated_by ? info.updated_by : null;
        const emp = await transactions.getEmployeeName(empId);
        let fn = "";
        let ln = "";
        if (emp.length > 0) {
          fn = emp[0].firstName;
          ln = emp[0].lastName;
          fn_r = emp[0].firstName_revive;
          ln_r = emp[0].lastName_revive;
        }

        // doer = `${fn} ${ln}`; // for email
          doer = fn && ln ? `${fn} ${ln}` : `${fn_r} ${ln_r}`; // for email

          
        const data_notif = {
          cookie,
          id,
          U_STATUS: "approved",
          U_UPDATED_BY: info.updated_by,
          U_UPDATEDATE: date,
          U_UPDATETIME: time,
        };

        const find = await transactions.selectNotifOne(data_notif)
        const val = await transactions.selectNotifOneInNotif(find[0])
        const ticket_number = find[0].U_TS_TICKETNUMBER

        for (let i = 0; i < val.length; i++) {
          const e = val[i];
          const patch = {
            cookie:info.cookie,
            id: e.Code,
            U_IS_READ: 0,
            U_MODIFYBY: info.updated_by,
            U_UPDATEDATE: date,
            U_UPDATETIME: time
          }

          await  transactions.requestNotifUpdate(patch)
        }

        // get all users to be receiver of notifs; all controller and supervisor;
        const user = await transactions.getUserReceiverNotif();
        for (let i = 0; i < user.length; i++) {
          const email = user[i].U_EMAIL;

          emailAddressesTo.push(email); // push receipients of email notif
        }

        const mailInfo = {
          from: `Biotech Farms Inc.`,
          to: emailAddressesTo,
          subject: "Request approved from Truckscale Integrated System",
          text: `Good Day Ma'am/Sir,\n\nRequest for Ticket Number ${ticket_number} have been approved by ${doer}.\n\nThank you.`,
        };
        // end for email

        while (count == 0) {
          // while still not approved
          if (count != 0) break; // approved
          if (counter == 0) break; // loop 5 times still error

          const check = await transactions.checkIfApproved(id);
          count = check[0].COUNTS; // only 0 when not approved;

          //   check if status is change to approved; loop 5 times;
          const data = {
            batchStr,
            cookie,
          };
          const res = await transactions.batchRequest(data);
          // console.log(res);
          counter--;
        }

        if (count !== 0) {
          // return updated request using socket io; for reload;
          const dummy = {
            isSocket: true,
            modules,
            mode: 1,
            id,
          };
          let req = await requestSelects(dummy);
          req = req[0]; // get object only; not array;
          io.sockets.emit("request", {
            req,
          }); // emit data to all connected clients

          // send email
          await emailNotif({ mailInfo }, nodemailer, dotenv); // send email

          const msg = "Updated successfully.";
          return msg;
        } else {
          throw new Error(`Not updated.`);
        }
      }

      // to validate;
      if (info.isValidate) {
        const allowed = await validateAccessRights(
          modules,
          "request",
          "validate requests"
        );

        if (!allowed) {
          throw new Error(`Access denied`);
        }

        const data = {
          cookie,
          id,
          U_STATUS: "validated",
          U_UPDATED_BY: info.updated_by,
          U_UPDATEDATE: date,
          U_UPDATETIME: time,
        };

        const find = await transactions.selectNotifOne(data)
        const val = await transactions.selectNotifOneInNotif(find[0])
        for (let i = 0; i < val.length; i++) {
          const e = val[i];
          const patch = {
            cookie:info.cookie,
            id: e.Code,
            U_IS_READ: 0,
            U_MODIFYBY: info.updated_by,
            U_UPDATEDATE: date,
            U_UPDATETIME: time
          }

          await  transactions.requestNotifUpdate(patch)
        }
        
    
        const res = await transactions.requestUpdate(data);

        if (res.status == 204) {
          // return updated request using socket io; for reload;
          const dummy = {
            isSocket: true,
            modules,
            mode: 1,
            id,
          };
          let req = await requestSelects(dummy);
          req = req[0]; // get object only; not array;
          io.sockets.emit("request", {
            req,
          }); // emit data to all connected clients

          const msg = "Updated successfully.";
          return msg;
        } else {
          throw new Error(`Error on updating, please try again.`);
        }
      }
    } else {
      // offline mode
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

async function emailNotif({ mailInfo }, nodemailer, dotenv) {
  try {
    dotenv.config();

    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });

    const result = await new Promise((resolve) => {
      transporter.sendMail(mailInfo, (err, data) => {
        let msg;
        if (err) {
          msg = {
            status: 400,
            data: `Not sent\n\n${err}`,
          };

          resolve(msg);
        }
        msg = {
          status: 200,
          data: `Sent successfully\n\n ${data}`,
        };

        resolve(msg);
      });
    });
    return result;
  } catch (e) {
    console.log("Error: ", e);
  }
}

module.exports = requestCancel;
