const transactionSelectAll = ({
  transactionDb,
  decrypt,
  transactions,
  transactionTypes,
  drivers,
  truckInfos,
  weightTypes,
  validateAccessRights,
  users,
}) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "view transactions"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }


    if (mode == 1) {
      delete info.modules;
      delete info.source;
      delete info.mode;

      if (typeof info.id !== "undefined") {
        // select one
        let transactionDetails = await transactions.transactionsSelectOne({
          info,
        });
        
        info.trckingCode = transactionDetails.data.U_TS_TRCK_CODE
        if(info.trckingCode){
        const check = await transactions.selectTransactionBySupplierCode(info)
        transactionDetails.data = check.data.value[0]

        let noOfTransaction = 1;
        noOfTransaction = check.data.value.length;
        if(noOfTransaction > 1){
          transactionDetails.data.specialCase = 1
        }
        }
        // throw new Error("test")
        // const secondDetails = await transactions.transactionsSelectOneRaw({info})


        //throw error if transaction does not exist
        if (transactionDetails.status === 404) {
          throw new Error(`Transaction does not exist.`);
        }

        // session expired
        if (transactionDetails.status === 401) {
          throw new Error("Session expired, please login again.");
        }

        const transactionTypeId = transactionDetails.data.U_TS_TRNS_TYPE;

        //get transaction type details
        const transactionTypeDetails = await transactionTypes.transactionTypesSelectById(
          {
            Code: transactionTypeId,
            cookie: info.cookie,
          }
        );

        if (transactionTypeDetails.status !== 200)
          throw new Error(
            `Error on retrieving data, please refresh the page or logout then try again.`
          );

        //get transaction type description
        transactionTypeDescription =
          transactionTypeDetails.data[0].U_TS_TRNSTYPE;

        if (transactionTypeDescription.toLowerCase() === "others") {
          const othersTransactionDetails = await transactions.transactionsSelectOneOthers(
            { info }
          );
          // console.log(othersTransactionDetails.data.value[0], "TRANSAC DETAILS OTHERS")

          const driverId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_DRIVERID;

          const truckInfoId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_TRKINFO;

          const inboundWeightTypeId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;

          const outboundWeightTypeId =
            transactionDetails.data.BFI_TS_OBTRNCollection[
              transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;

          const driverDetails = await drivers.driversSelectOne({
            info: {
              id: driverId,
              cookie: info.cookie,
            },
          });

          const truckInfoDetails = await truckInfos.truckInfosSelectOne({
            info: {
              id: truckInfoId,
              cookie: info.cookie,
            },
          });

          const inboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: inboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          const outboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: outboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          const usersDetails = await users.usersSelectOne({
            info: {
              id:
                transactionDetails.data.BFI_TS_OBTRNCollection[
                  transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
                ].U_TS_CREATEDBY,
              cookie: info.cookie,
            },
          });

          const result = {
            transactionDetails: othersTransactionDetails.data,
            driverDetails: driverDetails.data,
            truckInfoDetails: truckInfoDetails.data,
            inboundDetails: inboundDetails.data,
            outboundDetails: outboundDetails.data,
            weigher: `${usersDetails[0] ? usersDetails[0].lastName : ""}, ${
              usersDetails[0] ? usersDetails[0].firstName : ""
            }`,
          };

          return result;
        } else if (transactionTypeDescription.toLowerCase() === "delivery") {
          const driverId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_DRIVERID;

          const truckInfoId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_TRKINFO;

          const inboundWeightTypeId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;
          const outboundWeightTypeId =
            transactionDetails.data.BFI_TS_OBTRNCollection[
              transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;

          const driverDetails = await drivers.driversSelectOne({
            info: {
              id: driverId,
              cookie: info.cookie,
            },
          });

          const truckInfoDetails = await truckInfos.truckInfosSelectOne({
            info: {
              id: truckInfoId,
              cookie: info.cookie,
            },
          });

          const inboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: inboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          const outboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: outboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          let result;
          if (!transactionDetails.data.U_APP_PO_ID) {
            result = {
              transactionDetails: transactionDetails.data,
              driverDetails: driverDetails.data,
              truckInfoDetails: truckInfoDetails.data,
              inboundDetails: inboundDetails.data,
              outboundDetails: outboundDetails.data,
              transactionType: transactionTypeDescription,
              supplierDetails: null,
              items: null,
            };
          } else {
            const fsqrTransactionDetails = await transactions.selectFsqrTransactionBySupplierCode(
              transactionDetails.data.U_TS_TRCK_CODE
            );

            const fsqrTransactionId = fsqrTransactionDetails[0]
              ? fsqrTransactionDetails[0].Code
              : null;

            const draftPo = await transactions.selectDraftByFsqrTransactionId(
              fsqrTransactionId
            );

            if (draftPo.length > 0) {
              let supplierDetails, items;

              supplierDetails = {
                name: draftPo[0].CardName,
                address: draftPo[0].Address,
              };

              items = draftPo[0].ITEM_LIST;
              transactionDetails.data.rmrsId = fsqrTransactionId;
              
              result = {
                transactionDetails: transactionDetails.data,
                driverDetails: driverDetails.data,
                truckInfoDetails: truckInfoDetails.data,
                inboundDetails: inboundDetails.data,
                outboundDetails: outboundDetails.data,
                transactionType: transactionTypeDescription,
                supplierDetails: supplierDetails,
                items: items,
              };

              
            } else {
              const poDetails = await transactions.selectOnePurchaseOrder({
                info: {
                  id: transactionDetails.data.U_APP_PO_ID,
                  cookie: info.cookie,
                },
              });


              let supplierDetails, items;

              if (poDetails.length > 0) {
                supplierDetails = {
                  name: poDetails[0].CardName,
                  address: poDetails[0].Address,
                };

                items = poDetails[0].ITEM_LIST;
              }

              result = {
                transactionDetails: transactionDetails.data,
                driverDetails: driverDetails.data,
                truckInfoDetails: truckInfoDetails.data,
                inboundDetails: inboundDetails.data,
                outboundDetails: outboundDetails.data,
                transactionType: transactionTypeDescription,
                supplierDetails: supplierDetails,
                items: items,
              };
            }
          }
          return result;
        } else {
          // withdrawal

          const driverId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_DRIVERID;

          const truckInfoId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_TRKINFO;

          const inboundWeightTypeId =
            transactionDetails.data.BFI_TS_IBTRNCollection[
              transactionDetails.data.BFI_TS_IBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;
          const outboundWeightTypeId =
            transactionDetails.data.BFI_TS_OBTRNCollection[
              transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
            ].U_TS_WEIGHT_ID;

          const driverDetails = await drivers.driversSelectOne({
            info: {
              id: driverId,
              cookie: info.cookie,
            },
          });

          const truckInfoDetails = await truckInfos.truckInfosSelectOne({
            info: {
              id: truckInfoId,
              cookie: info.cookie,
            },
          });

          const inboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: inboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          const outboundDetails = await weightTypes.weightTypesSelectById({
            info: {
              id: outboundWeightTypeId,
              cookie: info.cookie,
            },
          });

          let result;
          if (transactionDetails.data.U_APP_SO_ID) {
            // has SO
            const id = transactionDetails.data.U_APP_SO_ID;
            const res = await transactions.getSalesOrderDetails(id);
            const so = [];
            for (let i = 0; i < res.length; i++) {
              const e = res[i];
              so.push({
                id: e.DocEntry,
                docNum: e.DocNum,
                supplier: e.CardName,
                address: e.Address,
                qty: e.QTY,
                uom: e.UNIT,
                item: e.ITEMS,
              });
            }
            result = {
              transactionDetails: transactionDetails.data,
              driverDetails: driverDetails.data,
              truckInfoDetails: truckInfoDetails.data,
              inboundDetails: inboundDetails.data,
              outboundDetails: outboundDetails.data,
              transactionType: transactionTypeDescription,
              so,
            };
          } else {
            // no SO
            result = {
              transactionDetails: transactionDetails.data,
              driverDetails: driverDetails.data,
              truckInfoDetails: truckInfoDetails.data,
              inboundDetails: inboundDetails.data,
              outboundDetails: outboundDetails.data,
              transactionType: transactionTypeDescription,
              so: null,
            };
          }

          return result;
        }
      } else {
        // select all
        let data = [];

        const res = await transactions.getTransactionsNotCompleted({ info });
// console.log(res, "SELEECT TRNS")
        for (let i = 0; i < res.length; i++) {
          const e = res[i];
          data.push({
            created_at: `${e.CreateDate} ${e.CreateTime}`,
            transaction_number: e.DocEntry,
            tracking_code: e.U_TS_TRCK_CODE,
            transaction_type: e.U_TS_TRNSTYPE,
            supplier: e.SUPPLIER,
            items: e.ITEMS,
            numBags: e.U_TS_NUM_BAGS ? e.U_TS_NUM_BAGS : 0,
            ibWeight: e.U_TS_IB_WEIGHT
              ? parseFloat(e.U_TS_IB_WEIGHT).toFixed(2)
              : 0,
            platenumber: e.U_TS_PLATE_NUM,
            status: e.U_TS_STATUS,
            ticket_number: e.U_TS_TICKETNUMBER,
            uom: e.UOM,
            uom_ts: e.U_UOM_TS,
            company: e.U_COMPANY,
            driver: e.DRIVER,
            address: e.U_TS_SUPPLIER_ADD
          });
        }

        return data;
      }
    } else {
      // offline mode
      let result;
      let transactions;
      let data = []; //declare empty array; main array

      let bool = false;
      // empty arrays to store data
      let transactionDetails = [];
      let suppliers = [];
      let user = [];
      let rawMaterial = [];
      let transactionType = [];
      let withdrawalDetails = [];
      let inbound = [];
      let outbound = [];

      // if there is id; select single transaction
      if (typeof info.id !== "undefined") {
        const id = info.id; // param id

        //get transaction details
        const transactionInfo = await transactionDb.selectOneTransactionById({
          id,
        });

        //throw error if transaction does not exist
        if (transactionInfo.rowCount === 0) {
          throw new Error(`Transaction does not exist.`);
        }

        // status of the transaction
        const transactionStatus = transactionInfo.rows[0].status.toLowerCase();
        // transaction type
        const transactionTypes = transactionInfo.rows[0].transaction_type_name.toLowerCase();

        // if delivery
        if (transactionTypes === "delivery") {
          // if status completed
          if (transactionStatus === "completed") {
            result = await transactionDb.selectOneTransactionsSpecialCase({
              id,
            });
            transactions = result.rows;

            for await (let i of transactions) {
              const tType = i.transaction_type_name; // transaction type
              tTypeLower = tType.toLowerCase();

              if (bool === false) {
                // transaction details
                transactionDetails.push({
                  transactionid: i.transactionid,
                  status: i.status,
                  tracking_code: i.tracking_code,
                  no_of_bags: i.no_of_bags,
                });
                // supplier
                suppliers.push({
                  suppliername: i.supplier_name,
                  supplieraddress: i.supplier_address,
                });

                // user who conducted the transaction
                user.push({
                  id: i.userid,
                  firstname: i.firstname ? decrypt(i.firstname) : "",
                  lastname: i.lastname ? decrypt(i.lastname) : "",
                });
                // raw material
                rawMaterial.push({
                  rawmaterial: i.description,
                });

                // transaction type
                transactionType.push({
                  transactiontypeid: i.transactiontypeid,
                  transaction_type_name: i.transaction_type_name,
                });

                // withdrawal data
                withdrawalDetails.push({
                  location_delivery: i.location_delivery,
                  warehouse_location: i.warehouse_location,
                  purchase_order_id: i.purchase_order_id,
                  transmittal_number: i.transmittal_number,
                });
                bool = true;
              }

              // inbound data
              inbound.push({
                id: i.inboundid,
                ib_weight: i.ib_weight,
                ib_timestamp: i.ib_timestamp,
                truckscale: i.intruckscale,
                weightInfo: {
                  id: i.inweighttypeid,
                  name: i.inweightname,
                },
                userInfo: {
                  id: i.inbounduserid,
                  firstname: i.inuserfn ? decrypt(i.inuserfn) : "",
                  lastname: i.inuserln ? decrypt(i.inuserln) : "",
                },
                driverInfo: {
                  id: i.indriverid,
                  firstname: i.indriverfn ? decrypt(i.indriverfn) : "",
                  lastname: i.indriverln ? decrypt(i.indriverln) : "",
                  truckinfo_id: i.truckinfo_id_in,
                  platenumber: i.inplatenumber,
                  trucktype: i.intrucktype,
                  truckmodel: i.intruckmodel,
                  trucksize: i.intrucksize,
                },
              });

              // outbound data
              outbound.push({
                id: i.outboundid,
                ob_weight: i.ob_weight,
                ob_timestamp: i.ob_timestamp,
                truckscale: i.outtruckscale,
                remarks: i.remarks,
                inbound_id: i.inbound_id,
                weightInfo: {
                  id: i.outweighttypeid,
                  name: i.outweightname,
                },
                userInfo: {
                  id: i.outbounduserid,
                  firstname: i.outuserfn ? decrypt(i.outuserfn) : "",
                  lastname: i.outuserln ? decrypt(i.outuserln) : "",
                },
                driverInfo: {
                  id: i.outdriverid,
                  firstname: i.outdriverfn ? decrypt(i.outdriverfn) : "",
                  lastname: i.outdriverln ? decrypt(i.outdriverln) : "",
                  truckinfo_id: i.truckinfo_id_out,
                  platenumber: i.outplatenumber,
                  trucktype: i.outtrucktype,
                  truckmodel: i.outtruckmodel,
                  trucksize: i.outrucksize,
                },
              });
            }

            transactionDetails[0].users = user;
            transactionDetails[0].suppliers = suppliers;
            transactionDetails[0].rawMaterial = rawMaterial;
            transactionDetails[0].transactionType = transactionType;
            transactionDetails[0].inbound = inbound;
            transactionDetails[0].outbound = outbound;
            data.push({
              transactionDetails,
            });
            return data;
          } else {
            result = await transactionDb.selectOneTransactions({ id });
            transactions = result.rows;

            for await (let i of transactions) {
              const tType = i.transaction_type_name; // transaction type
              tTypeLower = tType.toLowerCase();

              if (bool === false) {
                // transaction details
                transactionDetails.push({
                  transactionid: i.transactionid,
                  status: i.status,
                  tracking_code: i.tracking_code,
                  no_of_bags: i.no_of_bags,
                });
                // supplier
                suppliers.push({
                  suppliername: i.supplier_name,
                  supplieraddress: i.supplier_address,
                });

                // user who conducted the transaction
                user.push({
                  id: i.userid,
                  firstname: i.firstname ? decrypt(i.firstname) : "",
                  lastname: i.lastname ? decrypt(i.lastname) : "",
                });
                // raw material
                rawMaterial.push({
                  rawmaterial: i.description,
                });

                // transaction type
                transactionType.push({
                  transactiontypeid: i.transactiontypeid,
                  transaction_type_name: i.transaction_type_name,
                });

                // withdrawal data
                withdrawalDetails.push({
                  location_delivery: i.location_delivery,
                  warehouse_location: i.warehouse_location,
                  purchase_order_id: i.purchase_order_id,
                  transmittal_number: i.transmittal_number,
                });
                bool = true;
              }

              // inbound data
              inbound.push({
                id: i.inboundid,
                ib_weight: i.ib_weight,
                ib_timestamp: i.ib_timestamp,
                truckscale: i.intruckscale,
                weightInfo: {
                  id: i.inweighttypeid,
                  name: i.inweightname,
                },
                userInfo: {
                  id: i.inbounduserid,
                  firstname: i.inuserfn ? decrypt(i.inuserfn) : "",
                  lastname: i.inuserln ? decrypt(i.inuserln) : "",
                },
                driverInfo: {
                  id: i.indriverid,
                  firstname: i.indriverfn ? decrypt(i.indriverfn) : "",
                  lastname: i.indriverln ? decrypt(i.indriverln) : "",
                  truckinfo_id: i.truckinfo_id_in,
                  platenumber: i.inplatenumber,
                  trucktype: i.intrucktype,
                  truckmodel: i.intruckmodel,
                  trucksize: i.intrucksize,
                },
              });

              // outbound data
              outbound.push({
                id: i.outboundid,
                ob_weight: i.ob_weight,
                ob_timestamp: i.ob_timestamp,
                truckscale: i.outtruckscale,
                remarks: i.remarks,
                inbound_id: i.inbound_id,
                weightInfo: {
                  id: i.outweighttypeid,
                  name: i.outweightname,
                },
                userInfo: {
                  id: i.outbounduserid,
                  firstname: i.outuserfn ? decrypt(i.outuserfn) : "",
                  lastname: i.outuserln ? decrypt(i.outuserln) : "",
                },
                driverInfo: {
                  id: i.outdriverid,
                  firstname: i.outdriverfn ? decrypt(i.outdriverfn) : "",
                  lastname: i.outdriverln ? decrypt(i.outdriverln) : "",
                  truckinfo_id: i.truckinfo_id_out,
                  platenumber: i.outplatenumber,
                  trucktype: i.outtrucktype,
                  truckmodel: i.outtruckmodel,
                  trucksize: i.outrucksize,
                },
              });
            }

            transactionDetails[0].users = user;
            transactionDetails[0].suppliers = suppliers;
            transactionDetails[0].rawMaterial = rawMaterial;
            transactionDetails[0].transactionType = transactionType;
            transactionDetails[0].inbound = inbound;
            transactionDetails[0].outbound = outbound;
            data.push({
              transactionDetails,
            });
            return data;
          }
        }
        // if withdrawal
        if (transactionTypes === "withdrawal") {
          result = await transactionDb.selectOneTransactions({ id });
          transactions = result.rows;

          for await (let i of transactions) {
            const tType = i.transaction_type_name; // transaction type
            tTypeLower = tType.toLowerCase();

            if (bool === false) {
              // transaction details
              transactionDetails.push({
                transactionid: i.transactionid,
                status: i.status,
                tracking_code: i.tracking_code,
                no_of_bags: i.no_of_bags,
              });
              // supplier
              suppliers.push({
                suppliername: i.supplier_name,
                supplieraddress: i.supplier_address,
              });

              // user who conducted the transaction
              user.push({
                id: i.userid,
                firstname: i.firstname ? decrypt(i.firstname) : "",
                lastname: i.lastname ? decrypt(i.lastname) : "",
              });
              // raw material
              rawMaterial.push({
                rawmaterial: i.description,
              });

              // transaction type
              transactionType.push({
                transactiontypeid: i.transactiontypeid,
                transaction_type_name: i.transaction_type_name,
              });

              // withdrawal data
              withdrawalDetails.push({
                location_delivery: i.location_delivery,
                warehouse_location: i.warehouse_location,
                purchase_order_id: i.purchase_order_id,
                transmittal_number: i.transmittal_number,
              });
              bool = true;
            }

            // inbound data
            inbound.push({
              id: i.inboundid,
              ib_weight: i.ib_weight,
              ib_timestamp: i.ib_timestamp,
              truckscale: i.intruckscale,
              weightInfo: {
                id: i.inweighttypeid,
                name: i.inweightname,
              },
              userInfo: {
                id: i.inbounduserid,
                firstname: i.inuserfn ? decrypt(i.inuserfn) : "",
                lastname: i.inuserln ? decrypt(i.inuserln) : "",
              },
              driverInfo: {
                id: i.indriverid,
                firstname: i.indriverfn ? decrypt(i.indriverfn) : "",
                lastname: i.indriverln ? decrypt(i.indriverln) : "",
                truckinfo_id: i.truckinfo_id_in,
                platenumber: i.inplatenumber,
                trucktype: i.intrucktype,
                truckmodel: i.intruckmodel,
                trucksize: i.intrucksize,
              },
            });

            // outbound data
            outbound.push({
              id: i.outboundid,
              ob_weight: i.ob_weight,
              ob_timestamp: i.ob_timestamp,
              truckscale: i.outtruckscale,
              remarks: i.remarks,
              inbound_id: i.inbound_id,
              weightInfo: {
                id: i.outweighttypeid,
                name: i.outweightname,
              },
              userInfo: {
                id: i.outbounduserid,
                firstname: i.outuserfn ? decrypt(i.outuserfn) : "",
                lastname: i.outuserln ? decrypt(i.outuserln) : "",
              },
              driverInfo: {
                id: i.outdriverid,
                firstname: i.outdriverfn ? decrypt(i.outdriverfn) : "",
                lastname: i.outdriverln ? decrypt(i.outdriverln) : "",
                truckinfo_id: i.truckinfo_id_out,
                platenumber: i.outplatenumber,
                trucktype: i.outtrucktype,
                truckmodel: i.outtruckmodel,
                trucksize: i.outrucksize,
              },
            });
          }

          transactionDetails[0].users = user;
          transactionDetails[0].withdrawalDetails = withdrawalDetails;
          transactionDetails[0].suppliers = suppliers;
          transactionDetails[0].rawMaterial = rawMaterial;
          transactionDetails[0].transactionType = transactionType;
          transactionDetails[0].inbound = inbound;
          transactionDetails[0].outbound = outbound;
          data.push({
            transactionDetails,
          });
          return data;
        }
        // if others
        if (transactionTypes === "others") {
          result = await transactionDb.selectOneTransactions({ id });
          transactions = result.rows;

          for await (let i of transactions) {
            const tType = i.transaction_type_name; // transaction type
            tTypeLower = tType.toLowerCase();

            if (bool === false) {
              // transaction details
              transactionDetails.push({
                transactionid: i.transactionid,
                status: i.status,
                tracking_code: i.tracking_code,
                no_of_bags: i.no_of_bags,
              });
              // supplier
              suppliers.push({
                supplierid: i.supplierid,
                suppliername: i.suppliername,
                supplieraddress: i.supplieraddress,
              });

              // user who conducted the transaction
              user.push({
                id: i.userid,
                firstname: i.firstname ? decrypt(i.firstname) : "",
                lastname: i.lastname ? decrypt(i.lastname) : "",
              });
              // raw material
              rawMaterial.push({
                rawmaterialid: i.rawmaterialid,
                rawmaterial: i.rawmaterial,
              });

              // transaction type
              transactionType.push({
                transactiontypeid: i.transactiontypeid,
                transaction_type_name: i.transaction_type_name,
              });

              // withdrawal data
              withdrawalDetails.push({
                location_delivery: i.location_delivery,
                warehouse_location: i.warehouse_location,
                purchase_order_id: i.purchase_order_id,
                transmittal_number: i.transmittal_number,
              });
              bool = true;
            }

            // inbound data
            inbound.push({
              id: i.inboundid,
              ib_weight: i.ib_weight,
              ib_timestamp: i.ib_timestamp,
              truckscale: i.intruckscale,
              weightInfo: {
                id: i.inweighttypeid,
                name: i.inweightname,
              },
              userInfo: {
                id: i.inbounduserid,
                firstname: i.inuserfn ? decrypt(i.inuserfn) : "",
                lastname: i.inuserln ? decrypt(i.inuserln) : "",
              },
              driverInfo: {
                id: i.indriverid,
                firstname: i.indriverfn ? decrypt(i.indriverfn) : "",
                lastname: i.indriverln ? decrypt(i.indriverln) : "",
                truckinfo_id: i.truckinfo_id_in,
                platenumber: i.inplatenumber,
                trucktype: i.intrucktype,
                truckmodel: i.intruckmodel,
                trucksize: i.intrucksize,
              },
            });

            // outbound data
            outbound.push({
              id: i.outboundid,
              ob_weight: i.ob_weight,
              ob_timestamp: i.ob_timestamp,
              truckscale: i.outtruckscale,
              remarks: i.remarks,
              inbound_id: i.inbound_id,
              weightInfo: {
                id: i.outweighttypeid,
                name: i.outweightname,
              },
              userInfo: {
                id: i.outbounduserid,
                firstname: i.outuserfn ? decrypt(i.outuserfn) : "",
                lastname: i.outuserln ? decrypt(i.outuserln) : "",
              },
              driverInfo: {
                id: i.outdriverid,
                firstname: i.outdriverfn ? decrypt(i.outdriverfn) : "",
                lastname: i.outdriverln ? decrypt(i.outdriverln) : "",
                truckinfo_id: i.truckinfo_id_out,
                platenumber: i.outplatenumber,
                trucktype: i.outtrucktype,
                truckmodel: i.outtruckmodel,
                trucksize: i.outrucksize,
              },
            });
          }

          transactionDetails[0].users = user;
          transactionDetails[0].suppliers = suppliers;
          transactionDetails[0].rawMaterial = rawMaterial;
          transactionDetails[0].transactionType = transactionType;
          transactionDetails[0].inbound = inbound;
          transactionDetails[0].outbound = outbound;
          data.push({
            transactionDetails,
          });
          return data;
        }
        // end
      } else {
        if (!info.from || !info.to) {
          throw new Error(`Please enter input date filter.`);
        }

        const from = info.from;
        const to = info.to;
        const data = {
          from,
          to,
        };
        result = await transactionDb.selectAllTransactions({ data });
        transactions = result.rows;


        return transactions;
      }
    }
  };
};

module.exports = transactionSelectAll;
