const addNewTransaction = ({
    transactions,
    users
}) => {
    return async function posts(info) {

        const tsisLogin = await users.dbLogin({
            info: {
                db: process.env.DB,
                username: process.env.USERS,
                password: process.env.PW
            }
        })

        const transactionDetails = {
            U_TS_TRNS_TYPE: '3',
            U_TS_STATUS: 'others',
            U_TS_NUM_BAGS: '22',
            U_TS_SUPPLIER: 'E.B. TESTING CENTER, INC.',
            U_TS_SUPPLIER_ADD: 'MORALES AVENUE, BRGY. GPS, KORONADAL CITY',
            U_TS_ITEMCODE: 'FG21-00004',
            U_TS_ITEMNAME: 'PADDY',
            U_UOM: 'BAG',
            U_PRINT_COUNT: 5,
            U_CREATED_BY: '1',
            U_UPDATED_BY: '1',
            BFI_TS_IBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: '2',
                    U_TS_CREATEDBY: '1',
                    U_UPDATED_BY: '1',
                    U_TS_TRKSCL_ID: '4',
                    U_TS_DRIVERID: '1',
                    U_TS_TRKINFO: '1',
                    U_TS_CREATEDATE: '2020-08-14',
                    U_TS_CREATETIME: '1040',
                    U_TS_UPDATEDATE: '2020-08-14',
                    U_TS_UPDATETIME: '1040'
                }
            ],
            BFI_TS_OBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: '1',
                    U_TS_CREATEDBY: '1',
                    U_UPDATED_BY: '1',
                    U_TS_TRKSCL_ID: '4',
                    U_TS_DRIVERID: '1',
                    U_TS_TRKINFO: '1',
                    U_TS_CREATEDATE: '2020-08-14',
                    U_TS_CREATETIME: '1040',
                    U_TS_UPDATEDATE: '2020-08-14',
                    U_TS_UPDATETIME: '1040'
                }
            ],
            cookie: `B1SESSION=${tsisLogin.SessionId}`,
        };

        const res = await transactions.transactionsAdd(transactionDetails);

        return res

    }

};

module.exports = addNewTransaction;
