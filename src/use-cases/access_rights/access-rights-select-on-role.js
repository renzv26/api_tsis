const accessRightsSelectAllOnRole = ({ accessRightsDb, accessRights }) => {
  return async function selects(info) {
    const { id, mode } = info;
    delete info.mode;
    delete info.source;
    if (mode == 1) {
      const res = await accessRights.arSelectOnRole({ info });
      let modules = []; // declare empty array; to store modules
      let actions = []; // declare empty array; to store actions

      const status = res.status;
      if (status == 401) {
        // session expired
        throw new Error('Session expired, please login again.')
      } else {
        for await (let i of res) {
          //   push all modules
          modules.push({
            modules_id: i.U_BFI_TS_MOD.Code,
            modulename: i.U_BFI_TS_MOD.U_TS_DESC,
            modulestatus: i.U_BFI_TS_MOD.U_TS_STATUS
          });

          //   push all actions
          actions.push({
            modules_id: i.U_BFI_TS_ACT.U_TS_MOD_ID,
            actions_id: i.U_BFI_TS_ACT.Code,
            actionname: i.U_BFI_TS_ACT.U_TS_DESC,
            actionstatus: i.U_BFI_TS_ACT.U_TS_STATUS
          });
        }

        // filter the modules array; no duplicates allowed; filter thru modules_id which is the PK
        const filterModules = filterArrayNoDuplicates(modules, "modules_id");

        // filter the actions array; no duplicates allowed; filter thru actions_id which is the PK
        const filterActions = filterArrayNoDuplicates(actions, "actions_id");

        for await (let i of filterModules) {
          const id = i.modules_id; // modules id
          let dummyActions = []; // declare empty array where we will store all the actions in which module it belongs

          for await (let x of filterActions) {
            const m_id = x.modules_id; // modules id in actions array

            if (id == m_id) {
              dummyActions.push(x);
            }
          }

          //   append to filter modules object it's corresponding actions with actions attribute
          i.actions = dummyActions;
        }
        return filterModules;
      }
    } else {
      // offline mode
      const result = await accessRightsDb.selectAccessRightsOnRole({ id });
      const accessRights = result.rows;

      let modules = []; // declare empty array; to store modules
      let actions = []; // declare empty array; to store actions

      for (let i = 0; i < accessRights.length; i++) {
        const e = accessRights[i];

        //   push all modules
        modules.push({
          modules_id: e.modules_id,
          modulename: e.modulename,
          modulestatus: e.modulestatus
        });

        //   push all actions
        actions.push({
          modules_id: e.modules_id,
          actions_id: e.actions_id,
          actionname: e.actionname,
          actionstatus: e.actionstatus
        });
      }

      // push to main array the roles array
      // await data.push({ roles });
      // filter the modules array; no duplicates allowed; filter thru modules_id which is the PK
      const filterModules = filterArrayNoDuplicates(modules, "modules_id");

      // filter the actions array; no duplicates allowed; filter thru actions_id which is the PK
      const filterActions = filterArrayNoDuplicates(actions, "actions_id");

      // loop thru filtered modules array
      for (let i = 0; i < filterModules.length; i++) {
        const module_id = filterModules[i].modules_id; // module id
        let dummyActions = []; // declare empty array where we will store all the actions in which module it belongs

        //   loop thru filtered actions
        for (let x = 0; x < filterActions.length; x++) {
          const data = filterActions[x];

          // if module id are equal; then push to dummy array
          if (module_id === data.modules_id) {
            dummyActions.push(data);
          }
        }

        //   append to filter modules object it's corresponding actions with actions attribute
        filterModules[i].actions = dummyActions;
      }

      // return main data of array
      return filterModules;
    }
  };
};

// function to remove duplicate elements in array
// arr = is the array
// comp = is the component you want it to filter with
const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

module.exports = accessRightsSelectAllOnRole;
