const addAccessRights = ({
  insertActivityLogss,
  accessRightsDb,
  makeAccessRights,
  accessRights
}) => {
  return async function posts(info) {
    const { mode } = info;
    delete info.mode; //remove mode
    delete info.source; //remove source
    if (mode == 1) {
      const arr = info.U_TS_ACTION_ID;

      // continue here
    } else {
      // array of new access rights
      const roleId = info.id;
      const arr = info.access_rights;

      // if no actions selected; just delete the actions
      if (arr.length === 0) {
        await accessRightsDb.deleteAccessRightsOnRole({
          roles_id: roleId
        });
      }

      // to check if no missing data
      for await (let d of arr) {
        await makeAccessRights({ roles_id: roleId, actions_id: d });
      }

      // to insert to db
      let count = 0; // to count how many data were inserted
      let deleteCount = 0; // count how many data were deleted
      let bool = false; //use to exit from loop; just delete once

      for await (let d of arr) {
        const result = makeAccessRights({ roles_id: roleId, actions_id: d });

        if (bool === false) {
          const deleted = await accessRightsDb.deleteAccessRightsOnRole({
            roles_id: result.getRolesId()
          });
          deleteCount = deleted.rowCount; //deleted data count
          bool = true;
        }
        await accessRightsDb.insertAccessRights({
          actions_id: result.getActionsId(),
          roles_id: result.getRolesId(),
          created_at: result.getCreatedAt()
        });
        count++;
      }

      const data = {
        msg: `Inserted successfully ${count} access rights. Deleted access rights: ${deleteCount}.`
      };

      return data;
    }
  };
};

module.exports = addAccessRights;
