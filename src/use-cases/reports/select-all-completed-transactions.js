const completedTransactionSelectAll = ({
  reportsDb,
  decrypt,
  transactions,
  transactionTypes,
  weightTypes,
  drivers,
  truckInfos,
  validateAccessRights,
  users,
}) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "report",
      "view reports"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.source;
      delete info.mode;

      if (!info.from || !info.to) {
        throw new Error(`Please input date from and to`);
      }

      // get header
      const res = await transactions.getReportHeader({ info });
// console.log(res, "REeport head")
      const data = []; // final array to return
      const processedCode = []; // transactions exist in array already;

      // append details
      for (let i = 0; i < res.length; i++) {
        const e = res[i];
        // get po items
        if (e.U_APP_PO_ID) {
          // get po details;
          const id = e.U_APP_PO_ID;
          let po = await transactions.getPOdetails(id);
          e.po = po; // append
        }
        // get so items
        if (e.U_APP_SO_ID) {
          // get so details;
          const id = e.U_APP_SO_ID;
          let so = await transactions.getSOdetails(id);
          e.so = so;
          // console.log(e.so);
        }
      }

        for (let i = 0; i < res.length; i++) {
          const e = res[i];

          // if code already exist
          if (processedCode.includes(e.DocEntry)) {
            const index = data.findIndex(
              (x) => x.transaction_id === e.DocEntry
            );

            // not yet exist
            let nett = parseFloat(
              parseFloat(e.IB_WEIGHT ? e.IB_WEIGHT : 0) -
                parseFloat(e.OB_WEIGHT ? e.OB_WEIGHT : 0)
            ).toFixed(2);
            let net = Math.abs(nett)
            data[index].weight.push({
              ib: e.IB_WEIGHT ? e.IB_WEIGHT : 0,
              ib_remarks: e.IB_REMARKS ? e.IB_REMARKS : "",
              ib_weigher: e.IB_WEIGHER ? e.IB_WEIGHER : "",
              ib_date: e.IB_UPDATE_DATE,
              ib_time: e.IB_UPDATE_TIME,
              ob: e.OB_WEIGHT ? e.OB_WEIGHT : 0,
              net,
              ob_remarks: e.OB_REMARKS ? e.OB_REMARKS : "",
              ob_weigher: e.OB_WEIGHER ? e.OB_WEIGHER : "",
              ob_date: e.OB_UPDATE_DATE,
              ob_time: e.OB_UPDATE_TIME,
              signature: e.OB_SIGN ? e.OB_SIGN : "",
            });
          } else {
            // not yet exist
            let nett = parseFloat(
              parseFloat(e.IB_WEIGHT ? e.IB_WEIGHT : 0) -
                parseFloat(e.OB_WEIGHT ? e.OB_WEIGHT : 0)
            ).toFixed(2);
            let net = Math.abs(nett)
            // weight details
            let weight = [
              {
                ib: e.IB_WEIGHT ? e.IB_WEIGHT : 0,
                ib_remarks: e.IB_REMARKS ? e.IB_REMARKS : "",
                ib_weigher: e.IB_WEIGHER ? e.IB_WEIGHER : "",
                ib_date: e.IB_UPDATE_DATE,
                ib_time: e.IB_UPDATE_TIME,
                ob: e.OB_WEIGHT ? e.OB_WEIGHT : 0,
                net,
                ob_remarks: e.OB_REMARKS ? e.OB_REMARKS : "",
                ob_weigher: e.OB_WEIGHER ? e.OB_WEIGHER : "",
                ob_date: e.OB_UPDATE_DATE,
                ob_time: e.OB_UPDATE_TIME,
                signature: e.OB_SIGN ? e.OB_SIGN : "",
              },
            ];

            // formatted transaction type
            let ttype = `${e.U_COMPANY ? convertCase(e.U_COMPANY) : "N/A"} (${
              e.U_TS_TRNSTYPE ? convertCase(e.U_TS_TRNSTYPE) : "N/A"
            })`;

            
            let supplier = null;

            if (e.U_APP_PO_ID) {
              for (let i = 0; i < e.po.length; i++) {
                const a = e.po[i];
                supplier = a.CardName;
                break;
              }
            }
            if (e.U_APP_SO_ID) {
              for (let i = 0; i < e.so.length; i++) {
                const a = e.so[i];

                supplier = a.CardName;
                break;
              }
            }
            if (e.U_TS_SUPPLIER) {
              supplier = e.U_TS_SUPPLIER;
            } else {
              supplier = "N/A";
            }

            // get item
            let item = "";
            if (e.U_APP_PO_ID) {
              for (let i = 0; i < e.po.length; i++) {
                const a = e.po[i];

                if (i == e.po.length - 1) item += `${a.Dscription}`;
                else item += `${a.Dscription}, `;
              }
            }
            if (e.U_APP_SO_ID) {
              for (let i = 0; i < e.so.length; i++) {
                const a = e.so[i];

                if (i == e.so.length - 1) item += `${a.Dscription}`;
                else item += `${a.Dscription}, `;
              }
            } else {
              item = e.U_TS_ITEMNAME;
            }

            // get qty and uom
            let qtyUom = "";
            let qty = [];
            let uom = [];
            let uomTs = [];
            if (e.U_APP_PO_ID) {
              for (let i = 0; i < e.po.length; i++) {
                const a = e.po[i];
                // console.log(a, "PURCHASE ORDER")
                qty.push(a.Quantity ? parseFloat(a.Quantity) : 0);
                uom.push(a.unitMsr ? a.unitMsr : "");

                if (i == e.po.length - 1)
                  qtyUom += `${a.Quantity ? parseFloat(a.Quantity) : 0} ${
                    a.unitMsr ? a.unitMsr : ""
                  }`;
                else
                  qtyUom += `${a.Quantity ? parseFloat(a.Quantity) : 0} ${
                    a.unitMsr ? a.unitMsr : ""
                  }, `;
              }
            }
            if (e.U_APP_SO_ID) {
              for (let i = 0; i < e.so.length; i++) {
                const a = e.so[i];
                // console.log(a, "SALES ORDER")
                qty.push(a.Quantity ? parseFloat(a.Quantity) : 0);
                uom.push(a.unitMsr ? a.unitMsr : "");

                if (i == e.so.length - 1)
                  qtyUom += `${a.Quantity ? parseFloat(a.Quantity) : 0} ${
                    a.unitMsr ? a.unitMsr : ""
                  }`;
                else
                  qtyUom += `${a.Quantity ? parseFloat(a.Quantity) : 0} ${
                    a.unitMsr ? a.unitMsr : ""
                  }, `;
              }
            } else {
              qty.push(e.U_TS_NUM_BAGS ? e.U_TS_NUM_BAGS : 0);
              uom.push(e.U_UOM ? e.U_UOM : "");
              uomTs.push(e.U_UOM_TS ? e.U_UOM_TS : "");
              // for adding in transaction manually for others
              if(e.U_COMPANY == null){
                qtyUom = `${e.U_TS_NUM_BAGS ? e.U_TS_NUM_BAGS : 0} ${
                  e.U_UOM_TS ? e.U_UOM_TS : ""
                }`;
              }else{
                qtyUom = `${e.U_TS_NUM_BAGS ? e.U_TS_NUM_BAGS : 0} ${
                  e.U_UOM_TS ? e.U_UOM_TS : ""
                }`;
              }
              
            }

            // main data
            data.push({
              created_at: `${e.CREATEDATE} ${intToTime(e.CreateTime)}`,
              tracking_code: e.U_TS_TRCK_CODE ? e.U_TS_TRCK_CODE : "",
              transaction_id: e.DocEntry,
              company: e.U_COMPANY ? convertCase(e.U_COMPANY) : "N/A",
              transaction_type: e.U_TS_TRNSTYPE
                ? convertCase(e.U_TS_TRNSTYPE)
                : "N/A",
              formatted_transaction_type: ttype,
              plate_number: e.U_TS_PLATE_NUM
                ? e.U_TS_PLATE_NUM.toUpperCase()
                : "",
              supplier: supplier,
              address: e.U_TS_SUPPLIER_ADD,
              raw_material: item ? item.trim() : "",
              driver: convertCase(e.DRIVER_NAME),
              quantity: qtyUom,
              qty,
              uom,
              uomTs,
              status: e.U_TS_STATUS ? convertCase(e.U_TS_STATUS) : null,
              ticket_number: e.U_TS_TICKETNUMBER,
              plot_code: e.U_TS_PLOT_CODE,
              dr_number: e.U_TS_DRNUMBER,
              weight,
            });

            processedCode.push(e.DocEntry); // push code; already done
          }
        }

        return data;

      /*let finalView = [];

      let processedCode = [];
      console.log(res);
      if (res.length > 0) {
        for (let i = 0; i < res.length; i++) {
          let data = res[i];
          //special case
          if (processedCode.includes(data.DocEntry)) {
            const index = finalView.findIndex((x) => x.id === data.DocEntry);

            finalView[index].weightDetails.push({
              ibWeight: data.IB_WEIGHT,
              ibUpdateDate: data.IB_UPDATE_DATE,
              ibUpdateTime: data.IB_UPDATE_TIME,
              ibWeigher: data.IB_WEIGHER,
              ibRemarks: data.IB_REMARKS,
              obWeight: data.OB_WEIGHT,
              obRemarks: data.OB_REMARKS,
              obUpdateDate: data.OB_UPDATE_DATE,
              obUpdateTime: data.OB_UPDATE_TIME,
              obSign: data.OB_SIGN,
              obWeigher: data.OB_WEIGHER,
            });
          } else {
            let weights = [];

            weights.push({
              ibWeight: data.IB_WEIGHT,
              ibUpdateDate: data.IB_UPDATE_DATE,
              ibUpdateTime: data.IB_UPDATE_TIME,
              ibWeigher: data.IB_WEIGHER,
              ibRemarks: data.IB_REMARKS,
              obWeight: data.OB_WEIGHT,
              obRemarks: data.OB_REMARKS,
              obUpdateDate: data.OB_UPDATE_DATE,
              obUpdateTime: data.OB_UPDATE_TIME,
              obSign: data.OB_SIGN,
              obWeigher: data.OB_WEIGHER,
            });

            finalView.push({
              id: data.DocEntry,
              createDate: data.CREATEDATE,
              createTime: data.CreateTime,
              status: data.U_TS_STATUS,
              transactionType: `${data.U_COMPANY} (${data.U_TS_TRNSTYPE}) `,
              driverName: data.DRIVER_NAME,
              plateNumber: data.U_TS_PLATE_NUM,
              truckType: data.U_TS_TRTYPE,
              truckModel: data.U_TS_TRMODEL,
              supplier: data.CardName,
              supplierAddress: data.Address,
              trackingCode: data.U_TS_TRCK_CODE,
              numberOfBags: data.U_TS_NUM_BAGS,
              isNapier: data.U_TS_IS_NAPIER,
              supplier: data.U_TS_SUPPLIER,
              address: data.U_TS_SUPPLIER_ADD,
              item: data.U_TS_ITEMNAME,
              weightDetails: weights,
              po: data.po ? data.po : null,
              so: data.so ? data.so : null,
            });
          }

          processedCode.push(data.DocEntry);
        }
      } else {
        throw new Error("No data to display.");
      }*/
    } else {
      // offline mode
      const result = await reportsDb.selectAllCompletedTransaction({
        from: info.from,
        to: info.to,
      });
      const report = result.rows;

      let data = [];
      let res = [];
      // populate all data
      for await (let i of report) {
        const transactionId = i.id; // transaction id

        const weigher = {
          firstname: decrypt(i.wfirstname),
          lastname: decrypt(i.wlastname),
        };

        let inbound = [];
        let outbound = [];
        const driver = {
          firstname: decrypt(i.firstname),
          lastname: decrypt(i.lastname),
        };

        inbound.push({
          id: i.inbound_id ? i.inbound_id : "N/A",
          transactionId: i.transaction_id_ib ? i.transaction_id_ib : "N/A",
          weight: i.ib_weight ? i.ib_weight : 0,
          timestamp: i.ib_timestamp ? i.ib_timestamp : "N/A",
        });
        outbound.push({
          id: i.outbound_id ? i.outbound_id : "N/A",
          transactionId: i.transaction_id_ob ? i.transaction_id_ob : "N/A",
          weight: i.ob_weight ? i.ob_weight : 0,
          timestamp: i.ob_timestamp ? i.ob_timestamp : "N/A",
          inbound_id: i.ob_inbound_id ? i.ob_inbound_id : "N/A",
        });

        if (data.length > 0) {
          const previousTransactionId = data[data.length - 1].id; // previous transaction
          // if the same transaction id; for delivery
          if (transactionId == previousTransactionId) {
            // append inbound weight
            data[data.length - 1].inbound.push({
              id: i.inbound_id ? i.inbound_id : "N/A",
              transactionId: i.transaction_id_ib ? i.transaction_id_ib : "N/A",
              weight: i.ib_weight ? i.ib_weight : 0,
              timestamp: i.ib_timestamp ? i.ib_timestamp : "N/A",
            });

            // append outbound weight
            data[data.length - 1].outbound.push({
              id: i.outbound_id ? i.outbound_id : "N/A",
              transactionId: i.transaction_id_ob ? i.transaction_id_ob : "N/A",
              weight: i.ob_weight ? i.ob_weight : 0,
              timestamp: i.ob_timestamp ? i.ob_timestamp : "N/A",
            });
          } else {
            await data.push({
              id: i.id,
              transaction_type: i.transaction_type_name,
              created_at: i.created_at,
              purchase_order_id: i.purchase_order_id,
              tracking_code: i.tracking_code,
              transmittal_number: i.transmittal_number,
              no_of_bags: i.no_of_bags,
              raw_material: i.description,
              supplier: i.supplier_name,
              weigher,
              driver,
              plate_number: i.plate_number,
              status: i.status,
              inbound,
              outbound,
            });
          }
        } else {
          await data.push({
            id: i.id,
            transaction_type: i.transaction_type_name,
            created_at: i.created_at,
            purchase_order_id: i.purchase_order_id,
            tracking_code: i.tracking_code,
            transmittal_number: i.transmittal_number,
            no_of_bags: i.no_of_bags,
            raw_material: i.description,
            supplier: i.supplier_name,
            weigher,
            driver,
            plate_number: i.plate_number,
            status: i.status,
            inbound,
            outbound,
          });
        }
      }

      // filter data
      for await (let i of data) {
        await res.push({
          id: i.id,
          transaction_type: i.transaction_type,
          created_at: i.created_at,
          purchase_order_id: i.purchase_order_id,
          tracking_code: i.tracking_code,
          transmittal_number: i.transmittal_number,
          no_of_bags: i.no_of_bags,
          raw_material: i.raw_material,
          supplier: i.supplier,
          weigher: i.weigher,
          driver: i.driver,
          plate_number: i.plate_number,
          status: i.status,
          inbound: i.inbound ? filterArrayNoDuplicates(i.inbound, "id") : null,
          outbound: i.outbound
            ? filterArrayNoDuplicates(i.outbound, "id")
            : null,
        });
      }

      return res;
    }
  };
};

// function to remove duplicate elements in array
// arr = is the array
// comp = is the component you want it to filter with
const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map((e) => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter((e) => arr[e])
    .map((e) => arr[e]);

  return unique;
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

// format string
const convertCase = (str) => {
  if (str) {
    const splitStr = str.toLowerCase().split(" ");
    for (let i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }
};

module.exports = completedTransactionSelectAll;
