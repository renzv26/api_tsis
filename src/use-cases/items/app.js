const {items} = require("../../data-access/sl-layer/items/app");

const selectAllItem = require("./item-select-all");

const selectAllItemUseCase = selectAllItem({items})

const services = Object.freeze({
    selectAllItemUseCase
})

module.exports = services;
module.exports = {
    selectAllItemUseCase

}