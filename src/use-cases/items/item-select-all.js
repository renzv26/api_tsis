const selectAllItem = ({items}) => {
    return async function select(){

            const itemsDetails = await items.selectAllItems()

            return itemsDetails
        
    };
};
 module.exports = selectAllItem