const supplierDb = require("../../data-access/db-layer/suppllier/app")
const {supppliers} = require("../../data-access/sl-layer/suppliers/app")
const {users} = require("../../data-access/sl-layer/users/app")
const seedSupplier = require("./seed-supplier")

const seedSupplierUseCase = seedSupplier({supplierDb,supppliers,users})

const services = Object.freeze({
    seedSupplierUseCase
})

module.exports = services;
module.exports = {
    seedSupplierUseCase
}