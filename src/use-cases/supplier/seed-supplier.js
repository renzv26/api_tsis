const seedSupplier = ({ supplierDb, supppliers ,users}) => {
    return async function seed (info){
        const get = await supplierDb.selectSupplier()

        const DBlogin = await users.userLogin()

        const cookie = DBlogin.SessionId

        // console.log(get.rows)
        let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;
        // Get ID

        const maxValue = await supppliers.supplierGetMaxCode();
        
        // Loop through all assets
        for (let i = 0; i < get.rows.length; i++) {
            const asset = get.rows[i];

            batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_BFI_TS_SUPPLIER

{
    "Code": "${maxValue[0].maxCode  + i}",
    "Name": "${maxValue[0].maxCode  + i}",
    "U_Name": "${asset.name}",
    "U_Address": "${asset.address}"`;

            // Convert into string for batch request
            for (let j = 0; j < Object.keys(asset).length; j++) {
                const key = Object.keys(asset)[j];

                const type = typeof asset[key]
            }

            batchStr += `
}
`
        }

        batchStr += `
--b--
--a--`;


// console.log(batchStr);
        const batchData = {
            batchStr,
            cookie,
          };
        const batchPost = await supppliers.batchRequest(batchData);

        return batchPost

    }
}
module.exports = seedSupplier