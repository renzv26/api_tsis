const weightTypeSelectAll = ({ weightDb, weightTypes }) => {
  return async function selects(info) {

    const mode =info.mode

    if(mode == 1){

      delete info.source;
      delete info.mode;

      const res = await weightTypes.weightTypesSelectAll({info});

      return res

    }else {

    const result = await weightDb.selectAllWeightType();
    const weightType = result.rows;

    return weightType;
    }
  };
};

module.exports = weightTypeSelectAll;
