const updateWeightTypes = ({ weightDb, makeWeight, insertActivityLogss }) => {
  return async function put({ id, ...info } = {}) {
    const result = makeWeight(info);

    const weightExist = await weightDb.selectByNameUpdate({
      weight_name: result.getWeightName(),
      id
    });

    if (weightExist.rowCount !== 0) {
      throw new Error("The weight name already exist.");
    }

    // query previous values
    const previous = await weightDb.selectOneWeightType({
      id
    });
    const prev_data = previous.rows[0];
    const prev_values = {
      id: prev_data.id,
      weight_name: prev_data.weight_name,
      created_at: prev_data.created_at,
      updated_at: prev_data.updated_at
    };

    // update to db
    const update = await weightDb.updateWeightType({
      weight_name: result.getWeightName(),
      updated_at: result.getUpdatedAt(),
      id
    });

    const count = update.rowCount; // get the number of inserted data

    const data = {
      msg: `Updated successfully ${count} weight type.`
    };

    // new values
    const weight = {
      weight_name: result.getWeightName(),
      updated_at: result.getUpdatedAt()
    };

    // logs object
    const logs = {
      action_type: "UPDATE WEIGHT TYPE",
      table_affected: "ts_weight_types",
      new_values: weight,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = updateWeightTypes;
