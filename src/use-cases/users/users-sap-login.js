const loginSapUser = ({ users }) => {
  return async function posts(info) {
    // #########
    // determine mode; offline or online
    // 1 is online; 0 is offline
    if (!info.mode) {
      throw new Error(`Please enter mode of access.`);
    }

    const mode = info.mode;
    delete info.mode;
    delete info.cookie;
    delete info.source;

    // online mode so connect so SAP;
    if (mode == 1) {
      // validate info
      val({ info });

      // login
      const res = await users.userSoftLogin({ info });
      
      // console.log(res);
      if (res.SessionId) {
        const session = res.SessionId;

        return session;
      } else {
        throw new Error(`Invalid account, please try again.`);
      }
    } else {
      const d = {
        status: 403,
        msg: "Forbidden"
      };
      throw new Error(JSON.stringify(d));
    }
  };
};

const val = ({ info }) => {
  const { username, password } = info;

  if (!username) {
    const d = {
      msg: "Please enter username."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!password) {
    const d = {
      msg: "Please enter password."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = loginSapUser;
