const usersSelectAll = ({ usersDb, decrypt, users, validateAccessRights }) => {
  return async function selects(info) {
    const mode = info.mode;
    
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "view users"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const res = await users.usersSelectAll({ info });
      // const res1 = await users.usersSelectAllRevive({ info });
      // const AllUsers = res.concat(res1);

      return res;
    } else {
      const result = await usersDb.selectAllUsers({});
      const users = result.rows;

      let data = []; //declare empty array
      for await (let d of users) {
        data.push({
          id: d.id,
          employee_id: decrypt(d.employee_id),
          email: d.email ? decrypt(d.email) : d.email,
          fullname: decrypt(d.lastname) + ", " + decrypt(d.firstname),
          contact_number: d.contact_number
            ? decrypt(d.contact_number)
            : d.contact_number,
          role_name: d.name,
          role_status: d.status,
        });
      }

      return data;
    }
  };
};

module.exports = usersSelectAll;
