const dotenv = require("dotenv");
const { dbs } = require("../../data-access/db-layer/app");
const { users } = require("../../data-access/sl-layer/users/app");
const { objectLowerCaser } = require("../../lowerCaser/app");
const moment = require("moment-timezone");
// ##########

const {
  makeUser,
  userLoginEntitys,
  e_updateUser,
} = require("../../entities/users/app"); // entity
const usersDb = require("../../data-access/db-layer/users/app"); //db
const { decrypt, encrypt } = require("../../../crypting/app");
const { makeTokens } = require("../../token/app");
const { returnIpAddress, hostName } = require("../../entities/truckscale/app"); // entity

const { validateAccessRights } = require("../../validator/app"); //validator

const { accessRightsSelectAllOnRoles } = require("../access_rights/app"); // query for access rights during login
//####################

const addNewUser = require("./users-add");
const addNewSapUser = require("./users-add-sap")
const usersSelectAll = require("./users-select-all");
const usersSelectOne = require("./users-select-one");
const loginUser = require("./users-login");
const updateUser = require("./users-update");
const insertActivityLogs = require("./activity-logs-insert");
const resetPassword = require("./users-reset-password");
const selectActivityLogs = require("./activity-logs-select");
const employeesInfoSelectAll = require("./employeesInfo-select-all");
const loginSapUser = require("./users-sap-login");
const checkPin = require("./check-pin");
const resetCodeAndPw = require("./reset-password-code");

//####################
const insertActivityLogss = insertActivityLogs({ usersDb }); // function to insert into activity logs
const resetCodeAndPws = resetCodeAndPw({ users });
const loginSapUsers = loginSapUser({ users });

const uc_checkPin = checkPin({ users });

const resetPasswords = resetPassword({
  usersDb,
  encrypt,
  insertActivityLogss,
  users,
});

const uc_employeesInfoSelectAll = employeesInfoSelectAll({
  users,
  validateAccessRights,
});

const addNewUsers = addNewUser({
  usersDb,
  makeUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
});

const addNewSapUsers = addNewSapUser({
  usersDb,
  makeUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
});

const usersSelectAlls = usersSelectAll({
  usersDb,
  decrypt,
  users,
  validateAccessRights,
});
const usersSelectOnes = usersSelectOne({
  usersDb,
  decrypt,
  users,
  validateAccessRights,
});
const loginUsers = loginUser({
  usersDb,
  makeTokens,
  accessRightsSelectAllOnRoles,
  userLoginEntitys,
  returnIpAddress,
  hostName,
  decrypt,
  insertActivityLogss,
  users,
  moment,
});
const uc_updateUser = updateUser({
  usersDb,
  e_updateUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
});
const selectActivityLogss = selectActivityLogs({ usersDb, decrypt, users });

// ##############
const services = Object.freeze({
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  insertActivityLogss,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws,
  addNewSapUsers
});

module.exports = services;
module.exports = {
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  insertActivityLogss,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws,
  addNewSapUsers
};