const loginUser = ({
  usersDb,
  makeTokens,
  accessRightsSelectAllOnRoles,
  userLoginEntitys,
  returnIpAddress,
  hostName,
  decrypt,
  insertActivityLogss,
  users,
  moment,
}) => {
  return async function posts(info) {
    // #########
    // determine mode; offline or online
    // 1 is online; 0 is offline
    if (!info.mode) {
      throw new Error(`Please enter mode of access.`);
    }
    const mode = info.mode;

    // online mode so connect so SAP; make an env file to determine
    if (mode == 1) {
      // login to SAP; Service layer account
      const res = await users.userLogin();
      let sessiodId;
      if (res) {
        sessiodId = res.SessionId;
      } else {
        throw new Error("Could not login this time, please try again.");
      }

      // validate fields
      val({ info });

      // add session id to object
      info.cookie = sessiodId;
      // select user account from UDT
      const check = await users.userSelectCredentials({ info });
      // correct credentials; account exist
      if (check.status == 200) {
        // user doesn't exist
        if (check.data.value.length == 0) {
          throw new Error("Invalid account, please try again.");
        }
        // all user data
        const userData = check.data.value[0];
        const userId = userData.U_BFI_TS_USERS.Code;
        let loginCount = userData.U_BFI_TS_USERS.U_TS_LOGIN_COUNT;
        const userRole = userData.U_BFI_TS_ROLE;
        const userStatus = userData.U_BFI_TS_USERS.U_TS_STATUS;
        const userIdNumber = userData.U_BFI_TS_USERS.U_TS_EMP_ID

        if (userStatus.toLocaleLowerCase() == "inactive")
          throw new Error(`Account is inactive, please contact admin`);

        const userDetails = await users.usersSelectInEmployeesInfo({
          info: { id: userData.U_BFI_TS_USERS.U_APP_EMP_ID },
        });

        const name = `${userDetails[0] ? userDetails[0].firstName : "Ano"} ${
          userDetails[0] ? userDetails[0].lastName : "nymous"
        }`;

        // has SAP account or not; 1 for true, 0 for false
        const isDirectUser = userData.U_BFI_TS_USERS.U_TS_IS_DIRECTUSER;
        // check if role is active or not
        if (userRole.U_TS_STATUS.toLowerCase() !== "active") {
          throw new Error("Status is inactive, please contact administrator.");
        }

        // data use to get modules of user
        const userInfo = {
          id: userRole.Code,
          mode,
          cookie: `B1SESSION=${sessiodId}`,
        };

        // get modules of the user base on role id
        const modules = await accessRightsSelectAllOnRoles(userInfo);

        // query which truckscale the user logged in; for local storage
        // use for inbound and outbound transaction
        const ip = await returnIpAddress();
        const device_name = await hostName();

        const tsInfo = {
          ip,
          device_name,
          cookie: `B1SESSION=${sessiodId}`,
        };

        const ts = await users.userSelectTruckScale({ info: tsInfo });

        // console.log(ts, "TS TEST")
        let ts_id = null; //store truckscale id
        let ts_location = null;
        let ts_num = null;
        if (ts.length > 0) {
          // if truckscale exists
          ts_id = ts[0].Code;
          ts_num = ts[0].U_TS_ID;
          ts_location = ts[0].U_LOCATIONS;
        }

        const truckscale = {
          id: ts_id,
          number: ts_num,
          location: ts_location,
        };

        const countInfo = {
          id: userId,
          cookie: `B1SESSION=${sessiodId}`,
        };

        const count = await users.userGetLoginCount({ info: countInfo });
        let currentCount = count.data.value[0].U_TS_LOGIN_COUNT;

        // const d = new Date().toDateString();
        // const date = moment(d).format("YYYY-MM-DD");
        const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
        // const t = new Date().toTimeString();
        const t = moment().tz('Asia/Manila').format('HH:mm:ss')
        // console.log('=---=',t);
        const time = await fixTime(t);

        let lc = 0;
        if (loginCount) {
          lc = currentCount + 1;

          const data = {
            id: userId,
            cookie: "B1SESSION=" + sessiodId,
            U_TS_LOGIN_COUNT: lc,
            U_TS_UPDATEDATE: date,
            U_TS_UPDATETIME: time,
            U_UPDATED_BY: userId,
          };

          await users.usersUpdate({ info: data });
        }
        const data = {
          userId,
          isDirectUser,
          loginCount: lc,
          userRole,
          msg: `Connected to Service Layer DB : ${process.env.DB}`,
          sessiodId,
          modules,
          truckscale,
          name,
          userIdNumber
        };
        return data;
      } else {
        throw new Error("Invalid account, please try again.");
      }
    } else {
      // offline mode
      const result = userLoginEntitys(info); // entity

      const employee_id = result.getId();
      const password = result.getPassword();

      // select if id and password exist
      const check = await usersDb.selectCredentials({
        employee_id,
        password,
      });

      const count = check.rowCount; // count if it exist

      // greater than 0 means it exist; else error.
      if (count > 0) {
        const role_id = check.rows[0].role_id; //get role id to query the access rights; include in return

        // check if role is still active
        const role = await usersDb.checkRoleStatus({
          role_id,
        });

        const roleStatus = role.rows[0].status;

        if (roleStatus.toLowerCase() !== "active".toLocaleLowerCase()) {
          throw new Error("Status is inactive, please contact administrator.");
        }

        const token = await makeTokens({
          employee_id,
          password,
        });

        // previous values
        const previous = await usersDb.getDataOfUserDuringLogin({
          employee_id,
          password,
        });
        const prev_data = previous.rows[0];
        const prev_values = {
          employee_id: decrypt(prev_data.employee_id),
          email: prev_data.email ? decrypt(prev_data.email) : "",
          firstname: decrypt(prev_data.firstname),
          middlename: prev_data.middlename ? decrypt(prev_data.middlename) : "",
          lastname: decrypt(prev_data.lastname),
          name_extension: prev_data.name_extension
            ? decrypt(prev_data.name_extension)
            : "",
          password: decrypt(prev_data.password),
          employee_image: prev_data.employee_image,
          employee_signature: prev_data.employee_signature,
          contact_number: prev_data.contact_number
            ? decrypt(prev_data.contact_number)
            : "",
          token: prev_data.token,
          role: {
            id: prev_data.role_id,
            name: prev_data.name,
            status: prev_data.status,
          },
        };

        // update db
        await usersDb.userLogin({
          token,
          employee_id,
          password,
        });

        const userInfo = {
          id: role_id,
          mode,
        };

        // query access rights of the user
        const modules = await accessRightsSelectAllOnRoles(userInfo);

        // query which truckscale the user logged in; for local storage
        // use for inbound and outbound transaction whichTruckScaleLoggedIn
        const ip = await returnIpAddress();
        const device_name = await hostName();

        const ts = await usersDb.whichTruckScaleLoggedIn({ ip, device_name });
        const truckscale = ts.rows;

        const data = {
          id: check.rows[0].id,
          role: check.rows[0].role_name,
          count: check.rows[0].login_count,
          msg: "Login successfully.",
          db: `DB layer -> connected to ${process.env.PGDATABASE}.`,
          token,
          modules,
          truckscale,
        };

        const new_values = {
          employee_id: decrypt(prev_data.employee_id),
          email: prev_data.email ? decrypt(prev_data.email) : "",
          firstname: decrypt(prev_data.firstname),
          middlename: prev_data.middlename ? decrypt(prev_data.middlename) : "",
          lastname: decrypt(prev_data.lastname),
          name_extension: prev_data.name_extension
            ? decrypt(prev_data.name_extension)
            : "",
          password: decrypt(prev_data.password),
          employee_image: prev_data.employee_image,
          employee_signature: prev_data.employee_signature,
          contact_number: prev_data.contact_number
            ? decrypt(prev_data.contact_number)
            : "",
          token,
          role: {
            id: prev_data.role_id,
            name: prev_data.name,
            status: prev_data.status,
          },
        };

        // logs object
        const logs = {
          action_type: "LOGIN USER",
          table_affected: "ts_users",
          new_values,
          prev_values,
          created_at: null,
          updated_at: new Date().toISOString(),
          users_id: check.rows[0].id,
        };

        await insertActivityLogss({ logs });

        return data;
      } else {
        throw new Error("Invalid account, please try again.");
      }
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

const delay = (s) => {
  return new Promise((resolve) => setTimeout(resolve, s));
};

const val = ({ info }) => {
  const { employee_id, password } = info;

  if (!employee_id) {
    const d = {
      msg: "Please enter Employee ID.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!password) {
    const d = {
      msg: "Please enter password",
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = loginUser;
