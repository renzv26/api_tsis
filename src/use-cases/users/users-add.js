const addNewUser = ({
  usersDb,
  makeUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "add user"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const max = await users.usersGetMaxCode({});
      const maxCode = max[0].maxCode;

      info = await objectLowerCaser(info);

      info.Code = maxCode;
      info.Name = maxCode;

      val({ info }); // temporary validation
      if (info.U_TS_IS_BIOTECH == 1) {
        const employeeDetails = await users.usersSelectOneInEmployeesInfo({
          info: {
            id: info.U_APP_EMP_ID,
            cookie: info.cookie,
          },
        });
        info.U_TS_IS_DIRECTUSER = employeeDetails[0].userId ? 1 : 0;
      }

      if (info.U_TS_IS_REVIVE == 1) {
        const employeeDetails = await users.usersSelectOneInEmployeesInfoRevive({
          info: {
            id: info.U_APP_EMP_ID,
            cookie: info.cookie,
          },
        });
        info.U_TS_IS_DIRECTUSER = employeeDetails[0].userId ? 1 : 0;
      }

      info.U_TS_PW = info.U_TS_EMP_ID;

      const check = await users.usersAddSelectById({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Employee account already exists.`);
      }

      const check2 = await users.usersAddSelectByEmpId({ info });

      if (check2.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      const length2 = check2.data.length;

      if (length2 > 0) {
        throw new Error(`Employee ID already exists.`);
      }

      info.U_TS_STATUS = "active";
      info.U_TS_PIN = 1234;

      const res = await users.usersAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }
    } else {
      // offline mode
      const result = makeUser(info);
      // check employee id
      const idExist = await usersDb.selectUserId({
        employee_id: result.getId(),
      });

      if (idExist.rowCount !== 0) {
        throw new Error("The employee's ID already exist.");
      }

      // check full name
      const nameExist = await usersDb.selectUserFullName({
        firstname: result.getFirstName(),
        middlename: result.getLastName(),
      });

      if (nameExist.rowCount !== 0) {
        throw new Error("The employee already exist.");
      }

      // insert to db
      const insert = await usersDb.insertUser({
        employee_id: result.getId(),
        email: result.getEmail(),
        firstname: result.getFirstName(),
        middlename: result.getMiddleName(),
        lastname: result.getLastName(),
        name_extension: result.getNameExtension(),
        password: result.getPassword(),
        role_id: result.getRole(),
        employee_image: result.getImage(),
        employee_signature: result.getSignature(),
        contact_number: result.getContactNumber(),
        created_at: result.getCreatedAt(),
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} user.`,
      };

      // query the role of user for logs
      const ts_role = await usersDb.getRoleOfUser({ id: result.getRole() });
      const role = ts_role.rows[0];

      // new values
      const employee = {
        employee_id: decrypt(result.getId()),
        email: result.getEmail() ? decrypt(result.getEmail()) : "",
        firstname: decrypt(result.getFirstName()),
        middlename: result.getMiddleName()
          ? decrypt(result.getMiddleName())
          : "",
        lastname: decrypt(result.getLastName()),
        name_extension: result.getNameExtension()
          ? decrypt(result.getNameExtension())
          : "",
        password: decrypt(result.getPassword()),
        employee_image: result.getImage(),
        employee_signature: result.getSignature(),
        contact_number: result.getContactNumber()
          ? decrypt(result.getContactNumber())
          : "",
        created_at: result.getCreatedAt(),
        role,
      };

      const logs = {
        action_type: "CREATE USER",
        table_affected: "ts_users",
        new_values: employee,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id,
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_APP_EMP_ID, U_TS_EMP_ID, U_TS_ROLE_ID, U_EMAIL } = info;
  if (!U_APP_EMP_ID) {
    const d = {
      msg: "Please select Employee.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_EMP_ID) {
    const d = {
      msg: "Please enter Employee ID.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_ROLE_ID) {
    const d = {
      msg: "Please select role.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_EMAIL) {
    const d = {
      msg: "Please enter email.",
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewUser;
