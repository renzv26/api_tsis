const addNewSapUser = ({
    usersDb,
    makeUser,
    insertActivityLogss,
    decrypt,
    users,
    objectLowerCaser,
    validateAccessRights,
  }) => {
    return async function posts(info) {
      const mode = info.mode;
      // console.log('TEST', info);
      if (!info.modules) {
        throw new Error(`Access denied`);
      }
  
      const allowed = await validateAccessRights(
        info.modules,
        "admin",
        "add user"
      );
  
      if (!allowed) {
        throw new Error(`Access denied`);
      }
  
      if (mode == 1) {
        delete info.modules;
  
        delete info.source;
        delete info.mode;
  
        const max = await users.usersGetMaxCode({});
        const maxCode = max[0].maxCode;
  
        // info = await objectLowerCaser(info);
        let login;
        val({ info }); // temporary validation
      if(info.Company == "biotech") {      
         login = await users.dbLogin({
          info: {
            db: process.env.sapDatabase,
            username: process.env.BIOTECH_USER,
            password: process.env.BIOTECH_PASSWORD
          }
        });
      }

        if(info.Company == "revive"){        
         login = await users.dbLogin({
          info: {
            db: process.env.reviveDB,
            username: process.env.BIOTECH_USER,
            password: process.env.BIOTECH_PASSWORD
          }
        });
      }
      const companyCookie = `B1SESSION=${login.SessionId}`

      const checkIDnumber = await users.getSapUserCode({ info })

      if(checkIDnumber.length > 0){
        throw new Error("ID Number Already Exists on SAP.")
      }

      const checkFullName = await users.getSapUserFullName({ info })
      
      if(checkFullName.length > 0){
        throw new Error("User Already Exists.")
      }

      const addUserCode = await users.userAddUserCode({
        info: {
          usercode: info.UserCode,
          cookie: companyCookie
        }
      })
      if(addUserCode.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      if(addUserCode.status == 400) {
        // session expired
        throw new Error("Something is wrong. Contact admin.");
      }

      const addEmployeeInfo = await users.userAddUserSap({
        info: {
          ExternalEmployeeNumber: info.ExternalEmployeeNumber,
          FirstName: convertCase(info.FirstName),
          LastName: convertCase(info.LastName),
          cookie: companyCookie
        }
      })

      if(addEmployeeInfo.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      const reqStatus = addEmployeeInfo.status;
      if (reqStatus == 201) {
        return addEmployeeInfo;
      } else {
        // throw error from SL
        throw new Error(addEmployeeInfo.data.error.message.value);
      }
      } else {
        // offline mode
      }
    };
  };

  const val = ({ info }) => {
    const { Company, ExternalEmployeeNumber, FirstName, LastName } = info;
    if (!Company) {
      const d = {
        msg: "Please select Company.",
      };
      throw new Error(JSON.stringify(d));
    }
    if (!ExternalEmployeeNumber) {
      const d = {
        msg: "Please enter Employee ID.",
      };
      throw new Error(JSON.stringify(d));
    }
    if (!FirstName) {
      const d = {
        msg: "Please enter First Name.",
      };
      throw new Error(JSON.stringify(d));
    }
    if (!LastName) {
      const d = {
        msg: "Please enter Last Name.",
      };
      throw new Error(JSON.stringify(d));
    }
  };

  const convertCase = (str) => {
    if (str) {
      const splitStr = str.toLowerCase().split(" ");
      for (let i = 0; i < splitStr.length; i++) {
        splitStr[i] =
          splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
      }
      return splitStr.join(" ");
    }
  };

  module.exports = addNewSapUser;