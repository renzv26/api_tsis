const selectActivityLogs = ({ usersDb, decrypt, users }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      // online
      delete info.mode;
      delete info.source;

      const res = await users.selectLogs({ info });
      const data = []; // to push
      for (let i = 0; i < res.length; i++) {
        const e = res[i];

        data.push({
          id: e.Code,
          table: e.U_TS_TABLES_AFFECT,
          action: e.U_TS_ACTION,
          user_id: e.U_TS_USERS_ID ? e.U_TS_USERS_ID : null,
          created_at: e.U_TS_DATELOG,
          new_val: e.U_TS_NEW_VAL ? e.U_TS_NEW_VAL.toString() : null,
          prev_val: e.U_TS_PREV_VAL ? e.U_TS_PREV_VAL.toString() : null,
        });
      }

      return data;
    } else {
      // offline
      const result = await usersDb.selectAllActivityLogs(dateRange);
      const logs = result.rows;

      let data = []; //declare empty array
      for await (let d of logs) {
        const users = {
          id: d.users_id ? d.users_id : "",
          firstname: d.firstname ? decrypt(d.firstname) : "",
          lastname: d.lastname ? decrypt(d.lastname) : "",
        };

        await data.push({
          id: d.id,
          users,
          action_type: d.action_type,
          table_affected: d.table_affected,
          new_values: d.new_values,
          prev_values: d.prev_values,
          created_at: d.created_at,
          updated_at: d.updated_at,
        });
      }

      return data;
    }
  };
};

module.exports = selectActivityLogs;
