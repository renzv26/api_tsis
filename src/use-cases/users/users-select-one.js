const usersSelectOne = ({ usersDb, decrypt, users, validateAccessRights }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "view users"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const res = await users.usersSelectOne({ info });

      return res;
    } else {
      const result = await usersDb.selectSingleUser({ id });
      const users = result.rows;

      let data = []; //declare empty array here

      for await (let d of users) {
        data.push({
          id: d.id,
          employee_id: decrypt(d.employee_id),
          email: d.email ? decrypt(d.email) : email,
          firstname: decrypt(d.firstname),
          middlename: d.middlename ? decrypt(d.middlename) : d.middlename,
          lastname: decrypt(d.lastname),
          name_extension: d.name_extension
            ? decrypt(d.name_extension)
            : d.name_extension,
          contact_number: d.contact_number
            ? decrypt(d.contact_number)
            : d.contact_number,
          employee_image: d.employee_image,
          employee_signature: d.employee_signature,
          login_count: d.login_count,
          role_id: d.role_id,
          role_name: d.name,
          role_status: d.status,
        });
      }

      return data;
    }
  };
};

module.exports = usersSelectOne;
