const addNewAction = ({
  actionsDb,
  makeActions,
  insertActivityLogss,
  decrypt,
  actions,
  objectLowerCaser,
  validateAccessRights,
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "add action"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const max = await actions.actionsGetMaxCode({});

      const maxCode = max[0].maxCode;

      info = await objectLowerCaser(info);

      val({ info });

      const check = await actions.actionsAddSelectByName({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      const length = check.length;

      if (length > 0) {
        throw new Error(`Action name already exists.`);
      }

      info.Code = maxCode;
      info.Name = maxCode;
      
      const res = await actions.actionsAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }
    } else {
      const result = makeActions(info);

      const actionExist = await actionsDb.selectActionName({
        module_id: result.getModuleId(),
        description: result.getDesc(),
      });

      if (actionExist.rowCount !== 0) {
        throw new Error("The action name already exist.");
      }

      // insert to db
      const insert = await actionsDb.insertNewAction({
        module_id: result.getModuleId(),
        description: result.getDesc(),
        status: result.getStatus(),
        created_by: result.getCreatedBy(),
        created_at: result.getCreatedAt(),
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} action.`,
      };

      // logs
      // users
      const user = await actionsDb.returnCreatedBy({
        id: result.getCreatedBy(),
      });
      const create = user.rows[0];
      const created_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : "",
      };

      // modules
      const modules = await actionsDb.returnModule({
        id: result.getModuleId(),
      });
      const moduless = modules.rows[0];
      const module = {
        id: moduless.id,
        descriptions: moduless.descriptions,
        status: moduless.status,
      };

      // logs
      // new values
      const new_values = {
        description: result.getDesc(),
        status: result.getStatus(),
        created_at: result.getCreatedAt(),
        module,
        created_by,
      };

      const logs = {
        action_type: "CREATE ACTION",
        table_affected: "ts_actions",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: result.getCreatedBy(),
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_MOD_ID, U_TS_DESC, U_TS_STATUS, U_TS_CREATEDBY } = info;

  if (!U_TS_MOD_ID) {
    const d = {
      msg: "Please enter module of the action.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_DESC) {
    const d = {
      msg: "Please enter action name.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_CREATEDBY) {
    const d = {
      msg: "Please enter who created the action.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status.",
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewAction;
