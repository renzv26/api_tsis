const addNewRoles = ({
  rolesDb,
  makeRoles,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  roles,
  objectLowerCaser,
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "add role"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      // get max code; for auto increment
      const max = await roles.rolesGetMaxCode({});
      const maxCode = max[0].maxCode;

      info = await objectLowerCaser(info);

      val({ info });

      // select if module name exist
      const check = await roles.rolesAddSelectByName({ info });
      // number of data
      const length = check.length;
      // if exist
      if (length > 0) {
        throw new Error(`Role name already exists.`);
      }

      info.Code = maxCode;
      info.Name = maxCode;
      
      const res = await roles.rolesAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }
    } else {
      const result = makeRoles(info);

      const roleExist = await rolesDb.selectByRoleName({
        name: result.getRoleName(),
      });

      if (roleExist.rowCount !== 0) {
        throw new Error("The role name already exist.");
      }

      // insert to db
      const insert = await rolesDb.insertRoles({
        name: result.getRoleName(),
        status: result.getRoleStatus(),
        created_by: result.getCreatedBy(),
        created_at: result.getCreatedAt(),
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} role.`,
      };

      const user = await rolesDb.returnCreatedBy({ id: result.getCreatedBy() });
      const create = user.rows[0];
      const created_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : "",
      };

      // logs
      // new values
      const new_values = {
        name: result.getRoleName(),
        status: result.getRoleStatus(),
        created_at: result.getCreatedAt(),
        created_by,
      };

      const logs = {
        action_type: "CREATE ROLE",
        table_affected: "ts_roles",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: result.getCreatedBy(),
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_NAME, U_TS_STATUS, U_TS_CREATEDBY } = info;

  if (!U_TS_NAME) {
    const d = {
      msg: "Please enter role name.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_CREATEDBY) {
    const d = {
      msg: "Please enter who created the role.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status.",
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewRoles;
