const moment = require("moment-timezone");

const { makeRoles, makeRolesUpdate } = require("../../entities/roles/app"); // entity
const rolesDb = require("../../data-access/db-layer/roles/app"); //db
const { decrypt } = require("../../../crypting/app"); //decrypt
const { validateAccessRights } = require("../../validator/app"); //validator
const { insertActivityLogss } = require("../users/app"); // logs
const { roles } = require("../../data-access/sl-layer/roles/app");

const { objectLowerCaser } = require("../../lowerCaser/app");

const {
  accessRights
} = require("../../data-access/sl-layer/access-rights/app"); // SL

const { addAccessRightss } = require("../access_rights/app");

//####################

const addNewRoles = require("./roles-add");
const updateRoles = require("./roles-update");
const rolesSelectAll = require("./roles-select-all");
const rolesSelectOne = require("./roles-select-one");
//####################
const addNewRoless = addNewRoles({
  rolesDb,
  makeRoles,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  roles,
  objectLowerCaser
});
const updateRoless = updateRoles({
  rolesDb,
  makeRolesUpdate,
  validateAccessRights,
  addAccessRightss,
  insertActivityLogss,
  decrypt,
  roles,
  objectLowerCaser,
  accessRights,
  moment
});
const rolesSelectAlls = rolesSelectAll({
  rolesDb,
  decrypt,
  validateAccessRights,
  roles
});
const rolesSelectOnes = rolesSelectOne({
  rolesDb,
  decrypt,
  validateAccessRights,
  roles
});

const services = Object.freeze({
  addNewRoless,
  updateRoless,
  rolesSelectAlls,
  rolesSelectOnes
});

module.exports = services;
module.exports = {
  addNewRoless,
  updateRoless,
  rolesSelectAlls,
  rolesSelectOnes
};
