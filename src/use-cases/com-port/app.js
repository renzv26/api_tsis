const readData = require("../../entities/com-port/app"); // entity
const SerialPort = require("serialport");
const Readline = SerialPort.parsers.Readline;
const { io } = require("../../app");
const comPortDb = require("../../data-access/db-layer/com-port/app"); //db
const { ports } = require("../../data-access/sl-layer/com-port/app"); // sl query

let port; // com port config where the device will connect
let count = 0; // first time connection

//####################
const listAllPorts = require("./list-all-port");
const readSerialData = require("./read-data");
const serialComPort = require("./serial-com-port");
const closeSerialPort = require("./disconnect-port");
//####################
const serialComPorts = serialComPort({
  SerialPort,
  comPortDb,
  readData,
  port,
  ports
}); // com port declaration

const listAllPortss = listAllPorts({ SerialPort });

const readSerialDatas = readSerialData({
  serialComPorts,
  Readline,
  io,
  port,
  closeSerialPort,
  count
});

const services = Object.freeze({
  listAllPortss,
  readSerialDatas,
  serialComPorts
});

module.exports = services;
module.exports = {
  listAllPortss,
  readSerialDatas,
  serialComPorts
};
