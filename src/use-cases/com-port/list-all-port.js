const listAllPorts = ({ SerialPort }) => {
  return async function selects() {
    let portsList = [];
    let data = [];
    try {
      const port = await SerialPort.list();
      port.forEach(e => {
        data.push(e.path);
      });
      portsList.push({
        port: data
      });

      return portsList;
    } catch (e) {
      throw new Error("Error list all port: ", e);
    }
  };
};

module.exports = listAllPorts;
