const serialComPort = ({ SerialPort, comPortDb, readData, port, ports }) => {
  return async function get({ info }) {
    try {
      const { mode } = info;
      delete info.mode;
      if (mode == 1) {
        
        const { cookie } = info;
        const result = readData(info);

        const ip = result.getIp();
        const device = result.getHostName();
        
        const devices = {
          ip,
          device,
          cookie
        };
        // console.log(devices, "devices")

        const conf = await ports.selectConfigOfDevice({
          info: devices
        });
        // console.log(conf, "conf")

        if (conf.status == 200) {
          const config = conf.data[0];

          // com port to connect
          const comPort = result.getCOMPort();

          const options = {
            baudRate: config.U_TS_BAUDRATE,
            parity: config.U_TS_PARITY,
            dataBits: config.U_TS_DATABITS,
            stopBits: config.U_TS_STOPBITS
          };

          const port = new SerialPort(comPort, options, async err => {
            try {
              if (err) {
                return console.log("Error on connecting: ", err);
              }
              console.log(`Connected successfully to ${comPort}.`);
            } catch (e) {
              throw new Error(e);
            }
          });
          return port;
        } else {
          throw new Error(`No device found..`);
        }
      } else {
        // offline mode
        const result = readData(info.info);

        const ip = result.getIp();
        const device = result.getHostName();

        // query the config from db
        const conf = await comPortDb.selectConfigOfDevice({
          ip_address: ip,
          device_name: device
        });

        const config = conf.rows[0];

        // com port to connect
        const comPort = result.getCOMPort();

        const options = {
          baudRate: config.baudrate,
          parity: config.parity,
          dataBits: config.databits,
          stopBits: config.stopbits
        };

        const port = new SerialPort(comPort, options, async err => {
          try {
            if (err) {
              return console.log("Error on connecting: ", err);
            }
            console.log(`Connected successfully to ${comPort}.`);
          } catch (e) {
            throw new Error(e);
          }
        });
        return port;
      }
    } catch (e) {
      throw new Error(e);
    }
  };
};

module.exports = serialComPort;
