const addLocation = ({ truckscale, moment }) => {
  return async function posts(info) {
    const mode = info.mode;
    delete info.mode;

    if (mode == 1) {
      // online
      const { cookie } = info;
      delete info.cookie;
      delete info.source;

      if (!info.location) throw new Error(`Please enter location.`);

      //   check if exist
      const check = await truckscale.locationCheckExist(info.location);
      if (check.length > 0) throw new Error(`Location already exist`);

      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      // console.log('=---=',t);
      const time = await fixTime(t);

      //   get max code
      const max = await truckscale.locationMaxCode({});
      const maxCode = max[0].maxCode;

      const data = {
        Code: maxCode,
        Name: maxCode,
        U_LOCATIONS: info.location,
        U_TS_CREATEDBY: info.userId,
        U_TS_MODIFYBY: info.userId,
        U_TS_CREATEDATE: date,
        U_TS_CREATETIME: time,
        U_TS_UPDATEDATE: date,
        U_TS_UPDATETIME: time,
        U_DETAILED_LOCATION: info.detail,
        U_TS_STATUS: info.status,
        cookie,
      };

      const res = await truckscale.locationAdd({ info: data });
      return res;
    } else {
      // offline
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = addLocation;
