const locationSelect = ({ truckScaleDb, truckscale }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      if (info.isDisplay) {
        const res = await truckscale.displayLocations({});
        return res;
      }

      // default
      const res = await truckscale.getLocations({});
      return res;
    } else {
      // offline
    }
  };
};

module.exports = locationSelect;
