const tsSelectAll = ({ truckScaleDb, truckscale }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      const res = await truckscale.truckscaleSelectAll({ info });      
      return res;
    } else {
      const result = await truckScaleDb.selectAllTruckScale({});
      const ts = result.rows;

      return ts;
    }
  };
};

module.exports = tsSelectAll;
