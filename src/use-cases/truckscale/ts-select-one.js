const tsSelectOne = ({ truckScaleDb, truckscale }) => {
  return async function selects(info) {


    const mode = info.mode

    if(mode == 1){

      delete info.source;
      delete info.mode;

      const res = await truckscale.truckscaleSelectOne({info});

      return res

    }else {

    const result = await truckScaleDb.selectOneTruckScale({ id });
    const ts = result.rows;

    return ts;
    }
  };
};

module.exports = tsSelectOne;
