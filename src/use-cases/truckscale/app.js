const os = require("os");
const address = require("address");
const isIp = require("is-ip");
const moment = require("moment-timezone");

const returnIpAddress = () => {
  return address.ip();
};

// get host name of the device
const hostName = () => {
  return os.hostname();
};

// check if valid ip address
const checkIp = (ip) => {
  return isIp.v4(ip);
};
// only numbers allowed
const allNumbers = (text) => {
  const isnum = /^\d*(\.\d+)?$/.test(text);
  return isnum;
};

const {
  makeTruckScale,
  makeTruckScaleUpdate,
} = require("../../entities/truckscale/app"); // entity
const truckScaleDb = require("../../data-access/db-layer/truckscale/app"); //db

const { insertActivityLogss } = require("../users/app");

const { truckscale } = require("../../data-access/sl-layer/truckscale/app");

const { objectLowerCaser } = require("../../lowerCaser/app");

//#######################
const addNewTruckScale = require("./ts-add");
const tsSelectAll = require("./ts-select-all");
const tsSelectOne = require("./ts-select-one");
const updateTruckScales = require("./ts-update");
const locationSelect = require("./locations-select");
const addLocation = require("./locations-add");
const updateLocation = require("./locations-update");
const addPrintLoc = require("./print-location-add");
const printLocationSelect = require("./print-location-select");
const updatePrintLocation = require("./print-location-update");
//#######################
const addNewTruckScales = addNewTruckScale({
  truckScaleDb,
  makeTruckScale,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
  returnIpAddress,
  hostName,
});
const tsSelectAlls = tsSelectAll({ truckScaleDb, truckscale });
const tsSelectOnes = tsSelectOne({ truckScaleDb, truckscale });
const updateTruckScaless = updateTruckScales({
  truckScaleDb,
  makeTruckScaleUpdate,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
});
const locationSelects = locationSelect({ truckScaleDb, truckscale });
const addLocations = addLocation({ truckscale, moment });
const updateLocations = updateLocation({ truckscale, moment });
const addPrintLocs = addPrintLoc({ truckscale, moment });
const printLocationSelects = printLocationSelect({ truckScaleDb, truckscale });
const updatePrintLocations = updatePrintLocation({ truckscale, moment });
//#######################
const services = Object.freeze({
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless,
  locationSelects,
  addLocations,
  updateLocations,
  addPrintLocs,
  printLocationSelects,
  updatePrintLocations,
});

module.exports = services;
module.exports = {
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless,
  locationSelects,
  addLocations,
  updateLocations,
  addPrintLocs,
  printLocationSelects,
  updatePrintLocations,
};
