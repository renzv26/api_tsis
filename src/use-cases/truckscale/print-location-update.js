const updatePrintLocation = ({ truckscale, moment }) => {
  return async function posts(info) {
    const mode = info.mode;
    delete info.mode;

    if (mode == 1) {
      // online
      const { cookie } = info;
      delete info.cookie;
      delete info.source;

      if (!info.printLoc) throw new Error(`Please enter print location.`);
      if (!info.location_id) throw new Error(`Please enter location.`);
      if (!info.id) throw new Error(`Please enter print location to update.`);

      //   check if exist
      const check = await truckscale.printLocationCheckExistUpdate(
        info.printLoc,
        info.id
      );

      if (check.length > 0) throw new Error(`Print location already exist`);

      // const d = new Date().toDateString();
      // const date = moment(d).format("YYYY-MM-DD");
      const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");
      // const t = new Date().toTimeString();
      const t = moment().tz('Asia/Manila').format('HH:mm:ss')
      // console.log('=---=',t);
      const time = await fixTime(t);

      const data = {
        id: info.id,
        U_PRINT_LOC: info.printLoc,
        U_LOCATION_ID: info.location_id,
        U_TS_MODIFYBY: info.userId,
        U_TS_UPDATEDATE: date,
        U_TS_UPDATETIME: time,
        U_TS_STATUS: info.status,
        cookie,
      };

      const res = await truckscale.printLocationUpdate({ info: data });
      return res;
    } else {
      // offline
    }
  };
};

const fixTime = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = updatePrintLocation;
