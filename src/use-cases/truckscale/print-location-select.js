const printLocationSelect = ({ truckScaleDb, truckscale }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;
      
      // default
      const res = await truckscale.printLocationFetch({});
      return res;
    } else {
      //   offline
    }
  };
};

module.exports = printLocationSelect;
