const updateTruckScales = ({
  truckScaleDb,
  makeTruckScaleUpdate,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
}) => {
  return async function put({ id, ...info } = {}) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.mode;
      delete info.source;

      info.id = id;
      info = await objectLowerCaser(info);

      val({ info });

      const check = await truckscale.truckscaleAddSelectByIPAndDeviceName({
        info,
      });

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Device already exists.`);
      }

      const res = await truckscale.truckscaleUpdate({ info });
      return res;
    } else {
      // offline mode
      const result = makeTruckScaleUpdate(info);

      const tsExist = await truckScaleDb.selectTruckScaleNameUpdate({
        truckscale_name: result.getTSName(),
        id,
      });

      if (tsExist.rowCount !== 0) {
        throw new Error("The truck scale name already exist.");
      }

      // query previous values
      const prev = await truckScaleDb.selectOneTruckScale({ id });
      const ts = prev.rows[0];

      const prev_values = {
        truckscale_name: ts.truckscale_name,
        truckscale_location: ts.truckscale_location,
        truckscale_length: ts.truckscale_length,
        created_at: ts.created_at,
        updated_at: ts.updated_at,
        ip_address: ts.ip_address,
        device_name: ts.device_name,
        baudrate: ts.baudrate,
        parity: ts.parity,
        databits: ts.databits,
        stopbits: ts.stopbits,
      };

      // update to db
      const update = await truckScaleDb.updateTruckScale({
        truckscale_name: result.getTSName(),
        truckscale_location: result.getTSLocation(),
        truckscale_length: result.getTSLength(),
        ip_address: result.getIp(),
        device_name: result.getHostName(),
        baudrate: result.getBaudRate(),
        parity: result.getParity(),
        databits: result.getDataBits(),
        stopbits: result.getStopBits(),
        updated_at: result.getCreatedAt(),
        id,
      });

      const count = update.rowCount; // get the number of updated data

      const data = {
        msg: `Updated successfully ${count} truck scale.`,
      };

      // new values
      const new_values = {
        truckscale_name: result.getTSName(),
        truckscale_location: result.getTSLocation(),
        truckscale_length: result.getTSLength(),
        ip_address: result.getIp(),
        device_name: result.getHostName(),
        baudrate: result.getBaudRate(),
        parity: result.getParity(),
        databits: result.getDataBits(),
        stopbits: result.getStopBits(),
        updated_at: result.getCreatedAt(),
      };

      // logs object
      const logs = {
        action_type: "UPDATE TRUCKSCALE",
        table_affected: "ts_truckscales",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: info.users_id,
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const {
    U_TS_TRSCALE,
    U_LOCATION_ID,
    U_TS_TR_LENGTH,
    U_TS_BAUDRATE,
    U_TS_TR_IP,
    U_TS_DEVICE,
  } = info;

  if (!U_TS_TR_IP) {
    const d = {
      msg: "Please enter IP.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_DEVICE) {
    const d = {
      msg: "Please enter device name.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_TRSCALE) {
    const d = {
      msg: "Please enter truckscale name.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_LOCATION_ID) {
    const d = {
      msg: "Please enter truckscale location.",
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_TR_LENGTH) {
    const d = {
      msg: "Please enter truckscale length.",
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_BAUDRATE) {
    const d = {
      msg: "Please enter baudrate.",
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateTruckScales;
