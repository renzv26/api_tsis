const {
  addNewWeightTypes,
  weightTypeSelectAlls,
  weightTypeSelectOnes,
  updateWeightTypess
} = require("../../use-cases/weight-types/app");

//####################
const weightTypeAddNew = require("./weight-type-add");
const selectAllWeightType = require("./weight-type-select-all");
const selectOneWeightType = require("./weight-type-select-one");
const weightTypeUpdate = require("./weight-type-update");
//####################
const weightTypeAddNews = weightTypeAddNew({ addNewWeightTypes });
const selectAllWeightTypes = selectAllWeightType({ weightTypeSelectAlls });
const selectOneWeightTypes = selectOneWeightType({ weightTypeSelectOnes });
const weightTypeUpdates = weightTypeUpdate({ updateWeightTypess });

const services = Object.freeze({
  weightTypeAddNews,
  selectAllWeightTypes,
  selectOneWeightTypes,
  weightTypeUpdates
});

module.exports = services;
module.exports = {
  weightTypeAddNews,
  selectAllWeightTypes,
  selectOneWeightTypes,
  weightTypeUpdates
};
