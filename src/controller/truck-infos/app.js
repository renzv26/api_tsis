const {
    uc_addTruckInfo,
    uc_selectAllTruckInfos,
    uc_selectOneTruckInfo,
    uc_updateTruckInfo
  } = require("../../use-cases/truck-infos/app");
  

  const addTruckInfo = require("./truck-infos-add");
  const selectAllTruckInfos = require("./truck-infos-select-all")
  const selectOneTruckInfo = require("./truck-infos-select-one")
  const updateTruckInfo = require("./truck-infos-update")
  
  const c_addTruckInfo = addTruckInfo({ uc_addTruckInfo });
  const c_selectAllTruckInfos = selectAllTruckInfos({ uc_selectAllTruckInfos })
  const c_selectOneTruckInfo = selectOneTruckInfo({ uc_selectOneTruckInfo })
  const c_updateTruckInfo = updateTruckInfo({ uc_updateTruckInfo })
  
  const services = Object.freeze({
    c_addTruckInfo,
    c_selectAllTruckInfos,
    c_selectOneTruckInfo,
    c_updateTruckInfo
  });
  
  module.exports = services;
  module.exports = {
    c_addTruckInfo,
    c_selectAllTruckInfos,
    c_selectOneTruckInfo,
    c_updateTruckInfo
  };
  