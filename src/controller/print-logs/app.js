const {
  insertPrintLogss,
  printCounts,
  printLogsUpdates,
  printLogsSelects,
} = require("../../use-cases/print-logs/app");

// ###########################
const printLogsInsert = require("./insert-print-logs");
const countPrint = require("./print-count");
const updatePrintLog = require("./print-logs-update");
const selectPrintLog = require("./print-logs-select");
// ###########################
const printLogsInserts = printLogsInsert({ insertPrintLogss });
const countPrints = countPrint({ printCounts });
const updatePrintLogs = updatePrintLog({ printLogsUpdates });
const selectPrintLogs = selectPrintLog({ printLogsSelects });
// ###########################
const services = Object.freeze({
  printLogsInserts,
  countPrints,
  updatePrintLogs,
  selectPrintLogs,
});

module.exports = services;
module.exports = {
  printLogsInserts,
  countPrints,
  updatePrintLogs,
  selectPrintLogs,
};
