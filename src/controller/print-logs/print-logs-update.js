const updatePrintLog = ({ printLogsUpdates }) => {
  return async function put(httpRequest) {
    try {
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      info.cookie = httpRequest.headers["Cookie"]; // add cookie to req body
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const toEdit = {
        ...info,
        source,
        id: httpRequest.params.id,
      };
      const patched = await printLogsUpdates(toEdit);
      return {
        headers: {
          "Content-Type": "application/json",
        },
        statusCode: 200,
        body: { patched },
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};

module.exports = updatePrintLog;
