
const {
    selectAllItemUseCase
} = require ("../../use-cases/items/app")

const selectAllItems = require ("./item-select-all");

const selectAllItemsController = selectAllItems({selectAllItemUseCase})

const services = Object.freeze({
    selectAllItemsController
})

module.exports = services
module.exports = {
    selectAllItemsController
}