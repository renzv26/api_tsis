
const {
    seedSupplierUseCase
} = require("../../use-cases/supplier/app")

const seedSupplier = require("./seed-supplier")

const seedSupplierController = seedSupplier({seedSupplierUseCase});

const services = Object.freeze({
    seedSupplierController
})

module.exports = services
module.exports = {
    seedSupplierController
}

