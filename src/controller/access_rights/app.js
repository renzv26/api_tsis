const {
  addAccessRightss,
  accessRightsSelectAllOnRoles
} = require("../../use-cases/access_rights/app");

//####################
const accessRightsAddNew = require("./access-rights-add");
const selectOnRolesAccessRights = require("./access-rights-select-on-role");

//####################
const accessRightsAddNews = accessRightsAddNew({ addAccessRightss });
const selectOnRolesAccessRightss = selectOnRolesAccessRights({
  accessRightsSelectAllOnRoles
});

const services = Object.freeze({
  accessRightsAddNews,
  selectOnRolesAccessRightss
});

module.exports = services;
module.exports = {
  accessRightsAddNews,
  selectOnRolesAccessRightss
};
