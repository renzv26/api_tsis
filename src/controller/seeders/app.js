const {
    uc_seedDefault
  } = require("../../use-cases/seeders/app");
  
  
  const seedDefault = require("./seed-default");
 
  const c_seedDefault = seedDefault({ uc_seedDefault });
  
  const services = Object.freeze({
    c_seedDefault,
  });
  
  module.exports = services;
  module.exports = {
    c_seedDefault
  };
  