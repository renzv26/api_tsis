const transactionsToUpdate = ({ toUpdateTransactions }) => {
  return async function put(httpRequest) {
    try {
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      info.cookie = httpRequest.headers["Cookie"]; // add cookie to req body
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const toEdit = {
        ...info,
        source,
      };
      const patched = await toUpdateTransactions(toEdit);
      return {
        headers: {
          "Content-Type": "application/json",
        },
        statusCode: 200,
        body: { patched },
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};

module.exports = transactionsToUpdate;
