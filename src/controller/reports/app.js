const {
  completedTransactionSelectAlls
} = require("../../use-cases/reports/app");

//####################
const selectAllCompletedTransaction = require("./select-all-completed-transactions");
//####################
const selectAllCompletedTransactions = selectAllCompletedTransaction({
  completedTransactionSelectAlls
});

const services = Object.freeze({ selectAllCompletedTransactions });

module.exports = services;
module.exports = { selectAllCompletedTransactions };
