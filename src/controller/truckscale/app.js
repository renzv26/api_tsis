const {
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless,
  locationSelects,
  addLocations,
  updateLocations,
  addPrintLocs,
  printLocationSelects,
  updatePrintLocations,
} = require("../../use-cases/truckscale/app");

// ##########################
const truckScaleAddNew = require("./ts-add");
const selectAllTs = require("./ts-select-all");
const selectOneTs = require("./ts-select-one");
const truckScaleUpdate = require("./ts-update");
const selectLocation = require("./locations-select");
const locationAdd = require("./locations-add");
const locationUpdate = require("./locations-update");
const printLocAdd = require("./print-location-add");
const selectPrintLocation = require("./print-location-select");
const printLocationUpdate = require("./print-location-update");
// ##########################
const truckScaleAddNews = truckScaleAddNew({ addNewTruckScales });
const selectAllTss = selectAllTs({ tsSelectAlls });
const selectOneTss = selectOneTs({ tsSelectOnes });
const truckScaleUpdates = truckScaleUpdate({ updateTruckScaless });
const selectLocations = selectLocation({ locationSelects });
const locationAdds = locationAdd({ addLocations });
const locationUpdates = locationUpdate({ updateLocations });
const printLocAdds = printLocAdd({ addPrintLocs });
const selectPrintLocations = selectPrintLocation({ printLocationSelects });
const printLocationUpdates = printLocationUpdate({ updatePrintLocations });
// ##########################
const services = Object.freeze({
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates,
  selectLocations,
  locationAdds,
  locationUpdates,
  printLocAdds,
  selectPrintLocations,
  printLocationUpdates,
});

module.exports = services;
module.exports = {
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates,
  selectLocations,
  locationAdds,
  locationUpdates,
  printLocAdds,
  selectPrintLocations,
  printLocationUpdates,
};
