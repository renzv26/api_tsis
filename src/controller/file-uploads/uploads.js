const filesUpload = ({ imageUploads }) => {
  return async function post(httpRequest) {
    try {
      const { source = {} } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const posted = await imageUploads({
        source
      });
      // check if there is file to be uploaded
      if (httpRequest.file.length > 0) {
        const filePath = `/uploads/${httpRequest.file[0].filename}`
       
        return {
          headers: {
            "Content-Type": "application/json",
            "Last-Modified": new Date(posted.modifiedOn).toUTCString()
          },
          statusCode: 201,
          body: { posted, imagePath: filePath }
        };
      } else {
        throw new Error("Please upload a file.");
      }
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = filesUpload;
