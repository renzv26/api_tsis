const initDb = ({ dotenv }) => {
  return function init() {
    dotenv.config();
    const env = process.env.NODE_ENV; // environment
    if (env == "dev") {
      process.env.DB = process.env.TSIS_DB;
    }

    if (env == "sqa") {
      process.env.DB = process.env.TSIS_DB;
    }

    if (env == "uat") {
      process.env.DB = process.env.TSIS_DB;
    }

    if (env == "test") {
      process.env.DB = process.env.TSIS_DB_TEST;
    }
  };
};

module.exports = initDb;
