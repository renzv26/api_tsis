const db = ({ dbs }) => {
    return Object.freeze({
        selectAllPos,
        selectPoByNumber
    });
    
    
    async function selectAllPos() {
        const db = await dbs();
        const sql = `SELECT * FROM ts_po`;
        return db.query(sql);
    }

    async function selectPoByNumber(po){
        const db = await dbs()
        const sql = `
        SELECT id FROM ts_po WHERE purchase_order = $1
        `
        const params = [po]
        return db.query(sql, params);
    }
   
  };
  
  module.exports = db;
  