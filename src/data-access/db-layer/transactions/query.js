const db = ({ dbs }) => {
  return Object.freeze({
    insertNewTransaction,
    selectAllTransactions,
    selectOneTransactionsSpecialCase,
    fetchDataForUpdate,
    insertNewWithdrawalTransaction,
    updateTransactionWithdrawal,
    selectOneTransactionById,
    addInboundTransaction,
    selectOneInboundTransactionByTransactionId,
    addOutboundTransaction,
    selectOneOutboundTransactionByTransactionId,
    updateTransactionStatus,
    getSupplierId,
    selectOneTransactionType,
    selectOneSupplier,
    selectOneRawMaterial,
    insertSupplier,
    insertRawMaterial,
    insertNewTransactionOthers,
    updateInboundTransaction,
    updateOutboundTransaction,
    checkTrackingCode,
    updateWithdrawalTransaction,
    insertPurchaseOrderId,
    selectPurchaseOrderId,
    checkTransmittalNumber,
    deletePurchaseOrderId,
    selectTruckTypeInfo,
    insertTruckInfo,
    selectInboundToUpdate,
    updateInboundDeliveryTransaction,
    returnLatestInboundFromTransactionId,
    updateInboundIdToOutbound,
    checkPurchaseOrderId,
    updateOutboundTransactionDelivery,
    returnLatestOutboundFromTransactionId,
    selectOneTransactions,
    deleteNullInboundTransactions,
    deleteNullOutboundTransactions,
    forTransmittalNotif
  });
  // emit
  async function forTransmittalNotif({}) {
    const db = await dbs();
    const sql = `
      SELECT COUNT(*) FROM ts_transactions WHERE LOWER(status) = 'for transmittal';
    `;
    return db.query(sql);
  }
  // just delete null transactions before insert to avoid duplications
  async function deleteNullOutboundTransactions({ ...info }) {
    const db = await dbs();
    const sql = `
      DELETE FROM ts_outbound_transactions WHERE transaction_id = $1 
      AND ob_weight IS NULL AND ob_timestamp IS NULL;
    `;
    const params = [info.id];
    return db.query(sql, params);
  }
  // just delete null transactions before insert to avoid duplications
  async function deleteNullInboundTransactions({ ...info }) {
    const db = await dbs();
    const sql = `
      DELETE FROM ts_inbound_transactions WHERE transaction_id = $1 
      AND ib_weight IS NULL AND ib_timestamp IS NULL;
    `;
    const params = [info.id];
    return db.query(sql, params);
  }
  // return latest outbound
  async function returnLatestOutboundFromTransactionId({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_outbound_transactions WHERE transaction_id = $1
      ORDER BY id DESC LIMIT 1;
    `;
    const params = [info.id];
    return db.query(sql, params);
  }
  // update delivery outbound; transaction
  async function updateOutboundTransactionDelivery({ ...info }) {
    const db = await dbs();
    const sql = `
        UPDATE ts_outbound_transactions SET ob_weight=$1, ob_timestamp=$2, inbound_id=$3
        WHERE transaction_id=$4;    
    `;
    const params = [
      info.ob_weight,
      info.ob_timestamp,
      info.inbound_id,
      info.transaction_id
    ];
    return db.query(sql, params);
  }
  // update inbound_id in outbound table;
  async function checkPurchaseOrderId({ ...info }) {
   
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_transactions WHERE purchase_order_id = $1;
    `;
    const params = [info.purchase_order_id];
    return db.query(sql, params);
  }
  // update inbound_id in outbound table;
  async function updateInboundIdToOutbound({ ...info }) {
    const db = await dbs();
    const sql = `
      UPDATE ts_outbound_transactions SET inbound_id = $2 WHERE id=$1;
    `;
    const params = [info.id, info.inbound_id];
    return db.query(sql, params);
  }
  // return latest inbound
  async function returnLatestInboundFromTransactionId({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_inbound_transactions WHERE transaction_id = $1
      ORDER BY id DESC LIMIT 1;
    `;
    const params = [info.id];
    return db.query(sql, params);
  }
  // update null fields on delivery inbound;
  async function updateInboundDeliveryTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
      UPDATE ts_inbound_transactions SET ib_weight=$1, ib_timestamp=$2 WHERE id=$3;
    `;
    const params = [info.ib_weight, info.ib_timestamp, info.inboundId];
    return db.query(sql, params);
  }
  // select latest inbound to update during delivery;
  async function selectInboundToUpdate({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT tsi.id FROM ts_inbound_transactions tsi
      JOIN ts_transactions tst ON tst.id = tsi.transaction_id JOIN ts_transaction_types tstt
      ON tstt.id = tst.transaction_type_id WHERE tsi.transaction_id = $2 AND LOWER(tstt.transaction_type_name) = $1
      AND tsi.ib_weight IS NULL AND tsi.ib_timestamp IS NULL AND tst.status = 'for inbound';
    `;
    const params = [info.transactionType, info.transaction_id];
    return db.query(sql, params);
  }
  // insert new truck info during transaction
  async function insertTruckInfo({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_truck_infos (truck_type_id,plate_number,created_at)
      VALUES ($1,$2,$3)`;
    const params = [info.truck_type_id, info.plate_number, info.created_at];
    await db.query(sql, params);
    const sql1 = `SELECT id FROM ts_truck_infos ORDER BY id DESC LIMIT 1;`;
    return db.query(sql1);
  }
  // select truck type info thru plate number; auto insert inbound during delivery
  async function selectTruckTypeInfo({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_truck_infos WHERE plate_number = $1;
    `;
    const params = [info.plate_number];
    return db.query(sql, params);
  }
  // delete purchase order id then insert
  async function deletePurchaseOrderId({ ...info }) {
    const db = await dbs();
    const sql = `
      DELETE FROM ts_transactions WHERE purchase_order_id = $1;
    `;
    const params = [info.id];
    return db.query(sql, params);
  }
  // check if transmittal number exist
  async function checkTransmittalNumber({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_transactions WHERE transmittal_number = $1;
    `;
    const params = [info.transmittalNumber];
    return db.query(sql, params);
  }
  // to check if purchase order id exist; where transaction is withdrawal
  async function selectPurchaseOrderId({ ...info }) {
  
    const db = await dbs();
    const sql = `
      SELECT id FROM ts_transactions WHERE purchase_order_id = $1 AND transaction_type_id = $2;
    `;
    const params = [info.po_id, info.ttype_id];
    return db.query(sql, params);
  }
  // insert purchase order id FROM API of SAP; auto withdrawal transaction type
  async function insertPurchaseOrderId({ ...info }) {
    const db = await dbs();
    const sql = `
      INSERT INTO ts_transactions (transaction_type_id,created_at,purchase_order_id,status)
      VALUES ($1,$2,$3,$4);
    `;
    const params = [info.ttype_id, info.created_at, info.po_id, info.status];
    return db.query(sql, params);
  }
  // update withdrawal transaction
  async function updateWithdrawalTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
    UPDATE ts_transactions SET transaction_type_id = $1,
    status = $2, updated_at = $3, location_delivery = $4,
    warehouse_location = $5, no_of_bags = $6, transmittal_number = $7, created_by = $8
    WHERE id = $9;
    `;
    const params = [
      // info.supplier_id,
      // info.raw_material_id,
      info.transaction_type_id,
      info.status,
      info.updated_at,
      info.location_delivery,
      info.warehouse_location,
      info.no_of_bags,
      info.transmittal_number,
      info.created_by,
      info.id
    ];
    return db.query(sql, params);
  }
  // check tracking before adding in transactions
  async function checkTrackingCode({ code }) {
    const db = await dbs();
    const sql = `
      SELECT id FROM ts_transactions WHERE tracking_code = $1;
    `;
    const params = [code];
    return db.query(sql, params);
  }
  // update outbound for others transaction; for others
  async function updateOutboundTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
        UPDATE ts_outbound_transactions SET weight_type_id=$1, ob_weight=$2, ob_timestamp=$3, truckscale_id=$4,
        inbound_id=$5 WHERE transaction_id=$6;    
    `;
    const params = [
      info.weight_type_id,
      info.ob_weight,
      info.ob_timestamp,
      info.truckscale_id,
      info.inbound_id,
      info.transaction_id
    ];
    return db.query(sql, params);
  }
  // update inbound for others transaction; for others
  async function updateInboundTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
    UPDATE ts_inbound_transactions SET weight_type_id=$1, ib_weight=$2, ib_timestamp=$3, truckscale_id=$4
    WHERE transaction_id=$5;
    `;
    const params = [
      info.weight_type_id,
      info.ib_weight,
      info.ib_timestamp,
      info.truckscale_id,
      info.transaction_id
    ];
    return db.query(sql, params);
  }
  // insert new transaction others
  async function insertNewTransactionOthers({ ...info }) {
    const db = await dbs();
    // insert into transactions table
    const sql = `
    INSERT INTO ts_transactions (
    transaction_type_id,status,created_at,created_by) 
    VALUES ($1,$2,$3,$4);
    `;
    const params = [
      info.transaction_type_id,
      info.status,
      info.created_at,
      info.created_by
    ];
    await db.query(sql, params); // run query insert

    // select transaction id
    const sql1 = `
    SELECT id FROM ts_transactions ORDER BY id DESC LIMIT 1;
    `;
    const transaction = await db.query(sql1); // run query select
    const id = transaction.rows[0].id;

    // insert to inbound and outbound
    const sql2 = [
      `INSERT INTO ts_inbound_transactions (transaction_id,users_id,drivers_id,truck_info_id)
    VALUES ($1,$2,$3,$4);`,
      `INSERT INTO ts_outbound_transactions (transaction_id,users_id,drivers_id,truck_info_id)
      VALUES ($1,$2,$3,$4);`
    ];
    // loop thru and insert
    for await (let q of sql2) {
      const params = [id, info.created_by, info.drivers_id, info.truck_info_id];
      await db.query(q, params);
    }

    // return nothing? test..
  }
  // insert raw material when doesn't exist during transaction
  async function insertRawMaterial({ ...info }) {
    const db = await dbs();
    const sql = `
        INSERT INTO ts_raw_materials (description,created_by,created_at)
        VALUES ($1,$2,$3);
    `;
    const params = [info.raw_material_name, info.created_by, info.created_at];
    await db.query(sql, params);
    const sql1 = `SELECT id FROM ts_raw_materials ORDER BY id DESC LIMIT 1;`;
    return db.query(sql1);
  }
  // insert supplier when doesn't exist during transaction
  async function insertSupplier({ ...info }) {
    const db = await dbs();
    const sql = `
      INSERT INTO ts_suppliers (name) VALUES ($1);
    `;
    const params = [info.supplier];
    await db.query(sql, params);
    const sql1 = `SELECT id FROM ts_suppliers ORDER BY id DESC LIMIT 1;`;
    return db.query(sql1);
  }
  // get one raw material
  async function selectOneRawMaterial({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_raw_materials WHERE LOWER(description) = LOWER($1);
    `;
    const params = [info.raw_material_name];
    return db.query(sql, params);
  }
  // get supplier info
  async function selectOneSupplier({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_suppliers WHERE LOWER(name) = LOWER($1);
    `;
    const params = [info.supplier];
    return db.query(sql, params);
  }
  // get transaction type from name
  async function selectOneTransactionType({ ...info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_transaction_types WHERE LOWER(transaction_type_name) = LOWER($1);
    `;
    const params = [info.transaction_type];
    return db.query(sql, params);
  }
  // return supplier Id from supplier name
  async function getSupplierId({ ...info }) {
    const db = await dbs();
    const sql = `
        SELECT id FROM ts_suppliers WHERE name=$1;
    `;
    const params = [info.supplier_name];
    return db.query(sql, params);
  }
  // add new
  async function insertNewTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO ts_transactions (transaction_type_id,status,created_at,updated_at,location_delivery,tracking_code,
    warehouse_location,no_of_bags,purchase_order_id,transmittal_number,created_by
    ) 
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)  
    `;
    const params = [
      // info.supplier_id,
      // info.raw_material_id,
      info.transaction_type_id,
      info.status,
      info.created_at,
      info.updated_at,
      info.location_delivery,
      info.tracking_code,
      info.warehouse_location,
      info.no_of_bags,
      info.purchase_order_id,
      info.transmittal_number,
      info.created_by
    ];
    await db.query(sql, params);

    // return latest transaction
    const sql1 = `SELECT id FROM ts_transactions ORDER BY id DESC LIMIT 1;`;
    return db.query(sql1);
  }
  // select all
  async function selectAllTransactions({ data }) {
    const db = await dbs();
    const sql = `
      SELECT a.supplier_name , b.description ,tst.id, tstt.transaction_type_name,
      tst.tracking_code,tst.transmittal_number,tst.purchase_order_id,tst.created_at,tst.status
      FROM ts_transactions tst 
      LEFT JOIN ts_transaction_types tstt ON tstt.id = tst.transaction_type_id 
      LEFT JOIN ts_po a ON tst.purchase_order_id = a.id
      LEFT JOIN ts_raw_materials b ON a.raw_material_id = b.id
      WHERE TO_CHAR(tst.created_at, 'yyyy-mm-dd') BETWEEN $1 AND $2
      ORDER BY tst.id DESC;
    `;
    const params = [data.from, data.to];
    return db.query(sql, params);
  }
  // select one; special case delivery
  async function selectOneTransactionsSpecialCase({ id }) {
    const db = await dbs();
    const sql = `
        SELECT a.*,
        b.inboundid,b.weighttypeid inweighttypeid,b.weight_name inweightname,b.ib_weight,b.ib_timestamp,b.userid inbounduserid,
        b.firstname inuserfn, b.lastname inuserln, b.driverid indriverid, b.driverfn indriverfn, b.driverln indriverln,
        b.truckinfo_id truckinfo_id_in,b.plate_number inplatenumber, b.truck_type intrucktype, b.truck_model intruckmodel, b.truck_size intrucksize,
        b.truckscale_name intruckscale,
        c.outboundid,c.weighttypeid outweighttypeid,c.weight_name outweightname,c.ob_weight,c.ob_timestamp,c.userid outbounduserid,
        c.firstname outuserfn, c.lastname outuserln, c.driverid outdriverid, c.driverfn outdriverfn, c.driverln outdriverln,
        c.truckinfo_id truckinfo_id_out,c.plate_number outplatenumber, c.truck_type outtrucktype, c.truck_model outtruckmodel, c.truck_size outrucksize,
        c.truckscale_name outtruckscale,c.remarks,c.inbound_id
        FROM
        (
          SELECT a.supplier_name, a.supplier_address, b.description, tst.id transactionId, tst.status,tst.tracking_code,tst.no_of_bags,
          tstt.id transactionTypeId, tstt.transaction_type_name,
          tst.location_delivery,tst.warehouse_location,tst.purchase_order_id,tst.transmittal_number,
          tsu.id userId, tsu.firstname, tsu.lastname
          FROM ts_transactions tst 

          LEFT JOIN ts_po a ON tst.purchase_order_id = a.id
          LEFT JOIN ts_raw_materials b ON a.raw_material_id = b.id

          LEFT JOIN ts_transaction_types tstt ON tstt.id = tst.transaction_type_id 
          LEFT JOIN ts_users tsu ON tsu.id = tst.created_by
        )a LEFT JOIN
        (
          SELECT tsi.id inboundId,tst.id transactionId,tsw.id weightTypeId, tsw.weight_name, tsi.ib_weight, tsi.ib_timestamp,
          tsu.id userId, tsu.firstname, tsu.lastname, tstr.id truckscaleid, tstr.truckscale_name, tsd.id driverId,
          tsd.firstname driverFn, tsd.lastname driverLn,tstti.id truckinfo_id, tstti.plate_number,
          tstt.truck_type,tstt.truck_model,tstt.truck_size
          FROM ts_inbound_transactions tsi LEFT JOIN ts_transactions tst ON tst.id = tsi.transaction_id
          LEFT JOIN ts_weight_types tsw ON tsw.id = tsi.weight_type_id LEFT JOIN ts_truckscales tstr ON tstr.id = tsi.truckscale_id
          LEFT JOIN ts_drivers tsd ON tsd.id = tsi.drivers_id LEFT JOIN ts_truck_infos tstti ON tstti.id = tsi.truck_info_id
          LEFT JOIN ts_truck_types tstt ON tstt.id = tstti.truck_type_id LEFT JOIN ts_users tsu ON tsu.id = tsi.users_id
        )b ON b.transactionid = a.transactionid LEFT JOIN
        (
          SELECT tso.id outboundId,tst.id transactionId,tsw.id weightTypeId, tsw.weight_name, tso.ob_weight, tso.ob_timestamp,
          tsu.id userId, tsu.firstname, tsu.lastname, tstr.id, tstr.truckscale_name, tsd.id driverId,
          tsd.firstname driverFn, tsd.lastname driverLn,tstti.id truckinfo_id, tstti.plate_number,
          tstt.truck_type,tstt.truck_model,tstt.truck_size,
          tso.inbound_id,tso.remarks
          FROM ts_outbound_transactions tso LEFT JOIN ts_transactions tst ON tst.id = tso.transaction_id
          LEFT JOIN ts_weight_types tsw ON tsw.id = tso.weight_type_id LEFT JOIN ts_truckscales tstr ON tstr.id = tso.truckscale_id
          LEFT JOIN ts_drivers tsd ON tsd.id = tso.drivers_id LEFT JOIN ts_truck_infos tstti ON tstti.id = tso.truck_info_id
          LEFT JOIN ts_truck_types tstt ON tstt.id = tstti.truck_type_id LEFT JOIN ts_users tsu ON tsu.id = tso.users_id
          LEFT JOIN ts_inbound_transactions tsi ON tsi.id = tso.inbound_id
        )c ON c.transactionid = a.transactionid AND c.inbound_id = b.inboundid
        WHERE a.transactionid = $1;
    `;
    const params = [id];
    return db.query(sql, params);
  }
  // select one for normal transactions
  async function selectOneTransactions({ id }) {
    const db = await dbs();
    const sql = `
        SELECT a.*,
        b.inboundid,b.weighttypeid inweighttypeid,b.weight_name inweightname,b.ib_weight,b.ib_timestamp,b.userid inbounduserid,
        b.firstname inuserfn, b.lastname inuserln, b.driverid indriverid, b.driverfn indriverfn, b.driverln indriverln,
        b.truckinfo_id truckinfo_id_in,b.plate_number inplatenumber, b.truck_type intrucktype, b.truck_model intruckmodel, b.truck_size intrucksize,
        b.truckscale_name intruckscale,
        c.outboundid,c.weighttypeid outweighttypeid,c.weight_name outweightname,c.ob_weight,c.ob_timestamp,c.userid outbounduserid,
        c.firstname outuserfn, c.lastname outuserln, c.driverid outdriverid, c.driverfn outdriverfn, c.driverln outdriverln,
        c.truckinfo_id truckinfo_id_out,c.plate_number outplatenumber, c.truck_type outtrucktype, c.truck_model outtruckmodel, c.truck_size outrucksize,
        c.truckscale_name outtruckscale,c.remarks,c.inbound_id
        FROM
        (
          SELECT a.supplier_name, a.supplier_address, b.description, tst.id transactionId, tst.status,tst.tracking_code,tst.no_of_bags,
          tstt.id transactionTypeId, tstt.transaction_type_name,
          tst.location_delivery,tst.warehouse_location,tst.purchase_order_id,tst.transmittal_number,
          tsu.id userId, tsu.firstname, tsu.lastname
          FROM ts_transactions tst 

          LEFT JOIN ts_po a ON tst.purchase_order_id = a.id
          LEFT JOIN ts_raw_materials b ON a.raw_material_id = b.id

          LEFT JOIN ts_transaction_types tstt ON tstt.id = tst.transaction_type_id 
          LEFT JOIN ts_users tsu ON tsu.id = tst.created_by
        )a LEFT JOIN
        (
          SELECT tsi.id inboundId,tst.id transactionId,tsw.id weightTypeId, tsw.weight_name, tsi.ib_weight, tsi.ib_timestamp,
          tsu.id userId, tsu.firstname, tsu.lastname, tstr.id truckscaleid, tstr.truckscale_name, tsd.id driverId,
          tsd.firstname driverFn, tsd.lastname driverLn,tstti.id truckinfo_id, tstti.plate_number,
          tstt.truck_type,tstt.truck_model,tstt.truck_size
          FROM ts_inbound_transactions tsi LEFT JOIN ts_transactions tst ON tst.id = tsi.transaction_id
          LEFT JOIN ts_weight_types tsw ON tsw.id = tsi.weight_type_id LEFT JOIN ts_truckscales tstr ON tstr.id = tsi.truckscale_id
          LEFT JOIN ts_drivers tsd ON tsd.id = tsi.drivers_id LEFT JOIN ts_truck_infos tstti ON tstti.id = tsi.truck_info_id
          LEFT JOIN ts_truck_types tstt ON tstt.id = tstti.truck_type_id LEFT JOIN ts_users tsu ON tsu.id = tsi.users_id
        )b ON b.transactionid = a.transactionid LEFT JOIN
        (
          SELECT tso.id outboundId,tst.id transactionId,tsw.id weightTypeId, tsw.weight_name, tso.ob_weight, tso.ob_timestamp,
          tsu.id userId, tsu.firstname, tsu.lastname, tstr.id, tstr.truckscale_name, tsd.id driverId,
          tsd.firstname driverFn, tsd.lastname driverLn,tstti.id truckinfo_id, tstti.plate_number,
          tstt.truck_type,tstt.truck_model,tstt.truck_size,
          tso.inbound_id,tso.remarks
          FROM ts_outbound_transactions tso LEFT JOIN ts_transactions tst ON tst.id = tso.transaction_id
          LEFT JOIN ts_weight_types tsw ON tsw.id = tso.weight_type_id LEFT JOIN ts_truckscales tstr ON tstr.id = tso.truckscale_id
          LEFT JOIN ts_drivers tsd ON tsd.id = tso.drivers_id LEFT JOIN ts_truck_infos tstti ON tstti.id = tso.truck_info_id
          LEFT JOIN ts_truck_types tstt ON tstt.id = tstti.truck_type_id LEFT JOIN ts_users tsu ON tsu.id = tso.users_id
          LEFT JOIN ts_inbound_transactions tsi ON tsi.id = tso.inbound_id
        )c ON c.transactionid = a.transactionid OR c.inbound_id = b.inboundid
        WHERE a.transactionid = $1;
    `;
    const params = [id];
    return db.query(sql, params);
  }
  // fetch data for update
  async function fetchDataForUpdate({ id }) {
    const db = await dbs();
    const sql = ``;
    const params = [id];
    return db.query(sql, params);
  }

  async function insertNewWithdrawalTransaction({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_transactions (purchase_order_id) VALUES ($1)`;
    const params = [info.purchase_order_id];
    return db.query(sql, params);
  }

  async function updateTransactionWithdrawal({ id, ...info }) {
    const db = await dbs();
    const sql = `
    UPDATE public.ts_transactions
    SET drivers_id = $1, truck_info_id = $2, supplier_id =$3, raw_material_id=$4, transaction_type_id=$5, 
    status=$6, updated_at=$7, location_delivery=$8, tracking_code = $9, warehouse_location = $10, 
    no_of_bags = $11, purchase_order_id = $12, transmittal_number = $13
	  WHERE id = $14
    `;
    const params = [
      info.drivers_id,
      info.truck_info_id,
      info.supplier_id,
      info.raw_material_id,
      info.transaction_type_id,
      info.status,
      info.updated_at,
      info.location_delivery,
      info.tracking_code,
      info.warehouse_location,
      info.no_of_bags,
      info.purchase_order_id,
      info.transmittal_number,
      id
    ];
    return db.query(sql, params);
  }

  async function selectOneTransactionById({ id }) {
    const db = await dbs();
    const sql = `
    SELECT a.*, b.transaction_type_name FROM ts_transactions a 
    LEFT JOIN ts_transaction_types b on a.transaction_type_id = b.id
    WHERE a.id = $1
    `;
    const params = [id];
    return db.query(sql, params);
  }

  async function addInboundTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO ts_inbound_transactions(
    transaction_id, weight_type_id, ib_weight, ib_timestamp, users_id, truckscale_id,drivers_id,truck_info_id)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
    `;
    const params = [
      info.transaction_id,
      info.weight_type_id,
      info.ib_weight,
      info.ib_timestamp,
      info.users_id,
      info.truckscale_id,
      info.drivers_id,
      info.truck_info_id
    ];
    return db.query(sql, params);
  }

  async function selectOneInboundTransactionByTransactionId({
    transaction_id
  }) {
    const db = await dbs();
    const sql = `
    SELECT * FROM ts_inbound_transactions WHERE transaction_id = $1;
    `;
    const params = [transaction_id];
    return db.query(sql, params);
  }

  async function addOutboundTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO ts_outbound_transactions(
    transaction_id, weight_type_id, ob_weight, ob_timestamp, users_id, truckscale_id, drivers_id, truck_info_id, inbound_id, remarks)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);
    `;
    const params = [
      info.transaction_id,
      info.weight_type_id,
      info.ob_weight,
      info.ob_timestamp,
      info.users_id,
      info.truckscale_id,
      info.drivers_id,
      info.truck_info_id,
      info.inbound_id,
      info.remarks
    ];
    return db.query(sql, params);
  }

  async function selectOneOutboundTransactionByTransactionId({
    transaction_id
  }) {
    const db = await dbs();
    const sql = `
    SELECT * FROM ts_outbound_transactions WHERE transaction_id = $1
    `;
    const params = [transaction_id];
    return db.query(sql, params);
  }

  async function updateTransactionStatus({ id, status }) {
    const db = await dbs();
    const sql = `
    UPDATE ts_transactions SET status = $1 WHERE id = $2
    `;
    const params = [status, id];
    return db.query(sql, params);
  }
};

module.exports = db;
