const { makeDb } = require("../app");
const db = require("./query");

const rawMaterialDb = makeDb({ db });

module.exports = rawMaterialDb;
