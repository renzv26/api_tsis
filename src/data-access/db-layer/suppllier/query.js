const db = ({ dbs }) => {
    return Object.freeze({
        selectSupplier,
    })

    async function selectSupplier() {
        const db = await dbs();
        const sql = `select DISTINCT(i.*) from suppliers i
        JOIN transactions t ON t.supplier=i.id
        WHERE t.status='completed' and t.transaction_type_id='2'`;
        return db.query(sql);
      }

};
module.exports = db;