const { makeDb } = require("../app");
const db = require("./query");

const supplierDb = makeDb({ db });

module.exports = supplierDb;
