const { makeDb } = require("../app");
const db = require("./query");

const suppliersDb = makeDb({ db });

module.exports = suppliersDb;
