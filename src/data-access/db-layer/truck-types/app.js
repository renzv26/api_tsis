const { makeDb } = require("../app");
const db = require("./query");

const truckTypesDb = makeDb({ db });

module.exports = truckTypesDb;
