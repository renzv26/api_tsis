const db = ({ dbs }) => {
  return Object.freeze({
    insertTruckType,
    selectAllTruckTypes,
    selectOneTruckType,
    updateTruckType,
    selectTruckTypeAndModel, // for checking during add
    selectTruckTypeAndModelUpdate // for checking during update
  });
  async function selectTruckTypeAndModelUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_truck_types WHERE LOWER(truck_type)=LOWER($1) AND LOWER(truck_model)=LOWER($2) AND id <> $3;`;
    const params = [info.truck_type, info.truck_model, id];
    return db.query(sql, params);
  }

  async function selectTruckTypeAndModel({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_truck_types WHERE truck_type=$1 AND truck_model=$2;`;
    const params = [info.truck_type, info.truck_model];
    return db.query(sql, params);
  }

  async function insertTruckType({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_truck_types (truck_type,truck_model,truck_size,created_at)
      VALUES ($1,$2,$3,$4)`;
    const params = [
      info.truck_type,
      info.truck_model,
      info.truck_size,
      info.created_at
    ];
    return db.query(sql, params);
  }

  async function selectAllTruckTypes() {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_truck_types
      `;
    return db.query(sql);
  }

  async function selectOneTruckType({ id }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_truck_types WHERE id = $1
      `;
    const params = [id];
    return db.query(sql, params);
  }

  async function updateTruckType({ id, ...info }) {
    const db = await dbs();
    const sql = `
      UPDATE ts_truck_types SET truck_type = $1, truck_model = $2, truck_size = $3, updated_at = $4
      WHERE id = $5
      `;
    const params = [
      info.truck_type,
      info.truck_model,
      info.truck_size,
      info.updated_at,
      id
    ];
    return db.query(sql, params);
  }
};

module.exports = db;
