const db = ({ dbs }) => {
  return Object.freeze({
    insertAccessRights,
    deleteAccessRightsOnRole,
    selectAccessRightsOnRole
  });
  // add new action
  async function insertAccessRights({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_access_rights (actions_id,roles_id,created_at)
    VALUES ($1,$2,$3);`;
    const params = [info.actions_id, info.roles_id, info.created_at];
    return db.query(sql, params);
  }
  // to delete all access rights base on role; then insert
  async function deleteAccessRightsOnRole({ ...info }) {
    const db = await dbs();
    const sql = `DELETE FROM ts_access_rights WHERE roles_id = $1;`;
    const params = [info.roles_id];
    return db.query(sql, params);
  }
  // select all modules and actions base on role id
  async function selectAccessRightsOnRole({ id }) {
    const db = await dbs();
    const sql = `SELECT a.id roleid,a.name,a.status,b.modules_id,b.modulename,b.modulestatus,b.actions_id,
    b.actionname,b.actionstatus FROM
    (
      -- role
      SELECT DISTINCT tsr.id,tsr.name,tsr.status FROM ts_access_rights tsar 
      LEFT JOIN ts_roles tsr ON tsr.id = tsar.roles_id
    )a LEFT JOIN
    (
      -- actions and module of that role
      SELECT tsr.id role_id,tsm.id modules_id,tsm.descriptions modulename,tsm.status modulestatus,tsa.id actions_id,
      tsa.description actionname,tsa.status actionstatus FROM ts_access_rights tsar 
      LEFT JOIN ts_roles tsr ON tsr.id = tsar.roles_id LEFT JOIN ts_actions tsa ON tsa.id = tsar.actions_id
      LEFT JOIN ts_modules tsm ON tsm.id = tsa.modules_id
    )b ON b.role_id = a.id WHERE a.id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
