const db = ({ dbs }) => {
  return Object.freeze({
    insertUser,
    selectUserFullName,
    selectUserId,
    selectAllUsers,
    selectSingleUser,
    userLogin,
    selectCredentials,
    getTokenOfUser,
    checkRoleStatus,
    updateUser,
    whichTruckScaleLoggedIn,
    insertActivityLogs,
    getRoleOfUser,
    getPreviousValuesOfUser,
    getDataOfUserDuringLogin,
    resetPasswords,
    selectAllActivityLogs
  });
  // select all activity logs
  async function selectAllActivityLogs(dateRange) {
    const db = await dbs();
    const sql = `
        SELECT tsa.id,tsa.users_id,firstname,lastname,action_type,
        table_affected,new_values,prev_values,tsa.created_at,tsa.updated_at
        FROM ts_activity_logs tsa LEFT JOIN ts_users tsu
        ON tsu.id = tsa.users_id
        WHERE TO_CHAR(tsa.created_at , 'yyyy-mm-dd') BETWEEN $1 AND $2
        ORDER BY tsa.id DESC;
    `;
    const params = [dateRange.date_from, dateRange.date_to];
    return db.query(sql, params);
  }
  // first time login reset password
  async function resetPasswords({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_users SET password = $1,token = NULL, login_count = 1 WHERE id = $2;`;
    const params = [info.password, id];
    return db.query(sql, params);
  }
  // add new action
  async function insertUser({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_users (employee_id,email,firstname,middlename,lastname,name_extension,password,
        role_id,employee_image,employee_signature,created_at,login_count,contact_number) VALUES
        ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$12,0,$11);`;
    const params = [
      info.employee_id,
      info.email,
      info.firstname,
      info.middlename,
      info.lastname,
      info.name_extension,
      info.password,
      info.role_id,
      info.employee_image,
      info.employee_signature,
      info.contact_number,
      info.created_at
    ];
    return db.query(sql, params);
  }
  //update user
  async function updateUser({ id, ...info }) {
    const db = await dbs();
    const sql = `
    UPDATE ts_users SET employee_id = $1, email = $2, firstname = $3, middlename = $4, lastname = $5,
    name_extension = $6, role_id = $7, employee_image = $8, employee_signature = $9, updated_at = $10,
    contact_number = $11 WHERE id = $12;`;
    const params = [
      info.employee_id,
      info.email,
      info.firstname,
      info.middlename,
      info.lastname,
      info.name_extension,
      info.role_id,
      info.employee_image,
      info.employee_signature,
      info.updated_at,
      info.contact_number,
      id
    ];
    return db.query(sql, params);
  }
  // select user thru firstname and lastname to check if exist
  async function selectUserFullName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_users WHERE LOWER(firstname) = LOWER($1) AND LOWER(lastname) = LOWER($2);`;
    const params = [info.firstname, info.middlename];
    return db.query(sql, params);
  }
  // select employee id if exist
  async function selectUserId({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_users WHERE LOWER(employee_id) = LOWER($1);`;
    const params = [info.employee_id];
    return db.query(sql, params);
  }
  // select all users to display
  async function selectAllUsers({}) {
    const db = await dbs();
    const sql = `SELECT tsu.id,employee_id,email,lastname,firstname,contact_number,tsr.name,tsr.status FROM ts_users tsu 
    LEFT JOIN ts_roles tsr ON tsr.id = tsu.role_id;`;
    return db.query(sql);
  }
  // select single user
  async function selectSingleUser({ id }) {
    const db = await dbs();
    const sql = `SELECT tsu.id,employee_id,email,lastname,firstname,middlename,name_extension,contact_number,
    employee_image,employee_signature,login_count,tsr.id role_id,tsr.name,tsr.status FROM ts_users tsu 
    LEFT JOIN ts_roles tsr ON tsr.id = tsu.role_id WHERE tsu.id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // select employee ID and password for login;
  async function selectCredentials({ ...info }) {
    const db = await dbs();
    const sql = `SELECT a.id ,role_id, b.name as role_name,login_count FROM ts_users a
    LEFT JOIN ts_roles b on a.role_id = b.id
    WHERE employee_id=$1 AND password=$2
    ;`;
    const params = [info.employee_id, info.password];
    return db.query(sql, params);
  }
  // login of user to system; generate token
  async function userLogin({ ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_users SET token=$1, login_count = 
    CASE
    WHEN login_count > 0 THEN  login_count + 1
    ELSE 0 
    END
    WHERE employee_id=$2 AND password=$3;`;
    const params = [info.token, info.employee_id, info.password];
    return db.query(sql, params);
  }
  // get token of user
  async function getTokenOfUser({ ...info }) {
    const db = await dbs();
    const sql = `select * from ts_users where employee_id = $1
    and password = $2`;
    const params = [info.employee_id, info.password];
    return db.query(sql, params);
  }
  // check status of role
  async function checkRoleStatus({ ...info }) {
    const db = await dbs();
    const sql = `SELECT status FROM ts_roles WHERE id = $1;`;
    const params = [info.role_id];
    return db.query(sql, params);
  }
  // query which truckscale the user logged in
  async function whichTruckScaleLoggedIn({ ...info }) {
    const db = await dbs();
    const sql = `SELECT id FROM ts_truckscales WHERE ip_address = $1 AND device_name = $2;`;
    const params = [info.ip, info.device_name];
    return db.query(sql, params);
  }
  // ############### Activity logs insert
  async function insertActivityLogs({ ...info }) {
    const db = await dbs();

    const sql = `INSERT INTO ts_activity_logs (action_type,table_affected,new_values,prev_values,created_at,updated_at,users_id)
    VALUES ($1,$2,$3,$4,$5,$6,$7);`;
    const params = [
      info.data.action_type,
      info.data.table_affected,
      info.data.new_values,
      info.data.prev_values,
      info.data.created_at,
      info.data.updated_at,
      info.data.users_id
    ];
    return db.query(sql, params);
  }
  // ############### end activity logs insert
  // return the role of the user LOGS
  async function getRoleOfUser({ id }) {
    const db = await dbs();
    const sql = `SELECT id,name,status FROM ts_roles WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // return previous values of user before update LOGS
  async function getPreviousValuesOfUser({ id }) {
    const db = await dbs();
    const sql = `SELECT u.id,employee_id,email,firstname,middlename,lastname,name_extension,password,
    employee_image,employee_signature,contact_number,r.id role_id,r.name,r.status 
    FROM ts_users u LEFT JOIN ts_roles r
    ON r.id = u.role_id WHERE u.id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // return previous values of user before login LOGS
  async function getDataOfUserDuringLogin({ ...info }) {
    const db = await dbs();
    const sql = `SELECT u.id,employee_id,email,firstname,middlename,lastname,name_extension,password,
    employee_image,employee_signature,contact_number,token,r.id role_id,r.name,r.status 
    FROM ts_users u LEFT JOIN ts_roles r
    ON r.id = u.role_id WHERE u.employee_id=$1 AND u.password=$2;`;
    const params = [info.employee_id, info.password];
    return db.query(sql, params);
  }
};

module.exports = db;
