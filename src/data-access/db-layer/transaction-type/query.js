const db = ({ dbs }) => {
  return Object.freeze({
    insertNewTransactionType,
    selectByName,
    selectAllTransactionType,
    selectOneTransactionType,
    selectByNameUpdate,
    updateTransactionType
  });
  // add new supplier
  async function insertNewTransactionType({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_transaction_types (transaction_type_name,created_at)
      VALUES ($1,$2);`;
    const params = [info.name, info.created_at];
    return db.query(sql, params);
  }
  // check if name exist
  async function selectByName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_transaction_types WHERE LOWER(transaction_type_name) = LOWER($1);`;
    const params = [info.name];
    return db.query(sql, params);
  }
  // select all
  async function selectAllTransactionType() {
    const db = await dbs();
    const sql = `SELECT * FROM ts_transaction_types;`;
    return db.query(sql);
  }
  // select one
  async function selectOneTransactionType({ id }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_transaction_types WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // select name during update
  async function selectByNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_transaction_types WHERE LOWER(transaction_type_name) = LOWER($1) AND id <> $2;`;
    const params = [info.name, id];
    return db.query(sql, params);
  }
  // update
  async function updateTransactionType({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_transaction_types SET transaction_type_name=$1, updated_at=$2 WHERE id=$3;`;
    const params = [info.name, info.updated_at, id];
    return db.query(sql, params);
  }
};

module.exports = db;
