const { makeDb } = require("../app");
const db = require("./query");

const driversDb = makeDb({ db });

module.exports = driversDb;
