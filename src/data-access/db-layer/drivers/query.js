const db = ({ dbs }) => {
  return Object.freeze({
    insertNewDriver,
    findByName,
    findByNameUpdate,
    updateDriver,
    selectAllDrivers,
    selectOneDriver,
    insertDriverInTransaction
  });
  // add new driver in transaction add
  async function insertDriverInTransaction({ ...info }) {
    const db = await dbs();
    const sql = `
        INSERT INTO ts_drivers (firstname,lastname,created_at)
        VALUES ($1,$2,$3);
    `;
    const params = [info.firstname, info.lastname, info.created_at];
    await db.query(sql, params);
    const sql1 = `SELECT id FROM ts_drivers ORDER BY id DESC LIMIT 1;`;
    return db.query(sql1);
  }
  // add new driver
  async function insertNewDriver({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO ts_drivers (firstname,middlename,lastname,name_extension,created_at) " +
      "VALUES($1,$2,$3,$4,$5);";
    const params = [
      info.firstname,
      info.middlename,
      info.lastname,
      info.name_extension,
      info.created_at
    ];
    return db.query(sql, params);
  }
  // find by full name during insert
  async function findByName({ ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_drivers WHERE LOWER(firstname) = LOWER($1) " +
      "AND LOWER(lastname) = LOWER($2);";
    const params = [info.firstname, info.lastname];
    return db.query(sql, params);
  }
  // find by full name during update
  async function findByNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_drivers WHERE LOWER(firstname) = LOWER($1) " +
      "AND LOWER(lastname) = LOWER($2) AND id <> $3;";
    const params = [info.firstname, info.lastname, id];
    return db.query(sql, params);
  }
  // update existing driver
  async function updateDriver({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE ts_drivers SET firstname = $1, middlename = $2, lastname = $3, name_extension = $4, " +
      "updated_at = $6 WHERE id = $5";
    const params = [
      info.firstname,
      info.middlename,
      info.lastname,
      info.name_extension,
      id,
      info.updated_at
    ];
    return db.query(sql, params);
  }
  // select all drivers
  async function selectAllDrivers() {
    const db = await dbs();
    const sql =
      "SELECT id,firstname,middlename,lastname,name_extension FROM ts_drivers;";
    return db.query(sql);
  }
  // select single driver
  async function selectOneDriver({ id }) {
    const db = await dbs();
    const sql =
      "SELECT id,firstname,middlename,lastname,name_extension FROM ts_drivers WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
