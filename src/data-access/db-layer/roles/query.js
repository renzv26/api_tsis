const db = ({ dbs }) => {
  return Object.freeze({
    insertRoles,
    selectByRoleName,
    selectByRoleNameUpdate,
    updateRoles,
    selectAllRoles,
    selectOneRole,
    returnCreatedBy // return created by user name
  });
  // get the name of the user who created the role
  async function returnCreatedBy({ id }) {
    const db = await dbs();
    const sql = `SELECT id,employee_id,firstname,lastname
    FROM ts_users WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // add new roles
  async function insertRoles({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_roles (name,status,created_at,created_by)
    VALUES ($1,$2,$3,$4);`;
    const params = [info.name, info.status, info.created_at, info.created_by];
    return db.query(sql, params);
  }
  // select role name to check if exist
  async function selectByRoleName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_roles WHERE LOWER(name) = LOWER($1);`;
    const params = [info.name];
    return db.query(sql, params);
  }
  // select role name to check if exist during update
  async function selectByRoleNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_roles WHERE LOWER(name) = LOWER($1) AND id <> $2;`;
    const params = [info.name, id];
    return db.query(sql, params);
  }
  // update existing role
  async function updateRoles({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_roles SET name = $1, status = $2, updated_at = $5, modified_by = $3
    WHERE id = $4;`;
    const params = [
      info.name,
      info.status,
      info.modified_by,
      id,
      info.updated_at
    ];
    return db.query(sql, params);
  }
  // select all roles
  async function selectAllRoles({}) {
    const db = await dbs();
    const sql = `SELECT a.role_id,a.rolename,a.rolestatus,a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,a.created_at,
    b.firstname modify_fn,b.middlename modify_mn,b.lastname modify_ln,b.updated_at
    FROM
    (
      SELECT tsr.id role_id,tsr.name rolename,tsr.status rolestatus,tsu.firstname,tsu.middlename,tsu.lastname,tsr.created_at 
      FROM ts_roles tsr LEFT JOIN ts_users tsu ON tsu.id = tsr.created_by
    )a LEFT JOIN
    (
      SELECT tsr.id role_id,tsr.name rolename,tsr.status rolestatus,tsu.firstname,tsu.middlename,tsu.lastname,tsr.updated_at 
      FROM ts_roles tsr LEFT JOIN ts_users tsu ON tsu.id = tsr.modified_by
    )b ON b.role_id = a.role_id;`;
    return db.query(sql);
  }
  // select single role
  async function selectOneRole({ id }) {
    const db = await dbs();
    const sql = `SELECT a.role_id,a.rolename,a.rolestatus,a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,a.created_at,
    b.firstname modify_fn,b.middlename modify_mn,b.lastname modify_ln,b.updated_at
    FROM
    (
      SELECT tsr.id role_id,tsr.name rolename,tsr.status rolestatus,tsu.firstname,tsu.middlename,tsu.lastname,tsr.created_at 
      FROM ts_roles tsr LEFT JOIN ts_users tsu ON tsu.id = tsr.created_by
    )a LEFT JOIN
    (
      SELECT tsr.id role_id,tsr.name rolename,tsr.status rolestatus,tsu.firstname,tsu.middlename,tsu.lastname,tsr.updated_at 
      FROM ts_roles tsr LEFT JOIN ts_users tsu ON tsu.id = tsr.modified_by
    )b ON b.role_id = a.role_id WHERE a.role_id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
