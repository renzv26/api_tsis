const db = ({ dbs }) => {
  return Object.freeze({
    selectAllCompletedTransaction
  });
  
  async function selectAllCompletedTransaction(data) {
    const db = await dbs();
    const sql = `
      SELECT DISTINCT a.*,b.*,c.*
      FROM
      (
      SELECT DISTINCT a.supplier_name, a.supplier_address, c.firstname as wfirstname, c.lastname as wlastname, tst.created_by, b.description, tst.id,tstt.transaction_type_name,tst.purchase_order_id,tst.tracking_code,tst.transmittal_number,
      tst.no_of_bags, tsd.firstname,tsd.lastname,tsti.plate_number,tst.created_at,tst.status
      FROM ts_transactions tst 
      
      LEFT JOIN ts_users c ON tst.created_by = c.id
      LEFT JOIN ts_po a ON tst.purchase_order_id = a.id
      LEFT JOIN ts_raw_materials b ON a.raw_material_id = b.id

      LEFT JOIN ts_transaction_types tstt ON tstt.id = tst.transaction_type_id
      LEFT JOIN ts_inbound_transactions tsi ON tsi.transaction_id = tst.id LEFT JOIN ts_drivers tsd ON tsd.id = tsi.drivers_id
      LEFT JOIN ts_truck_infos tsti ON tsti.id = tsi.truck_info_id
      )a LEFT JOIN
      (
        SELECT tsi.id inbound_id,tsi.transaction_id transaction_id_ib, tsi.ib_weight,tsi.ib_timestamp
        FROM ts_inbound_transactions tsi LEFT JOIN ts_transactions tst ON tst.id = tsi.transaction_id
      )b ON b.transaction_id_ib = a.id LEFT JOIN
      (
        SELECT tso.id outbound_id,tso.transaction_id transaction_id_ob, tso.ob_weight,tso.ob_timestamp,tso.inbound_id ob_inbound_id
        FROM ts_outbound_transactions tso LEFT JOIN ts_transactions tst ON tst.id = tso.transaction_id
      )c ON c.transaction_id_ob = a.id 
      WHERE LOWER(a.status)='completed' AND TO_CHAR(a.created_at, 'yyyy-mm-dd') BETWEEN $1 AND $2
      ORDER BY a.id DESC;
    `;
    const params = [data.from, data.to];
    return db.query(sql, params);
  }
};

module.exports = db;
