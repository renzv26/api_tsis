const { makeDb } = require("../app");
const db = require("./query");

const reportsDb = makeDb({ db });

module.exports = reportsDb;
