const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const users = {
  // this is the account for the service layer

  usersUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_USERS('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  // usersSelectOne: async ({ info }) => {
  //   try {
  //     const cookie = info.cookie; // store cookie
  //     delete info.cookie; // remove cookie

  //     const res = await axios({
  //       method: "GET",
  //       url: `${url}/$crossjoin(U_BFI_TS_USERS,U_BFI_TS_ROLE,EmployeesInfo)?$expand=U_BFI_TS_USERS($select=Code,U_APP_EMP_ID,U_TS_ROLE_ID,U_TS_CONTACT,U_TS_STATUS,U_TS_IS_DIRECTUSER,U_TS_EMP_ID),U_BFI_TS_ROLE($select=Code,U_TS_NAME,U_TS_STATUS),EmployeesInfo($select=LastName,FirstName,EmployeeID,eMail)&$filter=U_BFI_TS_USERS/U_TS_ROLE_ID eq U_BFI_TS_ROLE/Code and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID and U_BFI_TS_USERS/Code eq '${info.id}'`,
  //       headers: {
  //         "Content-Type": "application/json",
  //         Cookie: `${cookie};`
  //       },
  //       httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //     });

  //     const req = {
  //       status: res.status,
  //       msg: res.statusText,
  //       data: res.data.value
  //     };

  //     return req;
  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // },

  // usersSelectOneInEmployeesInfo: async ({ info }) => {
  //   try {
  //     const cookie = info.cookie; // store cookie
  //     delete info.cookie; // remove cookie

  //     const res = await axios({
  //       method: "GET",
  //       url: `${url}/EmployeesInfo?$filter=EmployeeID eq ${info.id}`,
  //       headers: {
  //         "Content-Type": "application/json",
  //         Cookie: `${cookie};`
  //       },
  //       httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //     });

  //     const req = {
  //       status: res.status,
  //       msg: res.statusText,
  //       data: res.data.value
  //     };

  //     return req;
  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // },
  usersSelectOneInEmployeesInfo: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select * from "${process.env.sapDatabase}"."OHEM" "a" where "a"."empID" = ${info.id}`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  usersSelectInEmployeesInfo: async ({ info }) => {
    try {
      const client = await hdbClient();

      // const sql = `select * from "${process.env.sapDatabase}"."OHEM" "a" where "a"."empID" = ${info.id}`;
      const sql = `SELECT "a"."empID", "a"."firstName", "a"."lastName" 
      FROM "${process.env.sapDatabase}"."OHEM" as "a"
      Where "empID" = ${info.id}
      UNION
      SElect "b"."empID", "b"."firstName", "b"."lastName" FROM "${process.env.reviveDB}"."OHEM" as "b"
      Where "empID" = ${info.id}`;
      

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  usersSelectOneInEmployeesInfoRevive: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select * from "${process.env.reviveDB}"."OHEM" "a" where "a"."empID" = ${info.id}`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectPinCodeFromId: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_USERS?$filter=Code eq '${info.id}' and U_TS_PIN eq ${info.pin}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  employeeInfosSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."empID", "a"."lastName", "a"."middleName", "a"."firstName", "a"."userId", "a"."ExtEmpNo" from "${process.env.sapDatabase}"."OHEM" "a"`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  employeeInfosSelectAllRevive: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."empID", "a"."lastName", "a"."middleName", "a"."firstName", "a"."userId", "a"."ExtEmpNo" from "${process.env.reviveDB}"."OHEM" "a"`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  usersSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."Code" as userCode,"a"."U_EMAIL" as email, "a"."U_APP_EMP_ID", "a"."U_TS_ROLE_ID","a"."U_TS_CONTACT", "a"."U_TS_STATUS" as userStatus, "a"."U_TS_EMP_ID",
      "b"."Code" as RoleCode, "b"."U_TS_NAME", "b"."U_TS_STATUS" as roleStatus, "c"."firstName", "c"."lastName", "d"."firstName" as "firstName_revive", "d"."lastName" as "lastName_revive" 
      from "${process.env.DB}"."@BFI_TS_USERS" "a"
      left join "${process.env.DB}"."@BFI_TS_ROLE" "b" on "a"."U_TS_ROLE_ID" = "b"."Code"
      left join "${process.env.sapDatabase}"."OHEM" "c" on "a"."U_APP_EMP_ID" = "c"."empID"
      left join "${process.env.reviveDB}"."OHEM" "d" on "a"."U_APP_EMP_ID" = "d"."empID";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
    usersSelectAllRevive: async ({ info }) => {
        try {
         const client = await hdbClient();
    
         const sql = `select "a"."Code" as userCode,"a"."U_EMAIL" as email, "a"."U_APP_EMP_ID", "a"."U_TS_ROLE_ID","a"."U_TS_CONTACT", "a"."U_TS_STATUS" as userStatus, "a"."U_TS_EMP_ID",
          "b"."Code" as RoleCode, "b"."U_TS_NAME", "b"."U_TS_STATUS" as roleStatus, "c"."firstName", "c"."lastName"
          from "${process.env.DB}"."@BFI_TS_USERS" "a"
          left join "${process.env.DB}"."@BFI_TS_ROLE" "b" on "a"."U_TS_ROLE_ID" = "b"."Code"
          left join "${process.env.reviveDB}"."OHEM" "c" on "a"."U_APP_EMP_ID" = "c"."empID";`;
          const result = await new Promise((resolve) => {
            client.connect(function (err) {
              if (err) {
                return console.error("Connect error", err);
              }
              client.exec(sql, function (err, rows) {
                resolve(rows);
                client.end();
              });
            });
          });
    
          return result;
        } catch (e) {
          console.log("Error: ", e);
        }
      },

  usersSelectOne: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."Code" as userCode, "a"."U_APP_EMP_ID", "a"."U_TS_ROLE_ID","a"."U_TS_CONTACT", "a"."U_TS_STATUS" as userStatus, "a"."U_TS_EMP_ID", "a"."U_TS_IS_DIRECTUSER",
      "b"."Code" as RoleCode, "b"."U_TS_NAME", "b"."U_TS_STATUS" as roleStatus, "c"."firstName", "c"."lastName", "a"."U_EMAIL", "d"."firstName" as "firstNameRevive", "d"."lastName" as "lastNameRevive"
      from "${process.env.DB}"."@BFI_TS_USERS" "a"
      left join "${process.env.DB}"."@BFI_TS_ROLE" "b" on "a"."U_TS_ROLE_ID" = "b"."Code"
      left join "${process.env.sapDatabase}"."OHEM" "c" on "a"."U_APP_EMP_ID" = "c"."empID"
      left join "${process.env.reviveDB}"."OHEM" "d" on "a"."U_APP_EMP_ID" = "d"."empID"
      where "a"."Code" = ${info.id}
      `;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // usersSelectAll: async ({ info }) => {
  //   try {
  //     const cookie = info.cookie; // store cookie
  //     delete info.cookie; // remove cookie

  //     let data = [];

  //     const request = async link => {
  //       let res;

  //       res = await axios({
  //         method: "GET",
  //         url: `${url}/${link}`,
  //         headers: {
  //           "Content-Type": "application/json",
  //           Cookie: `${cookie};`
  //         },
  //         httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //       });

  //       const arr = res.data.value;
  //       for await (let i of arr) {
  //         data.push(i);
  //       }

  //       // loop always if there is next link
  //       while (res.data["odata.nextLink"]) {
  //         const nextPage = res.data["odata.nextLink"];
  //         await request(nextPage);
  //         break;
  //       }
  //     };

  //     await request(
  //       `$crossjoin(U_BFI_TS_USERS,U_BFI_TS_ROLE,EmployeesInfo)?$expand=U_BFI_TS_USERS($select=Code,U_APP_EMP_ID,U_TS_ROLE_ID,U_TS_CONTACT,U_TS_STATUS,U_TS_EMP_ID),U_BFI_TS_ROLE($select=Code,U_TS_NAME,U_TS_STATUS),EmployeesInfo($select=LastName,FirstName,EmployeeID,eMail)&$filter=U_BFI_TS_USERS/U_TS_ROLE_ID eq U_BFI_TS_ROLE/Code and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID`
  //     );

  //     return data;
  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // },

  usersAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_USERS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  usersAddSelectById: async ({ info }) => {
    try {

      const cookie = info.cookie; // store cookie
      const { U_TS_IS_BIOTECH, U_TS_IS_REVIVE } = info; // deconstruct

      // declare boolean
      const isBfi = U_TS_IS_BIOTECH == 1 ? true : false;
      const isRevive = U_TS_IS_REVIVE == 1 ? true : false;

      let queryUrl = null;
      if (isBfi) {
        queryUrl = `${url}/U_BFI_TS_USERS?$filter=U_APP_EMP_ID eq '${info.U_APP_EMP_ID}' and U_TS_IS_BIOTECH eq '1'`;
      }

      if (isRevive) {
        queryUrl = `${url}/U_BFI_TS_USERS?$filter=U_APP_EMP_ID eq '${info.U_APP_EMP_ID}' and U_TS_IS_REVIVE eq '1'`;
      }

      const res = await axios({
        method: "GET",
        url: `${queryUrl}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  usersAddSelectByEmpId: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_USERS?$filter=U_TS_EMP_ID eq '${info.U_TS_EMP_ID}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  usersAddSelectByEmail: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_USERS?$filter=U_TS_EMAIL eq '${info.U_TS_EMAIL}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  usersAddSelectByName: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_USERS?$filter=U_TS_FN eq '${info.U_TS_FN}' and U_TS_LN eq '${info.U_TS_LN}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  usersGetMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_USERS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // hard coded login for not SAP login
  userLogin: async () => {
    try {
      const data = {
        CompanyDB: process.env.DB,
        Password: process.env.PW,
        UserName: process.env.USERS,
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      return res.data;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  dbLogin: async ({ info }) => {
    try {
      const data = {
        CompanyDB: info.db,
        Password: info.password,
        UserName: info.username,
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      return res.data;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // login input SAP credentials
  userSoftLogin: async ({ info }) => {
    try {
      const { username, password } = info;

      const data = {
        CompanyDB: process.env.DB,
        Password: password,
        UserName: username,
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      return res.data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  //add user on SAP
  userAddUserCode: async ({ info }) => {
    try {
      const cookie = info.cookie;
      delete info.cookie
      const { UserCode } = info;

      const res = await axios({
        method: "POST",
        url: `${url}/Users`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        data: {
          UserCode: UserCode
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // add user information on SAP
  userAddUserSap: async ({ info }) => {
    try {
      const cookie = info.cookie;
      delete info.cookie;
      const { FirstName, LastName, ExternalEmployeeNumber} = info;

      const res = await axios({
        method: "POST",
        url: `${url}/EmployeesInfo`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        data: {
          FirstName: FirstName,
          LastName: LastName,
          ExternalEmployeeNumber: ExternalEmployeeNumber
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  //validation for existing user
  getSapUserFullName: async ({ info }) => {
    try {
      const { Company , FirstName , LastName} = info
      const client = await hdbClient();
      const bfi = process.env.sapDatabase;
      const rci = process.env.reviveDB;
      let sql;
        if(Company == "biotech"){
          sql = `SELECT * FROM "${bfi}"."OHEM"
          WHERE "firstName"='${FirstName}' AND "lastName"= '${LastName}'`;
        }

        if(Company == "revive"){
          sql = `SELECT * FROM "${rci}"."OHEM"
          WHERE "firstName"='${FirstName}' AND "lastName"= '${LastName}'`;
        }
      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getSapUserCode: async ({ info }) => {
    try {
      const { Company , ExternalEmployeeNumber} = info
      const client = await hdbClient();
      const bfi = process.env.sapDatabase;
      const rci = process.env.reviveDB;
      let sql;
        if(Company == "biotech"){
          sql = `SELECT * FROM "${bfi}"."OHEM"
          WHERE "ExtEmpNo"='${ExternalEmployeeNumber}'`;
        }

        if(Company == "revive"){
          sql = `SELECT * FROM "${rci}"."OHEM"
          WHERE "ExtEmpNo"='${ExternalEmployeeNumber}'`;
        }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // user login credentials confirm
  userSelectCredentials: async ({ info }) => {
    try {
      const cookie = `B1SESSION=${info.cookie}`; // store cookie
      delete info.cookie; // remove cookie
      delete info.mode;
      delete info.source;
      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(U_BFI_TS_USERS,U_BFI_TS_ROLE)?$expand=U_BFI_TS_USERS($select=Code,U_TS_LOGIN_COUNT,U_TS_IS_DIRECTUSER,U_APP_EMP_ID,U_TS_STATUS,U_TS_EMP_ID),U_BFI_TS_ROLE($select=Code,U_TS_NAME,U_TS_STATUS)&$filter=U_BFI_TS_USERS/U_TS_EMP_ID eq '${info.employee_id}' and U_BFI_TS_USERS/U_TS_PW eq '${info.password}' and U_BFI_TS_USERS/U_TS_ROLE_ID eq U_BFI_TS_ROLE/Code`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  userSelectTruckScale: async ({ info }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT a."Code", b."U_LOCATIONS", a."U_TS_ID"
      FROM "${db}"."@BFI_TS_TRSCALE" a LEFT JOIN "${db}"."@BFI_TS_LOCATIONS" b
      ON b."Code" = a."U_LOCATION_ID" WHERE a."U_TS_TR_IP" = '${info.ip}' 
      AND LOWER(a."U_TS_DEVICE") = '${info.device_name.toLowerCase()}';`;


      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  userGetLoginCount: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_USERS?$select=U_TS_LOGIN_COUNT&$filter=Code eq '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  // select all activity logs
  selectLogs: async ({ info }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const sql = `SELECT * FROM "${db}"."@BFI_TS_LOGS"
      WHERE CAST("U_TS_DATELOG" AS DATE) BETWEEN '${info.from}' AND '${info.to}'
      ORDER BY CAST("Code" AS INTEGER) DESC;`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
};

module.exports = { users };
