const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";
const { hdbClient } = require("../../hdb/app");
// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const truckscale = {
  truckscaleUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      delete info.modules;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_TRSCALE('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  truckscaleSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRSCALE?$filter=Code eq '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  truckscaleSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT a."Code",a."U_TS_TRSCALE",b."U_LOCATIONS",a."U_TS_TR_LENGTH",a."U_TS_CREATEDATE",a."U_TS_CREATETIME",
      a."U_TS_TR_IP",a."U_TS_DEVICE",a."U_TS_BAUDRATE" FROM 
      "${db}"."@BFI_TS_TRSCALE" a LEFT JOIN "${db}"."@BFI_TS_LOCATIONS" b
      ON b."Code" = a."U_LOCATION_ID";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  truckscaleAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_TRSCALE`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  truckscaleGetMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_TRSCALE";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  truckscaleAddSelectByIPAndDeviceName: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRSCALE?$filter=U_TS_TR_IP eq '${info.U_TS_TR_IP}' and U_TS_DEVICE eq '${info.U_TS_DEVICE}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  getLocations: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code","U_LOCATIONS","U_DETAILED_LOCATION" FROM "${db}"."@BFI_TS_LOCATIONS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  displayLocations: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."Code",a."U_TS_CREATEDATE",a."U_TS_CREATETIME",a."U_LOCATIONS",a."U_DETAILED_LOCATION", a."U_TS_STATUS",
      (
      SELECT CONCAT("firstName",CONCAT(' ',"lastName")) FROM "${bfi}"."OHEM" WHERE "empID" = a."U_TS_CREATEDBY"
      ) AS created_by,
      (
      SELECT CONCAT("firstName",CONCAT(' ',"lastName")) FROM "${bfi}"."OHEM" WHERE "empID" = a."U_TS_MODIFYBY"
      ) AS modified_by
      FROM "${db}"."@BFI_TS_LOCATIONS" a;`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  locationMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_LOCATIONS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  locationCheckExist: async (loc) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@BFI_TS_LOCATIONS" WHERE LOWER("U_LOCATIONS") = LOWER('${loc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  locationAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_LOCATIONS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  locationUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      delete info.modules;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_LOCATIONS('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  locationCheckExistUpdate: async (loc, id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@BFI_TS_LOCATIONS" WHERE LOWER("U_LOCATIONS") = LOWER('${loc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLocationCheckExist: async (loc) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@BFI_TS_PRINT_LOCS" WHERE LOWER("U_PRINT_LOC") = LOWER('${loc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLocationMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_PRINT_LOCS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLocationAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_PRINT_LOCS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  printLocationFetch: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."Code",a."U_PRINT_LOC",b."U_LOCATIONS",a."U_TS_CREATEDATE",a."U_TS_CREATETIME",a."U_LOCATION_ID", a.U_TS_STATUS,
      (
      SELECT CONCAT("firstName",CONCAT(' ',"lastName")) FROM "${bfi}"."OHEM" WHERE "empID" = a."U_TS_CREATEDBY"
      ) AS created_by
      FROM "${db}"."@BFI_TS_PRINT_LOCS" a LEFT JOIN "${db}"."@BFI_TS_LOCATIONS" b
      ON b."Code" = a."U_LOCATION_ID";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLocationCheckExistUpdate: async (loc, id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@BFI_TS_PRINT_LOCS" WHERE LOWER("U_PRINT_LOC") = LOWER('${loc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLocationUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      delete info.modules;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_PRINT_LOCS('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
};

module.exports = { truckscale };
