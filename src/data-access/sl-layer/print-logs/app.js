const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const printLogs = {
  printLogsMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
          AS "maxCode" FROM "${db}"."@BFI_TS_PRNTLOGS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  printLogsAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_PRNTLOGS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  printCounts: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE("U_PRINT_COUNT", 0) AS print_limit,
      (
      SELECT COUNT(*) FROM "${db}"."@BFI_TS_PRNTLOGS" WHERE "U_TS_TRNSCTION_ID" = ${id}
      ) AS printed_documents
      FROM "${db}"."@BFI_TS_TRNS" WHERE "DocEntry" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getPrintLocations: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT d."U_PRINT_LOC", d."U_TS_STATUS"
      FROM "${db}"."@BFI_TS_IBTRN" a LEFT JOIN
      "${db}"."@BFI_TS_TRSCALE" b ON b."Code" = a."U_TS_TRKSCL_ID" LEFT JOIN
      "${db}"."@BFI_TS_LOCATIONS" c ON c."Code" = b."U_LOCATION_ID" LEFT JOIN
      "${db}"."@BFI_TS_PRINT_LOCS" d ON d."U_LOCATION_ID" = c."Code"
      WHERE a."DocEntry" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  batchRequest: async (info) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.batchStr, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  selectPrintLogs: async (from, to) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT a."Code",a."U_TS_TRNSCTION_ID",a."U_TS_CREATEDATE",a."U_TS_CREATETIME",b."U_TS_EMP_ID",
      c."lastName",c."firstName",e."U_TS_TRNSTYPE",d."U_COMPANY", d."U_TS_TICKETNUMBER"
      FROM "${db}"."@BFI_TS_PRNTLOGS" a LEFT JOIN "${db}"."@BFI_TS_USERS" b
      ON b."Code" = a."U_TS_EMPLOYEE_ID" LEFT JOIN "EUT_BFI_20201217"."OHEM" c
      ON c."empID" = b."U_APP_EMP_ID" LEFT JOIN "${db}"."@BFI_TS_TRNS" d ON d."DocEntry" = a."U_TS_TRNSCTION_ID"
      LEFT JOIN "${db}"."@BFI_TS_TRNSTYPE" e ON d."U_TS_TRNS_TYPE" = e."Code"
      WHERE a."U_TS_CREATEDATE" BETWEEN '${from}' AND '${to}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  detailedAddress: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT c."U_DETAILED_LOCATION" FROM 
      "${db}"."@BFI_TS_IBTRN" a LEFT JOIN "${db}"."@BFI_TS_TRSCALE" b
      ON b."Code" = a."U_TS_TRKSCL_ID" LEFT JOIN "${db}"."@BFI_TS_LOCATIONS" c ON
      c."Code" = b."U_LOCATION_ID"
      WHERE a."DocEntry" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
};

module.exports = { printLogs };
