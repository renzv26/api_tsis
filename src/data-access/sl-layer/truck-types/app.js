const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";
const { hdbClient } = require("../../hdb/app");
// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const truckTypes = {

  truckTypesUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_TRTYPE('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },


  truckTypesSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRTYPE?$filter=Code eq '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },



  truckTypesSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@BFI_TS_TRTYPE";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },


  truckTypesAdd: async ({ info }) => {
    try {

      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_TRTYPE`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckTypesAddSelectByName: async ({ info }) => {
    try {

      const cookie = info.cookie; // store cookie


      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRTYPE?$filter=U_TS_TRMODEL eq '${info.U_TS_TRMODEL}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckTypesGetMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_TRTYPE";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

};

module.exports = { truckTypes };
