const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const { hdbClient } = require("../../hdb/app");

// ############
// base url for sap
const url = process.env.SAP_URL;
// base url for xsjs
const xsjs = process.env.XSJS_URL;

const accessRights = {
  batchRequest: async (info) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.batchString, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  arDelete: async (info) => {
    try {
      const cookie = info.cookie; // store cookie
      const res = await axios({
        method: "DELETE",
        url: `${url}/U_BFI_TS_ACCRGHT('${info.id}')`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  arSelectAllCode: async ({ info }) => {
    try {
      const cookie = info.cookie;
      let data = [];
      const request = async (link) => {
        let res;
        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });
        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");
            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };
      await request(
        `U_BFI_TS_ACCRGHT?$select=Code&$filter=U_TS_ROLE_ID eq '${info.id}'`
      );
      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  arGetMaxCode: async ({}) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_ACCRGHT";`;
      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  arAdd: async (info) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_ACCRGHT`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  arSelectOnRole: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      // declare empty array; to store every after pagination
      let data = [];
      // recursive function; request always if there is pagination link
      const request = async (link) => {
        let res;
        // main request
        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });
        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }
        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };
      await request(
        `$crossjoin(U_BFI_TS_MOD,U_BFI_TS_ACT,U_BFI_TS_ROLE,U_BFI_TS_ACCRGHT)?$expand=U_BFI_TS_MOD($select=Code,U_TS_DESC,U_TS_STATUS),U_BFI_TS_ACT($select=Code,U_TS_MOD_ID,U_TS_DESC,U_TS_STATUS)&$filter=U_BFI_TS_ROLE/Code eq '${info.id}' and U_BFI_TS_MOD/Code eq U_BFI_TS_ACT/U_TS_MOD_ID and U_BFI_TS_ACT/Code eq U_BFI_TS_ACCRGHT/U_TS_ACTION_ID and U_BFI_TS_ROLE/Code eq U_BFI_TS_ACCRGHT/U_TS_ROLE_ID&$orderby=U_BFI_TS_ACT/Code`
      );
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
};

module.exports = { accessRights };
