const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();

const { hdbClient } = require("../../hdb/app");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const transactions = {
  getUnfinishedRequests: async () => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS request_count 
      FROM "${db}"."@BFI_TS_REQUESTS" 
      WHERE "U_STATUS" = 'pending' OR "U_STATUS" = 'validated';`

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  requestMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_REQUESTS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  requestNotifMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_REQ_NOTIF";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  ifAccounting: async (info) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT u.* , r."U_TS_NAME" as "role" FROM  "${db}"."@BFI_TS_USERS" u
                   JOIN  "${db}"."@BFI_TS_ROLE" r ON u."U_TS_ROLE_ID"=r."Code"  
                   WHERE u."Code"=${info}`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getUserReceiverNotif: async (info) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT a."Code", a."U_EMAIL" FROM "${db}"."@BFI_TS_USERS" a
      LEFT JOIN "${db}"."@BFI_TS_ROLE" b ON b."Code" = a."U_TS_ROLE_ID"
      WHERE (LOWER(b."U_TS_NAME") = 'controller' AND LOWER(a."U_TS_STATUS") = 'active' 
      OR LOWER(b."U_TS_NAME") = 'supervisor' AND LOWER(a."U_TS_STATUS") = 'active') AND (a."Code" != '${info}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      if(info){
        const sql2 = `select "Code", "U_EMAIL" FROM  "${db}"."@BFI_TS_USERS" WHERE "Code"=${info}`

      const result2 = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql2, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      result.push(result2[0])

      }
      
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  checkIfExistReqNotif: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS counts FROM "${db}"."@BFI_TS_REQ_NOTIF" WHERE "U_REQUEST_ID" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },


  addPriceApprovalRequest: async ({ info }) => {

    try {

      const res = await axios({
        method: "POST",
        url: `${process.env.mdm_url}/mdm-api/pa-transactions/add_tsis`,
        data: {
          ...info,
        },
      })

      return { status: res.status, msg: res.data.posted.msg }

    } catch (e) {
      return { status: e.response.status, msg: e.response.data.error }

    }

  },

  selectOneDraft: async ({ info }) => {
    try {


      const cookie = info.cookie;

      const id = info.id

      const res = await axios({
        method: "GET",
        url: `${url}/Drafts?$filter = U_FSQRTransID eq '${id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };


      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  updateDraft: async ({ bfiPO }) => {
    try {
      
      const cookie = bfiPO.cookie;
      delete bfiPO.cookie
      const id = bfiPO.id
      delete bfiPO.id

      const res = await axios({
        method: "PATCH",
        url: `${url}/Drafts(${id})`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        data: {
          ...bfiPO
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  deleteDraft: async ({ info }) => {
    try {
      
      const cookie = info.cookie;
      delete info.cookie
      const id = info.id
      delete info.id

      const res = await axios({
        method: "DELETE",
        url: `${url}/Drafts(${id})`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  postPO: async ({ info }) => {
    try {
      const cookie = info.cookie;
      delete info.cookie
      delete info.DocEntry
      const res = await axios({
        method: "POST",
        url: `${url}/PurchaseOrders`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        data: {
          ...info
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  saveDraftToDocument: async ({ info }) => {
    try {

      const cookie = info.cookie;
      delete info.cookie
      const res = await axios({
        method: "POST",
        url: `${url}/DraftsService_SaveDraftToDocument`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        data: {
          ...info
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      console.log('res', res);
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },




  selectAllWarehouses: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(`Warehouses?$select=WarehouseCode,WarehouseName`);

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  getEmployeeName: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const bfi = process.env.sapDatabase;
      const rci = process.env.reviveDB;

      const sql = `SELECT b."firstName",b."lastName", c."firstName" as "firstName_revive", c."lastName" as "lastName_revive"
      FROM "${db}"."@BFI_TS_USERS" a 
      LEFT JOIN "${bfi}"."OHEM" b ON b."empID" = a."U_APP_EMP_ID"
      LEFT JOIN "${rci}"."OHEM" c ON c."empID" = a."U_APP_EMP_ID" WHERE a."Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectFsqrTransactionBySupplierCode: async (id) => {
    try {
      const client = await hdbClient();


      const sql = `
      select * from "${process.env.FSQR_DB}"."@FSQR_TRNS" where "U_suppl_code" = '${id}'
      `;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

//   selectTransactionBySupplierCode: async (id) => {
//     try {
//       const client = await hdbClient();


//       const sql = `
//       select * from "${process.env.DB}"."@BFI_TS_TRNS" where "U_TS_TRCK_CODE" = '${id}'
//       `;
// console.log('sql', sql);
//       const result = await new Promise((resolve) => {
//         client.connect(function (err) {
//           if (err) {
//             return console.error("Connect error", err);
//           }
//           client.exec(sql, function (err, rows) {
//             resolve(rows);
//             client.end();
//           });
//         });
//       });
//       return result;
//     } catch (e) {
//       console.log("Error: ", e);
//     }
//   },

  selectDraftByFsqrTransactionId: async (id) => {
    try {
      const client = await hdbClient();


      // const sql = `
      // select * from "${process.env.sapDatabase}"."ODRF" where "U_FSQRTransID" = ${id}
      // `;

      const sql = `
      
 select "a"."DocEntry", "a"."CardName", "a"."Address", 
 (
 select
    string_agg("Dscription",
    ', ') 
   from "${process.env.sapDatabase}"."DRF1" "b" 
   where "b"."DocEntry" = "a"."DocEntry" 
   group by "b"."DocEntry" 
 )as item_list from "${process.env.sapDatabase}"."ODRF" "a"
 where "a"."U_FSQRTransID" = '${id}'
      `

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getCurrentNotifs: async (id, date) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const bfi = process.env.sapDatabase;
      const rci = process.env.reviveDB;

      const sql = `SELECT  a."Code", a."U_REQUEST_ID", a."U_RECEIVER_ID", b."U_STATUS",
      a."U_CREATEDATE", a."U_UPDATEDATE", a."U_UPDATETIME", a."U_CREATETIME", a."U_IS_READ", a."U_SENDER_ID",     
      (
      SELECT y."firstName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${bfi}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_CREATEDBY"
      ) AS fn,
      (
      SELECT y."lastName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${bfi}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_CREATEDBY"
      ) AS ln,
      (
      SELECT y."firstName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${bfi}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_MODIFYBY"
      ) AS fn_mod,
      (
      SELECT y."lastName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${bfi}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_MODIFYBY"
      ) AS ln_mod,
      (
      SELECT y."firstName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${rci}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_CREATEDBY"
      ) AS fn_r,
      (
      SELECT y."lastName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${rci}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_CREATEDBY"
      ) AS ln_r,
      (
      SELECT y."firstName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${rci}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_MODIFYBY"
      ) AS fn_r_mod,
      (
      SELECT y."lastName"
      FROM "${db}"."@BFI_TS_USERS" x LEFT JOIN "${rci}"."OHEM" y
      ON y."empID" = x."U_APP_EMP_ID" WHERE x."Code" = a."U_MODIFYBY"
      ) AS ln_r_mod,
      b."U_TRANSCTION_ID", b."U_TS_TICKETNUMBER", a."U_CREATEDBY", a."U_MODIFYBY"
      FROM "${db}"."@BFI_TS_REQ_NOTIF" a LEFT JOIN "${db}"."@BFI_TS_REQUESTS" b
      ON b."Code" = a."U_REQUEST_ID" WHERE a."U_CREATEDATE" = '${date}' 
      AND (a."U_RECEIVER_ID" = '${id}')
      ORDER BY CAST(a."Code" AS INTEGER) DESC;`;

     


      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getNotifsId: async (id, date) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@BFI_TS_REQ_NOTIF" WHERE "U_CREATEDATE" = '${date}' AND "U_RECEIVER_ID" = '${id}'
      AND "U_IS_READ" = '0';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  selectAllPurchaseOrdersTaggedInTransactionTable: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(
        `BFITSUDO001?$select=U_APP_PO_ID&$filter=U_APP_PO_ID ne null`
      );

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  selectAllPurchaseOrders: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let { from, to } = info;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(
        `PurchaseOrders?$select=DocEntry,DocDate&$filter=DocDate ge '${from}' and DocDate le '${to}'`
      );

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsSelectAllTransmittalTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_STATUS eq 'for transmittal' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsSelectAllWithPoTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and U_BFI_TS_TRNSTYPE/U_TS_TRNSTYPE ne 'others' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  getTransactionsNotCompleted: async ({ info }) => {
    try {
      const { from, to } = info;

      const client = await hdbClient();
      const tsis = process.env.DB;
      const bfi = process.env.sapDatabase;

      const sql = `
      SELECT DISTINCT a."DocEntry", a."U_TS_SUPPLIER_ADD", a."CreateDate",a."CreateTime",a."U_TS_TRCK_CODE",a."U_TS_TICKETNUMBER",b."U_TS_TRNSTYPE", a."U_UOM_TS",d."U_TS_PLATE_NUM",a."U_TS_STATUS",a."U_COMPANY",
      COALESCE(a."U_TS_SUPPLIER",
      CASE 
      WHEN a."U_APP_PO_ID" IS NULL AND a."U_APP_SO_ID" IS NULL THEN NULL
      WHEN a."U_APP_PO_ID" IS NOT NULL AND a."U_APP_SO_ID" IS NULL THEN (SELECT "CardName" FROM "${bfi}"."OPOR" WHERE "DocEntry" = a."U_APP_PO_ID")
      WHEN a."U_APP_SO_ID" IS NOT NULL AND a."U_APP_PO_ID" IS NULL THEN (SELECT "CardName" FROM "${bfi}"."ORDR" WHERE "DocEntry" = a."U_APP_SO_ID")
      END
      ) AS supplier,
      COALESCE(a."U_TS_ITEMNAME",
      CASE 
      WHEN a."U_APP_PO_ID" IS NULL AND a."U_APP_SO_ID" IS NULL THEN NULL
      WHEN a."U_APP_PO_ID" IS NOT NULL AND a."U_APP_SO_ID" IS NULL THEN (SELECT STRING_AGG("Dscription", ',') FROM "${bfi}"."POR1" WHERE "DocEntry" = a."U_APP_PO_ID")
      WHEN a."U_APP_SO_ID" IS NOT NULL AND a."U_APP_PO_ID" IS NULL THEN (SELECT STRING_AGG("Dscription", ',') FROM "${bfi}"."RDR1" WHERE "DocEntry" = a."U_APP_SO_ID")
      END
      ) AS items,
      COALESCE(a."U_UOM",
      CASE 
      WHEN a."U_APP_PO_ID" IS NULL AND a."U_APP_SO_ID" IS NULL THEN NULL
      WHEN a."U_APP_PO_ID" IS NOT NULL AND a."U_APP_SO_ID" IS NULL THEN (SELECT STRING_AGG("unitMsr", ',') FROM "${bfi}"."POR1" WHERE "DocEntry" = a."U_APP_PO_ID")
      WHEN a."U_APP_SO_ID" IS NOT NULL AND a."U_APP_PO_ID" IS NULL THEN (SELECT STRING_AGG("unitMsr", ',') FROM "${bfi}"."RDR1" WHERE "DocEntry" = a."U_APP_SO_ID")
      END
      ) AS uom, a."U_TS_NUM_BAGS", c."U_TS_IB_WEIGHT",c."U_TS_REMARKS", CONCAT(f."U_TS_LN",CONCAT(', ',f."U_TS_FN")) AS driver
      FROM "${tsis}"."@BFI_TS_TRNS" a
      LEFT JOIN "${tsis}"."@BFI_TS_TRNSTYPE" b ON b."Code" = a."U_TS_TRNS_TYPE"
      LEFT JOIN "${tsis}"."@BFI_TS_IBTRN" c ON c."DocEntry" = a."DocEntry"
      LEFT JOIN "${tsis}"."@BFI_TS_TRINFO" d ON d."Code" = c."U_TS_TRKINFO"
      LEFT JOIN "${tsis}"."@BFI_TS_OBTRN" e ON e."DocEntry" = a."DocEntry"
      LEFT JOIN "${tsis}"."@BFI_TS_DRIVERS" f ON f."Code" = c."U_TS_DRIVERID"
      WHERE LOWER(a."U_TS_STATUS") <> 'completed' AND a."CreateDate" BETWEEN '${from}' AND '${to}'
      ORDER BY a."DocEntry" DESC;
      `;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  transactionsSelectAllWithoutPoTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async (link) => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`,
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsUpdate: async (info) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/BFITSUDO001(${id})`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  selectOnePurchaseOrder: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `
      select "a"."DocEntry", "a"."CardName", "a"."Address", 
      (
      select
         string_agg("Dscription",
         ', ') 
        from "${process.env.sapDatabase}"."POR1" "b" 
        where "b"."DocEntry" = "a"."DocEntry" 
        group by "b"."DocEntry" 
      )as item_list from "${process.env.sapDatabase}"."OPOR" "a"
      where "a"."DocEntry" = ${info.id}`
        ;


      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  transactionsSelectOneWithPo: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and U_BFI_TS_TRNSTYPE/U_TS_TRNSTYPE ne 'others' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and and BFITSUDO001/DocEntry eq ${info.id}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  getDocEntryForSeriesNumber: async () => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      let sql = null;
        // select one
        sql = `select "DocEntry" + 1 as "new_id" FROM "${db}"."@BFI_TS_TRNS" ORDER BY "DocEntry" DESC LIMIT 1;`;
      

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  transactionsSelectOneForGrpo: async ({ info }) => {
    try {
      delete info.cookie; // remove cookie
      delete info.user;
      delete info.pw;
      delete info.source;

      const client = await hdbClient();

      const sql = `select
            "a"."DocEntry",
            "a"."CreateDate",
            "a"."CreateTime",
            "a"."U_TS_STATUS",
            "a"."U_TS_TRCK_CODE",
            "a"."U_TS_NUM_BAGS",
            "a"."U_UOM",
            "a"."U_TS_TICKETNUMBER",
            "b"."U_TS_IB_WEIGHT" as ib_weight,
            "b"."U_TS_UPDATEDATE" as ib_update_date,
            "b"."U_TS_UPDATETIME" as ib_update_time,
            (select
              "aa"."U_TS_EMP_ID" 
              from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
              left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
              where "aa"."Code" = "b"."U_UPDATED_BY" ) as id_weigher_ib,
              (select
              "aa"."U_TS_EMP_ID" 
              from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
              left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
              where "aa"."Code" = "c"."U_UPDATED_BY" ) as id_weigher_ob,
            (select
            "bb"."lastName" || ', ' || "bb"."firstName" 
            from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
            left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
            where "aa"."Code" = "b"."U_UPDATED_BY" ) as ib_weigher,
            "c"."U_TS_OB_WEIGHT" as ob_weight,
            "c"."U_TS_REMARKS" as ob_remarks,
            "c"."U_TS_UPDATEDATE" as ob_update_date,
            "c"."U_TS_UPDATETIME" as ob_update_time,
            "c"."U_TS_OB_SIGN" as ob_sign,
            (select
            "bb"."lastName" || ', ' || "bb"."firstName" 
            from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
            left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
            where "aa"."Code" = "c"."U_UPDATED_BY" ) as ob_weigher,
            "d"."U_TS_TRNSTYPE",
            ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
            "f"."U_TS_PLATE_NUM",
            "g"."U_TS_TRTYPE",
            "g"."U_TS_TRMODEL",
            "z"."CardName",
            "z"."Address",
            (select
            string_agg("Dscription",
            ', ') 
            from "${process.env.sapDatabase}"."POR1" 
            where "DocEntry" = "z"."DocEntry" 
            group by "DocEntry" ) as item_list,
            "a"."U_TS_NUM_BAGS",
            "c"."U_TS_OB_SIGN"
          from "${process.env.DB}"."@BFI_TS_TRNS" "a" 
          left join "${process.env.DB}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
          left join "${process.env.DB}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
          and "b"."LineId" = "c"."LineId" 
          left join "${process.env.DB}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
          left join "${process.env.DB}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
          left join "${process.env.DB}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
          left join "${process.env.DB}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code" 
          left join "${process.env.sapDatabase}"."OPOR" "z" on "a"."U_APP_PO_ID" = "z"."DocEntry" WHERE "a"."U_TS_TRCK_CODE" = '${info.code}'
          order by "a"."DocEntry" asc;`;


      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  transactionsSelectOneOthers: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,BFITSUDO001/BFI_TS_IBTRNCollection,BFITSUDO001/BFI_TS_OBTRNCollection)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_TRCK_CODE,U_TS_STATUS,CreateDate,CreateTime,U_TS_TRCK_CODE,U_TS_NUM_BAGS,U_TS_SUPPLIER,U_TS_SUPPLIER_ADD,U_TS_ITEMCODE,U_TS_ITEMNAME,U_UOM,U_COMPANY,U_TS_LOC_DLVRY,U_TS_TICKETNUMBER,U_UOM_TS,U_TS_DRNUMBER),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),BFITSUDO001/BFI_TS_IBTRNCollection($select=LineId,DocEntry,U_TS_IB_WEIGHT,U_TS_UPDATEDATE,U_TS_UPDATETIME,U_TS_REMARKS),BFITSUDO001/BFI_TS_OBTRNCollection($select=LineId,DocEntry,U_TS_OB_WEIGHT,U_TS_UPDATEDATE,U_TS_UPDATETIME,U_TS_OB_SIGN,U_TS_REMARKS)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and BFITSUDO001/DocEntry eq  BFITSUDO001/BFI_TS_IBTRNCollection/DocEntry and BFITSUDO001/DocEntry eq  BFITSUDO001/BFI_TS_OBTRNCollection/DocEntry and BFITSUDO001/DocEntry eq ${info.id}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsSelectOneByTrackingCode: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const res = await axios({
        method: "GET",
        url: `${url}/BFITSUDO001?$filter=U_TS_TRCK_CODE eq '${info.trackingCode}'&$orderby=DocNum desc`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/BFITSUDO001(${info.id})`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  selectTransactionBySupplierCode: async (info) => {
    try {
      
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      
      const res = await axios({
        method: "GET",
        url: `${url}/BFITSUDO001?$filter=U_TS_TRCK_CODE eq '${info.trckingCode}'&$orderby=DocNum desc`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsAdd: async (info) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/BFITSUDO001`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data,
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  transactionsSelectOneRaw: async (id) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."DocEntry",a."DocNum",a."CardName",a."Address",CAST(b."Quantity" AS INTEGER) qty,b."UomCode" unit,
      string_agg(b."Dscription",',') items
      FROM "${bfi}"."ORDR" a LEFT JOIN "${bfi}"."RDR1" b
      ON b."DocEntry" = a."DocEntry"
      WHERE a."DocStatus" = 'O' AND a."DocEntry" = ${id}
      GROUP BY a."DocEntry",a."DocNum",a."CardName",b."Quantity",b."UomCode",a."Address";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  getSalesOrderDetails: async (id) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."DocEntry",a."DocNum",a."CardName",a."Address",CAST(b."Quantity" AS INTEGER) qty,b."UomCode" unit,
      string_agg(b."Dscription",',') items
      FROM "${bfi}"."ORDR" a LEFT JOIN "${bfi}"."RDR1" b
      ON b."DocEntry" = a."DocEntry"
      WHERE a."DocStatus" = 'O' AND a."DocEntry" = ${id}
      GROUP BY a."DocEntry",a."DocNum",a."CardName",b."Quantity",b."UomCode",a."Address";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  getUoM: async () => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase;

      const sql = `SELECT "UomEntry","UomCode","UomName" FROM "${bfi}"."OUOM";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  getReportHeader: async ({ info }) => {
    try {
      const { from, to } = info;
      const client = await hdbClient();

      const tsis = process.env.DB;
      const bfi = process.env.sapDatabase;

      const sql = `select
      "a"."DocEntry",
      TO_DATE("a"."CreateDate") as CreateDate,
      "a"."CreateTime",
      "a"."U_TS_STATUS",
      "a"."U_TS_TRCK_CODE",
      "a"."U_COMPANY",
      "a"."U_TS_TICKETNUMBER",
      "a"."U_TS_DRNUMBER",
      "a"."U_TS_PLOT_CODE",
      "b"."U_TS_IB_WEIGHT" as ib_weight,
      "b"."U_TS_REMARKS" as ib_remarks,
      "b"."U_TS_UPDATEDATE" as ib_update_date,
      "b"."U_TS_UPDATETIME" as ib_update_time,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${tsis}"."@BFI_TS_USERS" "aa" 
      left join "${bfi}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "b"."U_TS_CREATEDBY" ) as ib_weigher,
      "c"."U_TS_OB_WEIGHT" as ob_weight,
      "c"."U_TS_REMARKS" as ob_remarks,
      "c"."U_TS_UPDATEDATE" as ob_update_date,
      "c"."U_TS_UPDATETIME" as ob_update_time,
      "c"."U_TS_OB_SIGN" as ob_sign,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${tsis}"."@BFI_TS_USERS" "aa" 
      left join "${bfi}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "c"."U_UPDATED_BY" ) as ob_weigher,
      "d"."U_TS_TRNSTYPE",
      ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
      "f"."U_TS_PLATE_NUM",
      "g"."U_TS_TRTYPE",
      "g"."U_TS_TRMODEL",
      "a"."U_TS_NUM_BAGS",
      "a"."U_TS_IS_NAPIER",
      "a"."U_APP_PO_ID","a"."U_APP_SO_ID",
      "a"."U_TS_SUPPLIER","a"."U_TS_SUPPLIER_ADD","a"."U_TS_ITEMCODE","a"."U_TS_ITEMNAME","a"."U_UOM", "a"."U_UOM_TS"
      from "${tsis}"."@BFI_TS_TRNS" "a" 
      left join "${tsis}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
      left join "${tsis}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
      and "b"."LineId" = "c"."LineId" 
      left join "${tsis}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
      left join "${tsis}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
      left join "${tsis}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
      left join "${tsis}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code"
      WHERE TO_DATE("a"."CreateDate") BETWEEN '${from}' AND '${to}'
      order by "a"."DocEntry" DESC;`;
      

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getReportHeaderByTransId: async ( info ) => {
    try {
      // const { from, to } = info;
      const client = await hdbClient();

      const tsis = process.env.DB;
      const bfi = process.env.sapDatabase;

      const sql = `select
      "a"."DocEntry",
      "a"."CreateDate",
      "a"."CreateTime",
      "a"."U_TS_STATUS",
      "a"."U_TS_TRCK_CODE",
      "a"."U_TS_TICKETNUMBER",
      "a"."U_TS_DRNUMBER",
      "a"."U_UOM_TS",
      "a"."U_TS_PLOT_CODE",
      "a"."U_COMPANY",
      "b"."U_TS_IB_WEIGHT" as ib_weight,
      "b"."U_TS_REMARKS" as ib_remarks,
      "b"."U_TS_UPDATEDATE" as ib_update_date,
      "b"."U_TS_UPDATETIME" as ib_update_time,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${tsis}"."@BFI_TS_USERS" "aa" 
      left join "${bfi}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "b"."U_TS_CREATEDBY" ) as ib_weigher,
      "c"."U_TS_OB_WEIGHT" as ob_weight,
      "c"."U_TS_REMARKS" as ob_remarks,
      "c"."U_TS_UPDATEDATE" as ob_update_date,
      "c"."U_TS_UPDATETIME" as ob_update_time,
      "c"."U_TS_OB_SIGN" as ob_sign,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${tsis}"."@BFI_TS_USERS" "aa" 
      left join "${bfi}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "c"."U_TS_CREATEDBY" ) as ob_weigher,
      "d"."U_TS_TRNSTYPE",
      ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
      "f"."U_TS_PLATE_NUM",
      "g"."U_TS_TRTYPE",
      "g"."U_TS_TRMODEL",
      "a"."U_TS_NUM_BAGS",
      "a"."U_TS_IS_NAPIER",
      "a"."U_APP_PO_ID","a"."U_APP_SO_ID",
      "a"."U_TS_SUPPLIER","a"."U_TS_SUPPLIER_ADD","a"."U_TS_ITEMCODE","a"."U_TS_ITEMNAME","a"."U_UOM"
      from "${tsis}"."@BFI_TS_TRNS" "a" 
      left join "${tsis}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
      left join "${tsis}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
      and "b"."LineId" = "c"."LineId" 
      left join "${tsis}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
      left join "${tsis}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
      left join "${tsis}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
      left join "${tsis}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code"
      WHERE "a"."DocEntry" = ${ info };`;
      

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  getPOdetails: async (id) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."CardName",a."Address",b."Quantity",b."unitMsr",
      b."ItemCode",b."Dscription"
      FROM
      "${bfi}"."OPOR" a LEFT JOIN "${bfi}"."POR1" b
      ON b."DocEntry" = a."DocEntry" WHERE a."DocEntry" = ${id};`;
      

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  getSOdetails: async (id) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase;

      const sql = `SELECT a."CardName",a."Address",b."Quantity",b."unitMsr",
      b."ItemCode",b."Dscription"
      FROM
      "${bfi}"."ORDR" a LEFT JOIN "${bfi}"."RDR1" b
      ON b."DocEntry" = a."DocEntry" WHERE a."DocEntry" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectRequests: async ({ info }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;
      const bfi = process.env.sapDatabase;
      const rci = process.env.reviveDB;
      const { from, to, id } = info;
      let sql = null;
      if (!id) {
        // select all
        sql = `SELECT a.*,(
          SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) name
          FROM "${db}"."@BFI_TS_USERS" b LEFT JOIN "${bfi}"."OHEM" c
          ON c."empID" = b."U_APP_EMP_ID" WHERE b."Code" = a."U_CREATED_BY"
          ) AS requester_name,(
          SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) name
          FROM "${db}"."@BFI_TS_USERS" b LEFT JOIN "${rci}"."OHEM" c
          ON c."empID" = b."U_APP_EMP_ID" WHERE b."Code" = a."U_CREATED_BY"
          ) AS requester_name_rci,(
          SELECT "U_PRINT_COUNT" FROM "${db}"."@BFI_TS_TRNS" WHERE "DocEntry" = a."U_TRANSCTION_ID"
          ) current_count
          FROM "${db}"."@BFI_TS_REQUESTS" a WHERE a."U_CREATEDATE" BETWEEN '${from}' AND '${to}';`;
      } else {
        // select one
        sql = `SELECT a.*,(
          SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) name
          FROM "${db}"."@BFI_TS_USERS" b LEFT JOIN "${bfi}"."OHEM" c
          ON c."empID" = b."U_APP_EMP_ID" WHERE b."Code" = a."U_CREATED_BY"
          ) AS requester_name,(
          SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) name
          FROM "${db}"."@BFI_TS_USERS" b LEFT JOIN "${rci}"."OHEM" c
          ON c."empID" = b."U_APP_EMP_ID" WHERE b."Code" = a."U_CREATED_BY"
          ) AS requester_name_rci,(
          SELECT "U_PRINT_COUNT" FROM "${db}"."@BFI_TS_TRNS" WHERE "DocEntry" = a."U_TRANSCTION_ID"
          ) current_count
          FROM "${db}"."@BFI_TS_REQUESTS" a WHERE a."Code" = '${id}';`;
      }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  requestUpdate: async (info) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_REQUESTS('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  selectNotifOne: async (data) => {
    try {
      const client = await hdbClient();
      const db = process.env.DB;

      const sql = `select * from "${db}"."@BFI_TS_REQUESTS" where "Code"='${data.id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectNotifOneInNotif: async (data) => {
    try {
      const client = await hdbClient();
      const db = process.env.DB;

      // const sql = `select * from "${db}"."@BFI_TS_REQ_NOTIF" 
      //               where "U_REQUEST_ID"='${data.Code}'
      //               and "U_RECEIVER_ID"='${data.U_CREATED_BY}'`;

      const sql = `select * from "${db}"."@BFI_TS_REQ_NOTIF" 
                    where "U_REQUEST_ID"='${data.Code}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  requestNotifUpdate: async (info) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_REQ_NOTIF('${id}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },

  batchRequest: async (info) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.batchStr, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  checkIfApproved: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS counts FROM "${db}"."@BFI_TS_REQUESTS" WHERE "Code" = '${id}' AND "U_STATUS" = 'approved';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  reqCheckValidatePending: async (id) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS counts FROM "${db}"."@BFI_TS_REQUESTS" WHERE "U_TRANSCTION_ID" = '${id}'
      AND LOWER("U_STATUS") = 'pending' OR "U_TRANSCTION_ID" = '${id}' AND LOWER("U_STATUS") = 'validated';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
};

module.exports = { transactions };
