const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

const items = {
    insertItem: async (info) => {
        try {
            const cookie = info.cookie; // store cookie
            delete info.cookie; // remove cookie
      
            const res = await axios({
              method: "POST",
              url: `${url}/U_BFI_TS_ITEMS`,
              data: {
                ...info
              },
              headers: {
                "Content-Type": "application/json",
                Cookie: `${cookie};`
              },
              httpsAgent: new https.Agent({ rejectUnauthorized: false })
            });
      
            const req = {
              status: res.status,
              msg: res.statusText
            };
            return req;
          } catch (e) {
            const err = {
              status: e.response.status,
              msg: e.response.statusText,
              data: e.response.data
            };
            return err;
          }
      },

      selectAllItems: async () => {
        try {
          const client = await hdbClient();
    
          const db = process.env.DB;
    
          const sql = `SELECT "Code", "U_TS_ITEM_NAME" as "ItemDescription", "U_TS_IS_ACTIVE" as "Status" FROM "${db}"."@BFI_TS_ITEMS"`;
    
          const result = await new Promise(resolve => {
            client.connect(function (err) {
              if (err) {
                console.log("Connect error", err);
              }
              client.exec(sql, function (err, rows) {
                resolve(rows);
                client.end();
              });
            });
          });
          return result;
        } catch (e) {
          console.log("Error: ", e);
        }
      },

      itemsSelectByItemName: async ({ info }) => {
        try {
          const client = await hdbClient();
    
          const db = process.env.DB;

          const sql = `SELECT * FROM "${db}"."@BFI_TS_ITEMS" where "U_TS_ITEM_NAME"='${info.item}'`;

          const result = await new Promise(resolve => {
            client.connect(function (err) {
              if (err) {
                console.log("Connect error", err);
              }
              client.exec(sql, function (err, rows) {
                resolve(rows);
                client.end();
              });
            });
          });
          return result;
        } catch (e) {
          console.log("Error: ", e);
        }
      },

      itemsGetMaxCode: async ({ }) => {
        try {
          const client = await hdbClient();
    
          const db = process.env.DB;
    
          const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
          AS "maxCode" FROM "${db}"."@BFI_TS_ITEMS";`;
    
          const result = await new Promise(resolve => {
            client.connect(function (err) {
              if (err) {
                console.log("Connect error", err);
              }
              client.exec(sql, function (err, rows) {
                resolve(rows);
                client.end();
              });
            });
          });
          return result;
        } catch (e) {
          console.log("Error: ", e);
        }
      },
}

module.exports = { items };