const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const supppliers = {
  selectAllSuppliers: async ({}) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase; // bfi db

      const sql = `SELECT "CardCode","CardName","Address" 
      FROM "${bfi}"."OCRD";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  supplierGetMaxCode: async () => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_SUPPLIER";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  batchRequest: async info => {
    // console.log(info);

    try {
      const res = await axios.post(`${url}/$batch`, info.batchStr, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `B1SESSION=${info.cookie}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error
      };
      return err;
    }
  },
};

module.exports = { supppliers };
