const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const rawMats = {
  selectAllItems: async ({}) => {
    try {
      const client = await hdbClient();

      const bfi = process.env.sapDatabase; // bfi db

      const sql = `SELECT "ItemCode","ItemName" FROM "${bfi}"."OITM";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
};

module.exports = { rawMats };
