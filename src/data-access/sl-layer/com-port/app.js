const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const ports = {
   selectConfigOfDevice: async ({ info }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRSCALE?$select=U_TS_BAUDRATE,U_TS_PARITY,U_TS_DATABITS,U_TS_STOPBITS&$filter=U_TS_TR_IP eq '${info.ip}' and U_TS_DEVICE eq '${info.device.toLowerCase()}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${info.cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  }
};

module.exports = { ports };
