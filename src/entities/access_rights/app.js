const makeAccessRightsEntity = require("./access-rights");

const makeAccessRights = makeAccessRightsEntity({});

module.exports = { makeAccessRights };
