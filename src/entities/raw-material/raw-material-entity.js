const makeRawMaterialEntity = ({}) => {
  return function make({
    description,
    created_by,
    created_at = new Date().toISOString()
  } = {}) {
    if (!description) {
      throw new Error("Please enter raw material description.");
    }
    if (!created_by) {
      throw new Error("Please enter who created the raw material data.");
    }
    return Object.freeze({
      getDescription: () => description,
      getCreatedBy: () => created_by,
      getCreatedAt: () => created_at
    });
  };
};

module.exports = makeRawMaterialEntity;
