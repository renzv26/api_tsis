const updateTruckInfo = ({}) => {
    return function make({ 
      truck_type_id, 
      plate_number,
      updated_at = new Date().toISOString() } = {}) {
      
      if(!truck_type_id){
        throw new Error(`Please enter truck type ID.`)
      }

      if(!plate_number){
        throw new Error(`Please enter plate_number.`)
      }

      return Object.freeze({
        getTruckTypeId: () => truck_type_id,
        getPlateNumber: () => plate_number,
        getUpdatedAt: () => updated_at
      });
    };
  };
  
  module.exports = updateTruckInfo;
  