const makeActionsEntity = require("./actions-entity");
const makeActionsEntityUpdate = require("./actions-entity-update");

const makeActionsUpdate = makeActionsEntityUpdate({});
const makeActions = makeActionsEntity({});

module.exports = { makeActions, makeActionsUpdate };
