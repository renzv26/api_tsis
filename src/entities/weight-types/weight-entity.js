const makeWeightEntity = ({}) => {
  return function make({
    weight_name,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString()
  } = {}) {
    if (!weight_name) {
      throw new Error("Please enter weight type.");
    }
    return Object.freeze({
      getWeightName: () => weight_name,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeWeightEntity;
