const os = require("os");
const address = require("address");
const isIp = require("is-ip");

const makeTruckScaleEntity = require("./truckscale-entity");
const makeTruckScaleEntityUpdate = require("./truckscale-entity-update");

// return ip address of device
const returnIpAddress = () => {
  return address.ip();
};

// get host name of the device
const hostName = () => {
  return os.hostname();
};

// check if valid ip address
const checkIp = ip => {
  return isIp.v4(ip);
};
// only numbers allowed
const allNumbers = text => {
  const isnum = /^\d*(\.\d+)?$/.test(text);
  return isnum;
};

// ##########################

const makeTruckScale = makeTruckScaleEntity({
  returnIpAddress,
  hostName,
  allNumbers
});
const makeTruckScaleUpdate = makeTruckScaleEntityUpdate({
  allNumbers,
  checkIp
});

module.exports = {
  makeTruckScale,
  returnIpAddress,
  hostName,
  makeTruckScaleUpdate
};
