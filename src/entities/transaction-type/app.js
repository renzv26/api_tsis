const makeTransactionTypeEntity = require("./transaction-type-entity");

// ##########################
const makeTransactionType = makeTransactionTypeEntity({});

module.exports = { makeTransactionType };
