const makeModulesEntity = ({}) => {
  return function make({
    description,
    status = "active",
    created_at = new Date().toISOString(),
    created_by
  } = {}) {
    if (!description) {
      throw new Error("Please enter module name.");
    }

    if (!created_by) {
      throw new Error("Please enter who created the module.");
    }

    return Object.freeze({
      getDesc: () => description,
      getStatus: () => status,
      getCreatedAt: () => created_at,
      getCreatedBy: () => created_by
    });
  };
};

module.exports = makeModulesEntity;
