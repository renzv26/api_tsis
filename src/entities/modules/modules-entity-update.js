const makeModulesEntityUpdate = ({}) => {
  return function make({
    description,
    status,
    modified_by,
    updated_at = new Date().toISOString()
  } = {}) {
    if (!description) {
      throw new Error("Please enter module name.");
    }
    if (!status) {
      throw new Error("Please enter status.");
    }
    if (!modified_by) {
      throw new Error("Please enter who modified the module.");
    }
    return Object.freeze({
      getDesc: () => description,
      getStatus: () => status,
      getModifiedBy: () => modified_by,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeModulesEntityUpdate;
