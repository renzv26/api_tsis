const { io } = require("../app");
const { transactions } = require("../data-access/sl-layer/transactions/app");
const moment = require("moment-timezone");

// ####################
const webSocket = require("./socket-online");
// ####################
const webSockets = webSocket({ io, transactions, moment });
// ####################
const services = Object.freeze({
  webSockets,
});

module.exports = services;
module.exports = {
  webSockets,
};
