const webSocket = ({ io, transactions, moment }) => {
  return async function notif() {
    try {
      // if someone connected
      io.on("connect", async (socket) => {
        // retrieve and emit notif of user;

        // const d = new Date().toDateString();
        // const date = moment(d).format("YYYY-MM-DD"); // current date
        const date = moment().tz('Asia/Manila').format("YYYY-MM-DD");

        socket.on("my notif", async (data) => {
          const id = data.id;
          // const ifAcct = await transactions.ifAccounting(id)
          // const valid = ifAcct[0].role
          
          // get users notif
          const res = await transactions.getCurrentNotifs(id, date);
          let notifs = [];
          for (let i = 0; i < res.length; i++) {
            const e = res[i];
            // console.log('valid', valid + e.U_STATUS);
            // if(valid=='accounting' && e.U_STATUS!='pending'){

              notifs.push({
                id: e.Code,
                request_id: e.U_REQUEST_ID,
                receiver_id: e.U_RECEIVER_ID,
                request_status: e.U_STATUS,
                date: `${moment(e.U_UPDATEDATE).format("YYYY-MM-DD")} ${intToTime(
                  e.U_UPDATETIME
                )}`,
                date_created: `${moment(e.U_CREATEDATE).format("YYYY-MM-DD")} ${intToTime(
                  e.U_CREATETIME
                )}`,
                is_read: e.U_IS_READ > 0 ? true : false,
                sender_id: e.U_SENDER_ID,
                first_name: e.FN ? e.FN : e.FN_R,
                last_name: e.LN ? e.LN : e.LN_R,
                first_name_mod: e.FN_MOD ? e.FN_MOD : e.FN_R_MOD,
                last_name_mod: e.LN_MOD ? e.LN_MOD : e.LN_R_MOD,
                transaction_id: e.U_TRANSCTION_ID,
                ticket_number: e.U_TS_TICKETNUMBER,
                created_by: e.U_CREATEDBY,
                modified_by: e.U_MODIFYBY,
              });
            // }
            

            // if(valid!='accounting'){
            //   console.log('TEeeeST');
              
            //   notifs.push({
            //     id: e.Code,
            //     request_id: e.U_REQUEST_ID,
            //     receiver_id: e.U_RECEIVER_ID,
            //     request_status: e.U_STATUS,
            //     date: `${moment(e.U_CREATEDATE).format("YYYY-MM-DD")} ${intToTime(
            //       e.U_CREATETIME
            //     )}`,
            //     is_read: e.U_IS_READ > 0 ? true : false,
            //     is_read_val:e.U_IS_READ_VAL ,
            //     is_read_app:e.U_IS_READ_APP ,
            //     is_read_disapp:e.U_IS_READ_DISAPP ,
            //     sender_id: e.U_SENDER_ID,
            //     first_name: e.FN,
            //     last_name: e.LN,
            //     transaction_id: e.U_TRANSCTION_ID,
            //     created_by: e.U_CREATEDBY,
            //   });
            // }
          }

          const unfinishedRequests = await transactions.getUnfinishedRequests();
          const unfinishedRequestsCount = unfinishedRequests[0].REQUEST_COUNT;
          io.sockets.emit(`my req - ${id}`, {
            notifs, unfinishedRequestsCount,
          }); // emit data back to user;
        });

        // notif is read by user;
        socket.on("notif read", async (data) => {
          const id = data.id;
          const userId = data.userId;
          const cookie = `B1SESSION=${data.session}`;

          // update to read
          const info = { id, cookie, U_IS_READ: 1 };

          // console.log('@UPDATE-----',info);
          await transactions.requestNotifUpdate(info);

          // emit done update; to re-retrive notifs
          io.sockets.emit(`done all read - ${userId}`, {});
        });

        socket.on("remain requests", async (data) => {
          const userId = data.userId;
          const cookie = `B1SESSION=${data.session}`;

          // update to read
          const info = {cookie};

          // console.log('@REMAIN REQ-----');
          await transactions.getUnfinishedRequests( info );

          // emit done update; to re-retrive notifs
          io.sockets.emit(`receive unfinished notifs - ${userId}`, {});
        });

        // notif mark all read by user;
        socket.on("notif all read", async (data) => {
          const id = data.id;
          const cookie = `B1SESSION=${data.session}`;

          // get all request of that user on this day
          const req = await transactions.getNotifsId(id, date);
          let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;
          for (let i = 0; i < req.length; i++) {
            const e = req[i].Code;

            batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_BFI_TS_REQ_NOTIF('${e}')

{
    "U_IS_READ": "1"
}
`;
          }

          batchStr += `
--b--
--a--        
`;

          // update
          const datas = {
            batchStr,
            cookie,
          };
          // console.log('@PATCH----------',datas);
          await transactions.batchRequest(datas);

          // emit done update; to re-retrive notifs
          io.sockets.emit(`done all read - ${id}`, {});
        });
      });
    } catch (e) {
      console.log("Error: ", e);
    }
  };
};

const timeFormat = (t) => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}:${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

// convert into to time format
const intToTime = (i) => {
  if (i) {
    const str = i.toString();
    const len = str.length;

    let time = null;

    if (len == 4) {
      const hour = str.substring(0, 2);
      const min = str.substring(2, 4);
      time = `${hour}:${min}`;
      return time;
    } else if (len == 3) {
      const hour = str.substring(0, 1);
      const min = str.substring(1, 3);
      time = `0${hour}:${min}`;
      return time;
    } else {
      return `00:00`;
    }
  } else {
    return `00:00`;
  }
};

module.exports = webSocket;
